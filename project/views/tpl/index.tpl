<!DOCTYPE html>
<html lang="en">

{include file="file:`$config.core_dir`project/views/tpl/head.tpl"}

<body{if $html_body_id} id="{$html_body_id}"{/if}{if $html_body_style} style="{$html_body_style}"{/if}>
	<div id="core_checker">
		{include file="file:`$core_com`checker.tpl"}
	</div>

	{if !$core_pages->page_found}
		{include file=$core_pages->error_tpl}
	{else}
		{include file="layouts/`$core_pages->current_page.layout`.tpl"}
	{/if}
	
	{include file="includes/analytics.tpl"}
	
	<div id="core_sql_debugger">
		{include file="file:`$core_com`debuger.tpl"}
	</div>
</body>

</html>