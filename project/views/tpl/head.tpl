<head>
<base href="{$config.site_url}" />
<title>{include file="includes/page_title.tpl"}</title>

<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="robots" content="index, follow" />

<meta name="description" content="{include file="includes/page_description.tpl"}" />
<meta name="keywords" content="{include file="includes/page_keywords.tpl"}" />

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700,800,900&display=swap" />

{foreach from=$default_libs item="lib"}
{if $lib == "bootstrap"}
<link rel="stylesheet" type="text/css" href="{$config.views_url}vendors/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="{$config.views_url}vendors/magnific-popup/magnific-popup.css" />
<link rel="stylesheet" type="text/css" href="{$config.views_url}vendors/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="{$config.views_url}vendors/animate.css" />
{elseif $lib == "bootstrap-tour"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.10.1/css/bootstrap-tour.min.css" />
{elseif $lib == "bootstrap-rating"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/css/star-rating.min.css" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/themes/krajee-svg/theme.min.css" />
{elseif $lib == "bootstrap-modal"}
{elseif $lib == "bootstrap-table"}
{elseif $lib == "jasny-bootstrap"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.css" />
{elseif $lib == "ladda-bootstrap"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.1.0/ladda-themeless.min.css" />
{elseif $lib == "bootstrap-select"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.min.css" />
{elseif $lib == "bootstrap-social"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.min.css" />
{elseif $lib == "bootstrap-switch"}
{elseif $lib == "bootstrap-lightbox"}
{elseif $lib == "bootstrap-markdown"}
{elseif $lib == "knockout-bootstrap"}
{elseif $lib == "bootstrap-validator"}
{elseif $lib == "jquery-ui-bootstrap"}
{elseif $lib == "angular-ui-bootstrap"}
{elseif $lib == "bootstrap-datepicker"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
{elseif $lib == "bootstrap-tokenfield"}
{elseif $lib == "bootstrap-formhelpers"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/css/bootstrap-formhelpers.min.css" />
{elseif $lib == "bootstrap-datetimepicker"}
{elseif $lib == "bootstrap-hover-dropdown"}
{elseif $lib == "font-awesome"}
<link rel="stylesheet" type="text/css" href="{$config.views_url}vendors/font-awesome/css/fontawesome.css" />
{elseif $lib == "datatables"}
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />
{*<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css" />*}
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.0.0/css/responsive.dataTables.min.css" />
{elseif $lib == "datatables-buttons"}
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.1.0/css/buttons.bootstrap.min.css" />
{elseif $lib == "mdb"}
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/css/mdb.min.css" />
{elseif $lib == "tinymce"}
<link rel="stylesheet" type="text/css" href="{$config.views_url}vendors/tinymce/js/tinymce/skins/ui/oxide/skin.min.css" />
{/if}
{/foreach}

{foreach from=$default_css_styles item="style"}
<link rel="stylesheet" type="text/css" href="{$config.views_url}css/{$style}" />
{/foreach}

{if $core_pages->page_found}
	{assign var="css_file_name" value="`$config.views_dir`css/`$core_pages->include_page`.css"}
	{assign var="css_file_url" value="`$config.views_url`css/`$core_pages->include_page`.css"}
{/if}
{is_file_exists filename=$css_file_name}
{if $is_file_exists}<link rel="stylesheet" type="text/css" href="{$css_file_url}" />{/if}

<link rel="shortcut icon" href="{$config.views_url}images/favicon.ico" />

{$xajax_js}

{foreach from=$default_libs item="lib"}
{if $lib == "bootstrap"}
<script type="text/javascript" src="{$config.views_url}vendors/popper/popper.js"></script>
<script type="text/javascript" src="{$config.views_url}vendors/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{$config.views_url}vendors/slick/slick.js"></script>
{elseif $lib == "bootstrap-tour"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.10.1/js/bootstrap-tour.min.js"></script>
{elseif $lib == "bootstrap-rating"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/js/star-rating.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/themes/krajee-svg/theme.min.js"></script>
{elseif $lib == "bootstrap-modal"}
{elseif $lib == "bootstrap-table"}
{elseif $lib == "jasny-bootstrap"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
{elseif $lib == "ladda-bootstrap"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.1.0/spin.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.1.0/ladda.min.js"></script>
{elseif $lib == "bootstrap-select"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.min.js"></script>
{elseif $lib == "bootstrap-social"}
{elseif $lib == "bootstrap-switch"}
{elseif $lib == "bootstrap-lightbox"}
{elseif $lib == "bootstrap-markdown"}
{elseif $lib == "knockout-bootstrap"}
{elseif $lib == "bootstrap-validator"}
{elseif $lib == "jquery-ui-bootstrap"}
{elseif $lib == "angular-ui-bootstrap"}
{elseif $lib == "bootstrap-datepicker"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
{elseif $lib == "bootstrap-tokenfield"}
{elseif $lib == "bootstrap-formhelpers"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
{elseif $lib == "bootstrap-datetimepicker"}
{elseif $lib == "bootstrap-hover-dropdown"}
{elseif $lib == "jquery"}
<script type="text/javascript" src="{$config.views_url}vendors/jquery.min.js"></script>
{elseif $lib == "jquery.knob"}
<script type="text/javascript" src="https://raw.githubusercontent.com/aterrien/jQuery-Knob/master/js/jquery.knob.js"></script>
{elseif $lib == "datatables"}
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
{*<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>*}
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/responsive/2.0.0/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/plug-ins/1.10.25/sorting/natural.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/plug-ins/1.10.25/sorting/formatted-numbers.js"></script>
{elseif $lib == "datatables-buttons"}
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
{elseif $lib == "datatables-fixedheader"}
<script type="text/javascript" charset="utf8" src="//cdnjs.cloudflare.com/ajax/libs/datatables-fixedheader/2.1.1/dataTables.fixedHeader.min.js"></script>
{elseif $lib == "snap.svg"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/snap.svg/0.3.0/snap.svg-min.js"></script>
{elseif $lib == "bootlint"}
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootlint/0.5.0/bootlint.min.js"></script>
{elseif $lib == "mdb"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/js/mdb.min.js"></script>
{elseif $lib == "highcharts"}
<script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/exporting.js"></script>
{elseif $lib == "tinymce"}
<script type="text/javascript" src="{$config.views_url}vendors/tinymce/js/tinymce/tinymce.min.js"></script>
{elseif $lib == "google-jsapi"}
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
{/if}
{/foreach}

{foreach from=$default_js_scripts item="script"}
<script type="text/javascript" src="{$config.views_url}js/{$script}"></script>
{/foreach}

{if $core_pages->page_found}
	{assign var="js_file_name" value="`$config.views_dir`js/`$core_pages->include_page`.js"}
	{assign var="js_file_url" value="`$config.views_url`js/`$core_pages->include_page`.js"}
{/if}
{is_file_exists filename=$js_file_name}
{if $is_file_exists}<script type="text/javascript" src="{$js_file_url}"></script>{/if}

{assign var="meta_file_name" value="`$config.views_dir`tpl/includes/meta_tags.tpl"}
{is_file_exists filename=$meta_file_name}
{if $is_file_exists}{include file="includes/meta_tags.tpl"}{/if}

</head>