{foreach from=$default_libs item="lib"}
{if $lib == "bootstrap"}
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
{elseif $lib == "bootstrap-tour"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.10.1/js/bootstrap-tour.min.js"></script>
{elseif $lib == "bootstrap-growl"}
{elseif $lib == "bootstrap-modal"}
{elseif $lib == "bootstrap-table"}
{elseif $lib == "jasny-bootstrap"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
{elseif $lib == "ladda-bootstrap"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.1.0/spin.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.1.0/ladda.min.js"></script>
{elseif $lib == "bootstrap-select"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.min.js"></script>
{elseif $lib == "bootstrap-social"}
{elseif $lib == "bootstrap-switch"}
{elseif $lib == "bootstrap-lightbox"}
{elseif $lib == "bootstrap-markdown"}
{elseif $lib == "knockout-bootstrap"}
{elseif $lib == "bootstrap-validator"}
{elseif $lib == "jquery-ui-bootstrap"}
{elseif $lib == "angular-ui-bootstrap"}
{elseif $lib == "bootstrap-datepicker"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
{elseif $lib == "bootstrap-tokenfield"}
{elseif $lib == "bootstrap-formhelpers"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
{elseif $lib == "bootstrap-datetimepicker"}
{elseif $lib == "bootstrap-hover-dropdown"}
{elseif $lib == "jquery"}
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
{elseif $lib == "jquery.knob"}
<script type="text/javascript" src="https://raw.githubusercontent.com/aterrien/jQuery-Knob/master/js/jquery.knob.js"></script>
{elseif $lib == "datatables"}
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
{*<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>*}
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/responsive/2.0.0/js/dataTables.responsive.min.js"></script>
{elseif $lib == "datatables-buttons"}
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
{elseif $lib == "datatables-fixedheader"}
<script type="text/javascript" charset="utf8" src="//cdnjs.cloudflare.com/ajax/libs/datatables-fixedheader/2.1.1/dataTables.fixedHeader.min.js"></script>
{elseif $lib == "snap.svg"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/snap.svg/0.3.0/snap.svg-min.js"></script>
{elseif $lib == "bootlint"}
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootlint/0.5.0/bootlint.min.js"></script>
{elseif $lib == "mdb"}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/js/mdb.min.js"></script>
{elseif $lib == "highcharts"}
<script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/exporting.js"></script>
{elseif $lib == "google-jsapi"}
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">google.load("visualization", "1", {packages:["corechart"]});</script>
{/if}
{/foreach}

{foreach from=$default_js_scripts item="script"}
<script type="text/javascript" src="{$config.views_url}js/{$script}"></script>
{/foreach}

{if $core_pages->page_found}
	{assign var="js_file_name" value="`$config.views_dir`js/`$core_pages->include_page`.js"}
	{assign var="js_file_url" value="`$config.views_url`js/`$core_pages->include_page`.js"}
{/if}
{is_file_exists filename=$js_file_name}
{if $is_file_exists}<script type="text/javascript" src="{$js_file_url}"></script>{/if}