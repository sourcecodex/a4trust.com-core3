{strip}
<div class="modal-dialog" {if $modal.width}style="width: {$modal.width}px;"{/if}>
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closePopup();">&times;</button>
			<h4 class="modal-title">{$modal.title}</h4>
		</div>
		<div class="modal-body">
			{$smarty.capture.modal_body}
		</div>
		{if $smarty.capture.modal_footer}
		<div class="modal-footer">
			{$smarty.capture.modal_footer}
		</div>
		{/if}
	</div>
</div>
{/strip}