{strip}
{if $component.paging}
{include file="file:`$core_com`pagination.tpl"}
{/if}

{include file="file:`$core_com`table.tpl"}

{if $component.paging}
{include file="file:`$core_com`pagination.tpl"}
{/if}
{/strip}