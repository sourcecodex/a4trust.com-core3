{strip}
{if $item.custom}
	{include file="components/table/fields.tpl"}
{else}
{if $item.type == "text"}
	{if $item.maxlength}
	{$row[$field]|truncate:$item.maxlength}
	{else}{$row[$field]}{/if}
{elseif $item.type == "link"}
	<a href="{if $item.http}http://{/if}{$row[$field]}" {if $item.target == "blank"}target="_blank"{/if}{if $item.fontsize} style="font-size: {$item.fontsize}px;"{/if}>
		{if $item.maxlength}
		{$row[$field]|truncate:$item.maxlength}
		{else}{$row[$field]}{/if}
	</a>
{elseif $item.type == "email"}
	<a href="mailto:{$row[$field]}">{$row[$field]}</a>
{elseif $item.type == "string"}
	{$row[$field]|string_format:"`$item.format`"}
{elseif $item.type == "number"}
	{$row[$field]|number_format:"`$item.format`"}
{elseif $item.type == "date"}
	{if $row[$field] == "0000-00-00 00:00:00"}
		-
	{else}
		{$row[$field]|date_format:"`$item.format`"}
	{/if}
{elseif $item.type == "select"}
	{if $item.options}
		{assign_by_name var="select_options" value=$item.options}
		{assign var="select_key" value=$row[$field]}
		{$select_options[$select_key]}
	{/if}
{elseif $item.type == "yes_no"}
	{if $row[$field]}<span style="color:green;">Yes</span>{else}<span style="color:red;">No</span>{/if}
{elseif $item.type == "id_checkbox"}
	<input type="checkbox" name="ids[{$row.id}]" value="{$row.id}" />
{elseif $item.type == "checkboxes"}
	{assign_by_name var="checkboxes" value=$item.checkboxes}
	{foreach from=$checkboxes key="key" item="value"}
		{if $key|in_array:$row[$field]}{$value},{/if}
	{/foreach}
{elseif $item.type == "info_button"}
	<button type="button" class="btn btn-default btn-xs" onclick="xajax_createPopup('{$item.popup}', '{$row.id}');" title="Info">
		<span class="glyphicon glyphicon-info-sign"></span>
	</button>
{elseif $item.type == "edit_button"}
	<button type="button" class="btn btn-default btn-xs" onclick="xajax_createPopup('{$item.popup}', '{$row.id}');" title="Edit">
		<span class="glyphicon glyphicon-edit"></span>
	</button>
{elseif $item.type == "logo_button"}
	<button type="button" class="btn {if $row[$field]}btn-success{else}btn-default{/if} btn-xs" onclick="xajax_createPopup('{$item.popup}', '{$row.id}');">
		<span class="glyphicon glyphicon-picture"></span>
	</button>
{elseif $item.type == "img_button"}
	<button type="button" class="btn {if $row[$field]}btn-success{else}btn-default{/if} btn-xs" onclick="xajax_createPopup('{$item.popup}', '{$row.id}');">
		<span class="glyphicon glyphicon-picture"></span>
	</button>
{elseif $item.type == "remove_button"}
	<button type="button" class="btn btn-default btn-xs" onclick="tableDeleteItem('{$controller->name}', '{$row.id}', '{$item.confirmation}');" title="Remove">
		<span class="glyphicon glyphicon-remove"></span>
	</button>
{elseif $item.type == "call_button"}
	<button type="button" class="btn btn-default btn-xs" onclick="xajax_tableFunctionCall('{$controller->name}', '{$item.function}', {ldelim}'id': '{$row.id}'{rdelim}); $(this).prop('disabled', true);">
		<span class="glyphicon glyphicon-{$item.icon}"></span>
	</button>
{elseif $item.type == "emailhunter"}
	<a href="http://{$row[$field]}"target="_blank">{$row[$field]}</a> &nbsp;
	<a href="https://emailhunter.co/domain/{$row[$field]|replace:'www.':''}" target="_blank"><span class="glyphicon glyphicon-envelope"></span></a>
{elseif $item.type == "social_buttons"}
	{if $row.twitter}
	<a target="_blank" href="{$row.twitter}">
		<img width="22" src="http://serpyou.com/views/images/icons/social/twitter.png">
	</a>
	{/if}
	{if $row.facebook}
	<a target="_blank" href="{$row.facebook}">
		<img width="22" src="http://serpyou.com/views/images/icons/social/facebook.png">
	</a>
	{/if}
	{if $row.google_plus}
	<a target="_blank" href="{$row.google_plus}">
		<img width="22" src="http://serpyou.com/views/images/icons/social/googleplus.png">
	</a>
	{/if}
	{if $row.linkedin}
	<a target="_blank" href="{$row.linkedin}">
		<img width="22" src="http://serpyou.com/views/images/icons/social/linkedin.png">
	</a>
	{/if}
{elseif $item.type == "tag"}
	{if $row[$field] == 'moz'}<a href="{$row.profile_url}" target="_blank">{$row[$field]}</a>
	{else}{$row[$field]}{/if}
{elseif $item.type == "ip_link"}
	<a href="http://www.projecthoneypot.org/ip_{$row[$field]}" target="_blank">{$row[$field]}</a>
{else}
	Unknown field type `{$item.type}`
{/if}
{/if}
{/strip}