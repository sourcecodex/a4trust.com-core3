{strip}
{assign var="sorting_type" value=$page_info.order[$column]}
<a href="{$core_pages->url}?order_by[{$column}]={if $sorting_type|lower == "asc"}desc{else}asc{/if}">{$title}</a>
{if $sorting_type|lower == "asc" || $sorting_type|lower == "desc"}
{if $sorting_type|lower == "asc"}
&nbsp;<i class="fa fa-caret-up"></i>
{else}
&nbsp;<i class="fa fa-caret-down"></i>
{/if}
{*<a href="{$core_pages->url}?order_by[{$column}]=">x</a>*}
{/if}
{/strip}