{strip}
{*if $table.rows*}
<table {if $table.id}id="{$table.id}"{/if} class="table{if $table.options}
{if "striped"|in_array:$table.options} table-striped{/if}
{if "hover"|in_array:$table.options} table-hover{/if}
{if "bordered"|in_array:$table.options} table-bordered{/if}
{if "condensed"|in_array:$table.options} table-condensed{/if}
{if "compact"|in_array:$table.options} compact{/if}
{/if}">

{* ######## Table header ######## *}

{if $table.header}
<thead>
<tr>
	{foreach from=$table.columns key="field" item="item"}
	<th {if $table.hcol_attributes}{include file="file:`$core_com`table/hcol_attributes/`$table.hcol_attributes`.tpl"}{/if}>
		{if $item.type == "id_checkbox"}
			<input type="checkbox" onclick="markAll(document, this.checked);" />
		{elseif $item.sorting}
			{include file="file:`$core_com`table/sorting.tpl" column=$field title=$item.title}
		{else}
			{$item.title}
		{/if}
	</th>
	{/foreach}
</tr>
</thead>
{/if}

{* ######## Table data rows ######## *}
<tbody>
{foreach from=$table.rows key="key" item="row"}
<tr {if $table.row_attributes}{include file="file:`$core_com`table/row_attributes/`$table.row_attributes`.tpl"}{/if}>
	{foreach from=$table.columns key="field" item="item"}
	<td {if $table.col_attributes}{include file="file:`$core_com`table/col_attributes/`$table.col_attributes`.tpl"}{/if}>
		{include file="file:`$core_com`table/fields.tpl"}
	</td>
	{/foreach}
</tr>
{/foreach}
</tbody>

</table>
{*else}
<div class="alert alert-warning">No {$controller->name} found</div>
{/if*}
{/strip}