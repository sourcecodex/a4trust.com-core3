{strip}
<div class="row" style="margin-bottom: 20px;">
	<div class="col-lg-3">
		<button type="button" class="btn btn-primary" onclick="xajax_createPopup('{$controller->name}/add');">
			<span class="glyphicon glyphicon-plus"></span> {if $add_button_title}{$add_button_title}{else}Add {$controller->name|substr:0:-1}{/if}
		</button>
	</div>
	<div class="col-lg-6">
		{include file="file:`$core_com`search_form.tpl"}
	</div>
	<div class="col-lg-3">
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-danger" onclick="xajax_tableFunctionCall('{$controller->name}', 'deleteMulti', getFormDataIds());">
				<span class="glyphicon glyphicon-remove"></span> Remove Selected
			</button>
		</div>
	</div>
</div>
{/strip}