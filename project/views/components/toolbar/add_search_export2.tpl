{strip}
<div class="row" style="margin-bottom: 20px;">
	<div class="col-lg-3">
		<button type="button" class="btn btn-primary" onclick="xajax_createPopup('{$controller->name}/add');">
			<span class="glyphicon glyphicon-plus"></span> {if $add_button_title}{$add_button_title}{else}Add {$controller->name|substr:0:-1}{/if}
		</button>
	</div>
	<div class="col-lg-6">
		{include file="file:`$core_com`search_form.tpl"}
	</div>
	<div class="col-lg-3">
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-default" onclick="xajax_tableFunctionCall('{$controller->name}', 'export', getFormDataIds());">
				<span class="glyphicon glyphicon-export"></span> Export Selected
			</button>
		</div>
	</div>
</div>
{/strip}