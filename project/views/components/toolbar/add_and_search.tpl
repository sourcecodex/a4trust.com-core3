{strip}
<div class="row" style="margin-bottom: 20px;">
	<div class="col-lg-3">
		<button type="button" class="btn btn-primary" onclick="xajax_createPopup('{$controller->name}/add', {ldelim}'initAutocomplete': 1{rdelim});">
			<span class="glyphicon glyphicon-plus"></span> {if $add_button_title}{$add_button_title}{else}Add {$controller->name|substr:0:-1}{/if}
		</button>
	</div>
	<div class="col-lg-9">
		<div class="pull-right">
			{include file="file:`$core_com`search_form.tpl"}
		</div>
	</div>
</div>
{/strip}