{strip}
<div style="margin-bottom: 20px;">
	<button type="button" class="btn btn-primary" onclick="xajax_createPopup('{$controller->name}/add');">
		<span class="glyphicon glyphicon-plus"></span> {if $add_button_title}{$add_button_title}{else}Add {$controller->name|substr:0:-1}{/if}
	</button>
</div>
{/strip}