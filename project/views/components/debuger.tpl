{strip}
{if $debug}
<div id="core_sql_debugger">
<div style="margin-top: 10px; background-color: #efefef; border-top: 2px solid orange; color: #000;">

<div style="color: #333; font-weight: bold; padding: 5px;">
	<span style="color: green;">Core3</span> SQL DEBUG:
</div>

<table cellspacing="0" cellpadding="0" width="100%">
{foreach from=$debug_sql key="key" item="query"}
	<tr>
		<td style="border: 1px solid #aa55aa;">{$key}</td>
		<td style="border: 1px solid #aa55aa;">{$query[0]|nl2br}</td>
		<td style="border: 1px solid #aa55aa; width: 70px;">Rows: {$query[1]}</td>
		{if $query[2] != 0}<td style="border: 1px solid #aa55aa;">Error: #{$query[2]}</td>{/if}
		<td style="border: 1px solid #aa55aa; width: 120px;">Time: {$query[3]} micro s.</td>
		<td style="border: 1px solid #aa55aa; width: 100px;">{$query[4]}</td>
	</tr>
{/foreach}
</table>

<table cellspacing="0" cellpadding="0" width="100%" style="border-top: 2px solid red;">
{foreach from=$debug_sql2 key="key" item="query"}
	<tr>
		<td style="border: 1px solid #aa55aa;">{$key}</td>
		<td style="border: 1px solid #aa55aa;">{$query[0]|nl2br}</td>
		<td style="border: 1px solid #aa55aa; width: 70px;">Rows: {$query[1]}</td>
		{if $query[2] != 0}<td style="border: 1px solid #aa55aa;">Error: #{$query[2]}</td>{/if}
		<td style="border: 1px solid #aa55aa; width: 120px;">Time: {$query[3]} micro s.</td>
		<td style="border: 1px solid #aa55aa; width: 100px;">{$query[4]}</td>
	</tr>
{/foreach}
</table>

{if $debug_session_sql}
<br />
<table cellspacing="0" cellpadding="0" width="100%">
{foreach from=$debug_session_sql key="key" item="query"}
	<tr>
		<td style="border: 1px solid #0055aa;">{$key}</td>
		<td style="border: 1px solid #0055aa;">{$query[0]|nl2br}</td>
		<td style="border: 1px solid #0055aa; width: 70px;">Rows: {$query[1]}</td>
		{if $query[2] != 0}<td style="border: 1px solid #0055aa;">Error: #{$query[2]}</td>{/if}
		<td style="border: 1px solid #0055aa; width: 120px;">Time: {$query[3]} micro s.</td>
		<td style="border: 1px solid #0055aa; width: 100px;">{$query[4]}</td>
	</tr>
{/foreach}
</table>
{/if}

</div>
</div>
{/if}
{/strip}