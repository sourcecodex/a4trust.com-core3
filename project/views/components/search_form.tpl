{strip}
<form name="quick_search_form" action="{$core_pages->url}" method="post">
	<div class="input-group">
		<span class="input-group-btn">
			<button class="btn btn-default" type="submit">
				<span class="glyphicon glyphicon-search"></span>
			</button>
		</span>
		
		<input type="text" id="search_query" name="search_query" value="{$page_info.filter.search_query|escape}"
			   class="form-control" placeholder="Search" />

		<span class="input-group-btn">
			<button class="btn btn-default" onclick="xajax_createPopup('{$controller->name}/search'); return false;">
				<span>Advanced</span>
				{foreach from=$page_info.filter key="field" item="item"}
					{if $item != "" && $field != "search_query"}
					<span class="label label-info">{$controller->list_table.columns[$field].title}={$item}</span>
					{/if}
				{/foreach}
			</button>
		</span>
	</div>
</form>
{/strip}