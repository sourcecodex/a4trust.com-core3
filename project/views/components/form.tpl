{strip}
{assign var="form" value=$controller->$form_name}

{assign var="smarty_post" value=$smarty.post}
<form role="form" id="{$form_name}" name="{$form_name}" {if $form.method == "post"}method="post"{/if} {if $form.action}action="{$form.action}"{/if} {if $form.multipart}enctype="multipart/form-data"{/if}>
	<input type="hidden" name="controller" value="{$controller->name}" />
	<input type="hidden" name="act" value="{$act}" />
	{if $form.method == "xajax"}
		{if $form.redirect}<input type="hidden" name="xajax_redirect" value="{$form.redirect}" />{/if}
	{else}
		<input type="hidden" name="act_redirect" value="{$form.redirect}" />
	{/if}

	{if $form.layout == "default"}
		{include file="file:`$core_com`form/layouts/default.tpl"}
	{elseif $form.layout == "modal"}
		{include file="file:`$core_com`form/layouts/modal.tpl"}
	{elseif $form.layout == "modal-project"}
		{include file="file:`$core_com`form/layouts/modal-project.tpl"}
	{else}
		Form layout is not set.
	{/if}
</form>
{/strip}