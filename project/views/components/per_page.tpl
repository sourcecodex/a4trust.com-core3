{strip}
<div class="per_page">
	<select onchange="window.location='{$core_pages->url}?per_page='+this.value;" class="form-control" style="width: 80px;">
		<option value="10" {if $page_info.per_page == 10}selected{/if}>10</option>
		<option value="20" {if $page_info.per_page == 20}selected{/if}>20</option>
		<option value="50" {if $page_info.per_page == 50}selected{/if}>50</option>
		<option value="100" {if $page_info.per_page == 100}selected{/if}>100</option>
	</select>
</div>
{/strip}