{strip}
{assign var="table" value=$controller->$table_name}

{if $table.layout == "default"}
	{include file="file:`$core_com`table/layouts/default.tpl"}
{else}
	Table layout is not set.
{/if}
{/strip}