{strip}
{assign var="page_nr" value=$smarty.get.page}
{assign var="page" value=$page_info.pages[$page_nr]}

<div class="row">
	{if $page_info.result_count > 0}
	<div class="col-lg-3">
		<span>Showing <b>{$page.from}</b> to <b>{$page.to}</b> of <b>{$page_info.result_count}</b> entries</span>
	</div>

	<div class="col-lg-6" style="text-align: center;">
		<ul class="pagination">

		{if $page_info.pages_count > 0}

			<li{if $page_nr <= 1} class="disabled"{/if}>
				{if $page_nr <= 1}<span>&laquo;</span>
				{else}<a href="{$core_pages->url}?page={$page_nr-1}">&laquo;</a>{/if}
			</li>

			{if $page_nr < 10}
				{if $page_info.pages_count < 10}
					{section name=pages start=1 loop=$page_info.pages_count+1 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li{if $page_id == $page_nr} class="active"{/if}><a href="{$core_pages->url}?page={$page_id}">{$page_id}</a></li>
					{/section}
				{else}
					{section name=pages start=1 loop=11 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li{if $page_id == $page_nr} class="active"{/if}><a href="{$core_pages->url}?page={$page_id}">{$page_id}</a></li>
					{/section}
				{/if}
			{else}
				{if $page_nr+5 < $page_info.pages_count}
					{section name=pages start=$page_nr-5 loop=$page_nr+5 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li{if $page_id == $page_nr} class="active"{/if}><a href="{$core_pages->url}?page={$page_id}">{$page_id}</a></li>
					{/section}
				{else}
					{section name=pages start=$page_info.pages_count-10 loop=$page_info.pages_count+1 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li{if $page_id == $page_nr} class="active"{/if}><a href="{$core_pages->url}?page={$page_id}">{$page_id}</a></li>
					{/section}
				{/if}
			{/if}

			<li{if $page_nr >= $page_info.pages_count} class="disabled"{/if}>
				{if $page_nr >= $page_info.pages_count}<span>&raquo;</span>
				{else}<a href="{$core_pages->url}?page={$page_nr+1}">&raquo;</a>{/if}
			</li>

		{/if}

		</ul>
	</div>
		
	<div class="col-lg-3">
		{include file="file:`$core_com`per_page.tpl"}
	</div>
	{/if}
</div>
{/strip}