{strip}
{assign var="modal" value=$controller->$modal_name}

{if $modal.layout == "default"}
	{include file="file:`$core_com`modal/layouts/default.tpl"}
{else}
	Modal layout is not set.
{/if}
{/strip}