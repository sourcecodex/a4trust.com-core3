{strip}
{if $core_session.checker}
<div style="background-color: #efefef; border-bottom: 2px solid orange; color: #000;">

<div style="color: #333; font-weight: bold; padding: 5px;">
	<span style="color: green;">Core2</span> System Checker..
</div>

<div style="color: #333;">
{if $core_session.checker_logs|@count == 0}
	<span style="color: blue; font-weight: bold;">All right</span>
{else}
	<table cellspacing="0" cellpadding="0" width="100%">
	{foreach from=$core_session.checker_logs key="key" item="log"}
		<tr>
			<td style="padding-left: 10px; border-top: 1px solid #aa55aa; border-right: 1px solid #aa55aa;" width="30">{$key+1}</td>
			<td style="padding-left: 7px; border-top: 1px solid #aa55aa;">{$log}</td>
		</tr>
	{/foreach}
	</table>
{/if}
</div>

</div>
{/if}
{/strip}