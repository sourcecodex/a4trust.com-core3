<label class="control-label" for="{$field}">{$item.title}</label>
{if $smarty_post[$field]}
	<div {if $item.div_style}style="{$item.div_style}"{/if}>
		<img src="{$config.project_url}{$smarty_post[$field]}" alt="" />
		<button type="button" class="btn btn-default btn-xs" title="Remove"
				onclick="postToUrl('', {ldelim}'controller': '{$controller->name}', 'act': '{$item.remove_act}', 'id': '{$smarty_post.id}'{rdelim});">
			Remove
		</button>
	</div>
{else}
	{if $item.prepend || $item.append}<div class="input-group">{/if}
	{if $item.prepend}<span class="input-group-addon">{$item.prepend}</span>{/if}
	<input type="file"
		id="{$field}" name="{$field}" value="{if $item.default}{$item.default}{else}{$smarty_post[$field]|escape}{/if}"
		class="form-control{if $item.required} required{/if}"
		{if $item.disabled}disabled{/if}
		{if $item.placeholder}placeholder="{$item.placeholder}"{/if}
	/>
	{if $item.append}<span class="input-group-addon">{$item.append}</span>{/if}
	{if $item.prepend || $item.append}</div>{/if}

	<span for="{$field}" class="help-block" style="display: none;"></span>
	<span class="glyphicon form-control-feedback" style="display: none;"></span>
{/if}