<label class="control-label" for="{$field}">{$item.title}</label>

<textarea id="{$field}" name="{$field}" class="tinymce form-control{if $item.required} required{/if}" {if $item.style}style="{$item.style}"{/if}
			  {if $item.placeholder}placeholder="{$item.placeholder}"{/if}>{if $item.default}{$item.default}{else}{$smarty_post[$field]|escape}{/if}</textarea>
	<span for="{$field}" class="help-block" style="display: none;"></span>
	<span class="glyphicon form-control-feedback" style="display: none;"></span>