{if $item.custom}
	{include file="components/form/fields.tpl"}
{else}
{if $item.type == "text" || $item.type == "email" || $item.type == "password" || $item.type == "file"}
	<label class="control-label" for="{$field}">{$item.title}</label>
	{if $item.prepend || $item.append}<div class="input-group">{/if}
	{if $item.prepend}<span class="input-group-addon">{$item.prepend}</span>{/if}
	<input type="{if $item.type == "text" || $item.type == "email"}text{elseif $item.type == "password"}password{elseif $item.type == "file"}file{/if}"
		id="{$field}" name="{$field}" value="{if $item.default}{$item.default}{else}{$smarty_post[$field]|escape}{/if}"
		class="form-control{if $item.required} required{/if}{if $item.isemail} isemail{/if}"
		{if $item.autocomplete == "off"}autocomplete="off"{/if}
		{if $item.disabled}disabled{/if}
		{if $item.placeholder}placeholder="{$item.placeholder}"{/if}
	/>
	{if $item.append}<span class="input-group-addon">{$item.append}</span>{/if}
	{if $item.prepend || $item.append}</div>{/if}

	<span for="{$field}" class="help-block" style="display: none;"></span>
	<span class="glyphicon form-control-feedback" style="display: none;"></span>

{elseif $item.type == "hidden"}
	<input type="hidden" name="{$field}" value="{if $item.default}{$item.default}{else}{$smarty_post[$field]|escape}{/if}" />

{elseif $item.type == "select"}
	<label class="control-label" for="{$field}">{$item.title}</label>
	<select id="{$field}" name="{$field}" class="form-control{if $item.required} required{/if}" {if $item.multiple}multiple{/if} 
		{if $item.disabled}disabled{/if} {if $item.attributes}{$item.attributes}{/if}>
		{if $item.hide_default_option}{else}<option value="">- select -</option>{/if}
		{if $item.options}
		{assign_by_name var="select_options" value=$item.options}
		{foreach from=$select_options key="key" item="select_option"}
		<option value="{$key}" {if $smarty_post[$field] == $key || $item.default == $key}selected{/if}>{$select_option}</option>
		{/foreach}
		{/if}
	</select>
	<span for="{$field}" class="help-block" style="display: none;"></span>
	
{elseif $item.type == "yes_no"}
	<label class="control-label" for="{$field}">{$item.title}</label>
	<select id="{$field}" name="{$field}" class="form-control{if $item.required} required{/if}">
		{if $item.hide_default_option}{else}<option value="">- select -</option>{/if}
		<option value="1" {if $smarty_post[$field] == "1" || $item.default == "1"}selected{/if}>Yes</option>
		<option value="0" {if $smarty_post[$field] == "0" || $item.default == "0"}selected{/if}>No</option>
	</select>
	<span for="{$field}" class="help-block" style="display: none;"></span>

{elseif $item.type == "date"}

{elseif $item.type == "checkbox"}
	{if $item.title}<label class="control-label" for="{$field}">{$item.title}</label>{/if}
	
	<input name="{$field}" type="hidden" value="0" />
	
	{if $item.inline}
	<label class="checkbox-inline">
		<input name="{$field}" type="checkbox" value="1" {if $smarty_post[$field]}checked="checked"{/if} />
		{$item.text}
	</label>
	{else}
	<div class="checkbox">
		<label>
			<input name="{$field}" type="checkbox" value="1" {if $smarty_post[$field]}checked="checked"{/if} />
			{$item.text}
		</label>
	</div>
	{/if}
	
{elseif $item.type == "checkboxes"}
	{if $item.title}<label class="control-label" for="{$field}">{$item.title}</label><br/>{/if}
	{if $item.checkboxes}
	{assign_by_name var="checkboxes" value=$item.checkboxes}
	{foreach from=$checkboxes key="key" item="value"}
		<input name="{$field}[{$key}]" type="hidden" value="0" />
		{if $item.inline}
		<label class="checkbox-inline">
			<input name="{$field}[{$key}]" type="checkbox" value="{$key}" {if $key|in_array:$smarty_post[$field]}checked="checked"{/if} />
			{$value}
		</label>
		{else}
		<div class="checkbox">
			<label>
				<input name="{$field}[{$key}]" type="checkbox" value="{$key}" {if $key|in_array:$smarty_post[$field]}checked="checked"{/if} />
				{$value}
			</label>
		</div>
		{/if}
	{/foreach}
	{/if}
	
{elseif $item.type == "radios"}
	{if $item.title}<label class="control-label" for="{$field}">{$item.title}</label>{/if}
	{if $item.radios}
	{assign_by_name var="radios" value=$item.radios}
	{foreach from=$radios key="key" item="value"}
		{if $item.inline}
		<label class="radio-inline">
			<input name="{$field}" type="radio" value="{$key}" {if $smarty_post[$field] == $key}checked="checked"{/if} />
			{$value}
		</label>
		{else}
		<div class="radio">
			<label>
				<input name="{$field}" type="radio" value="{$key}" {if $smarty_post[$field] == $key}checked="checked"{/if} />
				{$value}
			</label>
		</div>
		{/if}
	{/foreach}
	{/if}

{elseif $item.type == "textarea"}
	<label class="control-label" for="{$field}">{$item.title}</label>
	<textarea id="{$field}" name="{$field}" class="form-control{if $item.required} required{/if}" {if $item.style}style="{$item.style}"{/if}
			  {if $item.placeholder}placeholder="{$item.placeholder}"{/if}>{if $item.default}{$item.default}{else}{$smarty_post[$field]|escape}{/if}</textarea>
	<span for="{$field}" class="help-block" style="display: none;"></span>
	<span class="glyphicon form-control-feedback" style="display: none;"></span>

{elseif $item.type == "editor"}
	{include file="file:`$core_com`form/fields/editor.tpl"}
{elseif $item.type == "image"}
	{include file="file:`$core_com`form/fields/image.tpl"}
{elseif $item.type == "label"}
	<label>{$item.title}</label>
	
{elseif $item.type == "div"}
	<label class="control-label" for="{$field}">{$item.title}</label>
	<div>{$smarty_post[$field]|escape}</div>
	
{elseif $item.type == "submit_button"}
	{if $item.remember_me}
	<div class="checkbox pull-left">
		<label>
			<input name="autologin" type="checkbox" value="1" /> Remember me
		</label>
	</div>
	{/if}
	{if $item.agree_terms}
	<div class="pull-left">
		<span style="font-size: 11px;">
			By clicking on Sign up, you agree to <a href="{$config.site_url}terms" target="_blank">terms & conditions</a><br/>
			and <a href="{$config.site_url}privacy" target="_blank">privacy policy</a>
		</span>
	</div>
	{/if}
	<button id="submit_button" type="submit" class="btn btn-primary{if $item.ladda} ladda-button{/if}{if $item.align == "right"} pull-right{/if}"
			{if $item.ladda}data-style="expand-right" data-size="s"{/if} onclick="
			{if $item.ladda}ladda_button = Ladda.create(this); ladda_button.start();{/if}
			{if $form.method == "xajax"}xajax_checkForm(getCheckFormData(document.forms['{$form_name}']));
			{else $form.method == "post"}document.forms['{$form_name}'].submit();
			{/if} $(this).prop('disabled', true); return false;
		">
		{$item.title}
	</button>
	{if $item.align == "right"}<div class="clearfix"></div>{/if}

{elseif $item.type == "coolcaptcha"}
	<div>
		This question is for testing whether you are a human visitor and to prevent automated spam submissions.<br/>
		<img id="captcha-image" src="{$config.root_url}libs/cool-php-captcha/captcha.php" alt="captcha" width="{$item.width}" height="{$item.height}" />
		<a id="change-image" href="#" onclick="
			document.getElementById('captcha-image').src='{$config.root_url}libs/cool-php-captcha/captcha.php?'+Math.random();
			document.getElementById('{$field}').focus(); return false;"
		>
			<span class="glyphicon glyphicon-refresh"></span>
		</a>
	</div>
	<label class="control-label" for="{$field}">{$item.title}</label>
	<input type="text" id="{$field}" name="{$field}" autocomplete="off" class="form-control required" />
	<span for="{$field}" class="help-block" style="display: none;"></span>
	<span class="glyphicon form-control-feedback" style="display: none;"></span>
{else}
	Unknown field type `{$item.type}`
{/if}
{/if}