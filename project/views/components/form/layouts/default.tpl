{strip}
{foreach from=$form.fields key="field" item="item"}
<div id="{$field}-group" class="form-group">
	{include file="file:`$core_com`form/fields.tpl"}
</div>
{/foreach}

{if $form.method == "post" && $act_errors}
<script type="text/javascript">
var act_errors = JSON.parse('{$act_errors}');
checkFormErrorHandling(act_errors);
</script>
{/if}
{/strip}