{strip}
<div class="modal-dialog" {if $form.width}style="width: {$form.width}px;"{/if}>
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closePopup();">&times;</button>
			<h4 class="modal-title">{$form.title}</h4>
		</div>
		<div class="modal-body">
			{foreach from=$form.fields key="field" item="item"}
				<div id="{$field}-group" class="form-group">
					{include file="file:`$core_com`form/fields.tpl"}
				</div>
			{/foreach}
		</div>
		{if $form.hide_footer}
		{else}
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" onclick="closePopup();">Cancel</button>
			<button id="submit_button" type="submit" class="btn btn-primary{if $form.ladda} ladda-button{/if}"
					{if $form.ladda}data-style="expand-right" data-size="s"{/if} onclick="
					{if $form.ladda}ladda_button = Ladda.create(this); ladda_button.start();{/if}
					{if $form.method == "xajax"}xajax_checkForm(getCheckFormData(document.forms['{$form_name}']));
					{else $form.method == "post"}document.forms['{$form_name}'].submit();
					{/if} $(this).prop('disabled', true); return false;
				">
				{if $form.submit_title}{$form.submit_title}{else}Save changes{/if}
			</button>
		</div>
		{/if}
	</div>
</div>
{/strip}