<?php

function createPopup($name, $form_data = null) {
	global $config, $coreSQL, $coreSession, $coreControllers, $smarty,
			$pages, $popups, $users, $languages, $translations, $options, $countries,
			$mailer_emails, $mailer_templates, $mailer_users, $mailer_contacts,
			$news, $companies, $accounting_payments, $seo_contents, $seo_links, $addresses, $projects;

	$objResponse = new xajaxResponse();

	$create_popup = true;

	if ($popups->popups[$name]['default']) {
		$coreSQL->query_class = 'core/assign/popups/'.$name.'.assign.php';
		$assign_file_name = $config['core_control_dir'].'assign/popups/'.$name.'.assign.php';
	}
	else {
		$coreSQL->query_class = 'assign/popups/'.$name.'.assign.php';
		$assign_file_name = $config['control_dir'].'assign/popups/'.$name.'.assign.php';
	}
	if (file_exists($assign_file_name)) { include_once $assign_file_name; }

	if ($create_popup) {
		$smarty->assign('form_data', $form_data);

		if ($popups->popups[$name]['default']) {
			$content = $smarty->fetch('file:'.$config['core_tpl_dir'].'popups/'.$name.'.tpl');
		}
		else {
			$content = $smarty->fetch('popups/'.$name.'.tpl');
		}

		$objResponse->addScriptCall('createPopup');
		$objResponse->addAssign('popup_content', 'innerHTML', $content);
		$objResponse->addScriptCall('showPopup');

		$objResponse->addScriptCall('createPopupCallback', $name, json_encode($form_data));

		if ($form_data['initAutocomplete']) {
			$objResponse->addIncludeScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyCHWl4_-yEVpSUePDMcYkc9KCRI1W-QE4g&libraries=places&callback=initAutocomplete');
		}
		
		if ($coreSQL->debug) {
			$smarty->assign('debug', true);
			$smarty->assign('debug_sql', $coreSQL->queries);
			$debugger_content = $smarty->fetch($config['core_dir'].'project/views/tpl/debug.tpl');
			$objResponse->addAssign("core_sql_debugger", "innerHTML", $debugger_content);
		}
	}

	return $objResponse;
}
$xajax->registerFunction("createPopup");

?>