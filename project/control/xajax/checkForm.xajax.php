<?php

function checkForm($data) {
	global $config, $coreLogs, $coreSQL, $coreSession, $coreControllers, $smarty, $users, $translations;

	$objResponse = new xajaxResponse();

	$controller_name = '';
	$function = '';
	$form_data = array();
	$errors = array();

	foreach ($data as $form_field) {

		if ($form_field['name'] == "controller") $controller_name = $form_field['value'];
		if ($form_field['name'] == "act") $function = $form_field['value'];

		if (in_array("required", $form_field['classes'])) {
			if (empty($form_field['value'])) {
				$errors[$form_field['name']][] = "error_required";
			}
		}

		if (in_array("isemail", $form_field['classes'])) {
			if (!is_valid_email($form_field['value'])) {
				$errors[$form_field['name']][] = "error_isemail";
			}
		}

		$form_data[$form_field['name']] = $form_field['value'];

	}

	if (isset($coreControllers[$controller_name]) && count($errors) == 0) {

		if (in_array($function."_xajax", $coreControllers[$controller_name]->public_acts) ||
			(in_array($function."_xajax", $coreControllers[$controller_name]->logged_acts) && $users->isLogged)) {

			$coreSQL->query_class = $controller_name.'->'.$function."_xajax";

			$form_data['function_result'] = $coreControllers[$controller_name]->$function($form_data);

			foreach ($smarty->get_template_vars() as $var => $value) {
				if (substr($var, 0, 6) == "error_") {
					$errors[$value][] = $var;
				}
			}

			// TODO log errors

			if ($config['action_logs']) {
				$form_data['controller'] = $controller_name;
				$form_data['act'] = $function."_xajax";

				$log_data = $coreLogs->getLogData($form_data);

				$coreSQL->query_class = "coreLogs";
				$coreLogs->add($log_data);
			}

			if ($coreSQL->debug) {
				$smarty->assign('debug', true);
				$smarty->assign('debug_sql', $coreSQL->queries);
				$debugger_content = $smarty->fetch($config['core_dir'].'project/views/tpl/debug.tpl');
				$objResponse->addAssign("core_sql_debugger", "innerHTML", $debugger_content);
			}

			$objResponse->addScriptCall("checkFormCallback", $controller_name, $function, json_encode($form_data));
		}
		else {
			$objResponse->addAlert("Error: Act '".$function."_xajax' is not public in `".$controller_name."` controller.");
		}

	}

	$errors_return = array();
	foreach ($data as $form_field) {
		if (isset($errors[$form_field['name']])) {
			foreach ($errors[$form_field['name']] as $value) {
				$errors_return[] = array($form_field['name'], $value, $translations->t($value));
			}
		}
		else {
			$errors_return[] = array($form_field['name'], "ok");
		}
	}
	
	if (count($errors) > 0) {
		$objResponse->addScriptCall("checkFormErrorHandling", $errors_return);
	}
	else {
		$objResponse->addScriptCall("checkFormSuccessful", $controller_name, $function, json_encode($form_data));
		
		if (isset($form_data['xajax_redirect']) && !$coreSQL->debug) {
			$coreSession->close();
			$objResponse->addRedirect($form_data['xajax_redirect']);
		}
	}

	return $objResponse;
}
$xajax->registerFunction("checkForm");

?>