<?php

function getMailerTemplate($id) {
	global $mailer_templates;

	$objResponse = new xajaxResponse();

	$template = $mailer_templates->getById($id);

	if ($template) {
		$objResponse->addAssign("subject", "value", $template['subject']);
		$objResponse->addAssign("body", "innerHTML", $template['body']);
	}

	return $objResponse;
}
$xajax->registerFunction("getMailerTemplate");

?>
