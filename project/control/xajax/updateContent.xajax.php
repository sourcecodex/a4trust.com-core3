<?php

function updateContent($container, $name, $form_data = null) {
	global $config, $coreSQL, $coreControllers, $smarty, $pages, $popups, $users;

	$objResponse = new xajaxResponse();

	$assign_file_name = $config['control_dir'].'assign/contents/'.$name.'.assign.php';
	if (file_exists($assign_file_name)) include_once $assign_file_name;

	$content = $smarty->fetch('contents/'.$name.'.tpl');

	$objResponse->addAssign($container, "innerHTML", $content);

	return $objResponse;
}
$xajax->registerFunction("updateContent");

?>