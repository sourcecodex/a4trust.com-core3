<?php

function tableFunctionCall($controller_name, $function, $form_data) {
	global $config, $coreSQL, $coreControllers, $smarty, $users;

	$objResponse = new xajaxResponse();

	if (isset($coreControllers[$controller_name])) {

		if (in_array($function."_xajax", $coreControllers[$controller_name]->public_acts) ||
			(in_array($function."_xajax", $coreControllers[$controller_name]->logged_acts) && $users->isLogged)) {

			$coreSQL->query_class = $controller_name.'->'.$function."_xajax";

			$form_data['function_result'] = $coreControllers[$controller_name]->$function($form_data);

			if ($coreSQL->debug) {
				$smarty->assign('debug', true);
				$smarty->assign('debug_sql', $coreSQL->queries);
				$debugger_content = $smarty->fetch($config['core_dir'].'project/views/tpl/debug.tpl');
				$objResponse->addAssign("core_sql_debugger", "innerHTML", $debugger_content);
			}

			$objResponse->addScriptCall("tableFunctionCallback", $controller_name, $function, json_encode($form_data));
		}
		else {
			$objResponse->addAlert("Error: Act '".$function."_xajax' is not public in `".$controller_name."` controller.");
		}

	}

	return $objResponse;
}
$xajax->registerFunction("tableFunctionCall");

?>