<?php

/**
 * Projet User class
 * 
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

// TODO: add cookieHash salt (security++)

class project_users extends controller {

	/**
	 * Vartotojo id
	 *
	 * @var int
	 */
	var $id;
	
	/**
	 * Duomenys apie vartotoja
	 *
	 * @var array
	 */
	var $userData;
	
	/**
	 * Ar vartotojas prisijunges
	 *
	 * @var bool
	 */
	var $isLogged;

	/**
	 * Sesijos ir COOKIE lauku pavadinimai
	 *
	 */
	var $session_user, $session_user_data, $cookie_user, $cookie_hash;
	
	/**
	 * Konstruktorius, kuris patikrina ar vartotojas prisijunges
	 *
	 * @return coreUser
	 */
	function project_users() {
		
		parent::controller("users");
		
		$this->fields = array(
			"created" => "created",
			"last_online" => "datetime",
			"user_ip" => "string",
			"admin" => "bool",
			"first_name" => "string",
			"last_name" => "string",
			"email" => "string",
			"username" => "string",
			"password" => "password",
			"active" => "int",
			"confirm_code" => "string",
			"email_confirmed" => "bool",
			"default_lang" => "string",
		);

		$this->id = 0;
		$this->userData = array();
		$this->isLogged = 0;

		$this->session_user = 'coreUser';
		$this->session_user_data = 'coreUserData';
		$this->cookie_user = 'cookieUser';
		$this->cookie_hash = 'cookieHash';

	}
	
	function getByEmail($email) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `email`='".addslashes($email)."' LIMIT 1");
	}

	function getByEmailPass($email, $password) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `email`='".addslashes($email)."' AND `password`='".md5($password)."' LIMIT 1");
	}

	function getByEmailHash($email, $password) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `email`='".addslashes($email)."' AND `password`='".addslashes($password)."' LIMIT 1");
	}

	function getBySocialIdentifier($social_identifier) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `social_identifier`='".addslashes($social_identifier)."' LIMIT 1");
	}
	
	function getByUserCodeHash($user_id, $code, $hash) {
		global $coreSQL;
		$code = clean($code);
		$hash = clean($hash);
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE
				`confirm_code`='$code' AND `password`='$hash' AND `id`=".(int)$user_id);
	}

	function updateIp($id) {
		global $coreSQL;
		$coreSQL->query("UPDATE `".$this->table."` SET `user_ip`='".clean(getIp())."' WHERE `id`=".(int)$id);
	}

	// ============================= Authentication ============================================

	function logUser() {
		global $config, $coreSQL, $coreSession;

		// Log by COOKIE
		if ($config['user_cookie']) {
			if (!isset($coreSession->session[$this->session_user]) || empty($coreSession->session[$this->session_user])) {
				if (isset($_COOKIE[$this->cookie_user]) && !empty($_COOKIE[$this->cookie_user])) {
					$user_info = $this->getById($_COOKIE[$this->cookie_user]);
					if ($user_info['password'] == $_COOKIE[$this->cookie_hash] && $user_info['active'] == 1) {
						$coreSession->session[$this->session_user] = $user_info['id'];
						$coreSession->session[$this->session_user_data] = $user_info;
					}
				}
			}
		}

		if ($coreSession->session[$this->session_user] == 0) {
			unset($coreSession->session[$this->session_user]);
		}

		// Log by SESSION
		if (isset($coreSession->session[$this->session_user]) && !empty($coreSession->session[$this->session_user])) {
			$this->id = $coreSession->session[$this->session_user];
			$this->userData = $coreSession->session[$this->session_user_data];
			$this->isLogged = 1;
		}

		/*if ($this->isLogged) {
			if (isset($this->userData['disable_last_online'])) {
				// Do nothing
			}
			else $coreSQL->query("UPDATE `".$this->table."` SET `last_online`='".date('Y-m-d H:i:s')."' WHERE `id`=".(int)$this->id);
		}*/
	}

	function signIn($form_data, $by_hash = false) {
		global $config, $coreSQL, $coreSession, $smarty;

		$email = trim($form_data['email']);
		$password = trim($form_data['password']);

		if ($by_hash) {
			$rec = $this->getByEmailHash($email, $password);
		}
		else {
			$rec = $this->getByEmailPass($email, $password);
		}
		
		if (!empty($rec)) {
			if ($rec['active'] == 1) {
				if ($rec['email_confirmed'] == 1) {
					
					//if (isset($form_data['disable_last_online'])) {
					//	$rec['disable_last_online'] = 1;
					//}
					
					$coreSession->session[$this->session_user] = $this->id = $rec['id'];
					$coreSession->session[$this->session_user_data] = $this->userData = $rec;
					$this->isLogged = 1;
					
					if (isset($form_data['disable_update_ip'])) {
						// Do nothing
					}
					else {
						$this->updateIp($this->id);
					}
					
					if ($config['user_cookie']) {
						if ($form_data['autologin']) {
							setcookie($this->cookie_user, $this->id, $config['user_cookie_time'], $config['cookie_path']);
							setcookie($this->cookie_hash, ($by_hash?$password:md5($password)), $config['user_cookie_time'], $config['cookie_path']);
						}
					}

					$this->signInHook($form_data);

					if (isset($form_data['act_redirect'])) {
						$coreSession->close();
						redirect(clean($form_data['act_redirect']));
					}
				}
				else {
					$smarty->assign('error_email_confirmed', 'email');
				}
			}
			else {
				$smarty->assign('error_active', 'email');
			}
		}
		else {
			$smarty->assign('error_login', 'email');
			$smarty->assign('error_login_pass', 'password');
		}
	}

	function signInSocial($form_data) {
		global $coreSession, $smarty;

		$rec = $this->getBySocialIdentifier($form_data['social_identifier']);
		
		if (!empty($rec)) {
			if ($rec['active'] == 1) {
				if ($rec['email_confirmed'] == 1) {
					
					$coreSession->session[$this->session_user] = $this->id = $rec['id'];
					$coreSession->session[$this->session_user_data] = $this->userData = $rec;
					$this->isLogged = 1;
					
					if (isset($form_data['disable_update_ip'])) {
						// Do nothing
					}
					else {
						$this->updateIp($this->id);
					}

					$this->signInHook($form_data);

					if (isset($form_data['act_redirect'])) {
						$coreSession->close();
						redirect(clean($form_data['act_redirect']));
					}
				}
				else {
					$smarty->assign('error_email_confirmed', 'email');
				}
			}
			else {
				$smarty->assign('error_active', 'email');
			}
		}
		else {
			$smarty->assign('error_login', 'email');
		}
	}
	
	function signInHook($form_data) {
		// empty
	}

	function signOut($form_data) {
		global $config, $coreSession;

		if (isset($coreSession->session[$this->session_user])) {
			unset($coreSession->session[$this->session_user]);
		}
		if (isset($coreSession->session[$this->session_user_data])) {
			unset($coreSession->session[$this->session_user_data]);
		}
		
		if ($config['user_cookie']) {
			setcookie($this->cookie_user, '', time()-1, $config['cookie_path']);
			setcookie($this->cookie_hash, '', time()-1, $config['cookie_path']);
		}

		$this->id = 0;
		$this->userData = array();
		$this->isLogged = 0;
	}
	
	function autoLogin($form_data) {
		global $smarty, $coreSession;

		$user_info = $this->getByUserCodeHash($form_data['u'], $form_data['c'], $form_data['h']);

		if ($user_info) {
			
			if (isset($form_data['disable_update_ip'])) {
				$user_info['disable_update_ip'] = (bool)$form_data['disable_update_ip'];
			}
			
			$this->signIn($user_info, true);

			if ($form_data['reset_password']) {
				$coreSession->session['reset_password_user_id'] = (int)$user_info['id'];
				$coreSession->close();
			}

			if (isset($form_data['act_redirect'])) {
				redirect(clean($form_data['act_redirect']));
			}

		}
		else {
			$smarty->assign('error_auto_login', 'Error: Incorrect auto login');
		}
	}
	
	// ============================= Social Authentication =========================================
	
	function socialAuthenticate($provider, $success_redirect) {
		global $config, $smarty;

		if (isset($provider) && $provider) {

			require_once($config['hybridauth_include']);

			try {
				$provider = trim(strip_tags($provider));
				$hybridauth = new Hybrid_Auth($config['hybridauth_config']);
				$adapter = $hybridauth->authenticate($provider);
				$hybridauth->redirect($success_redirect.$provider);
			}
			catch (Exception $e) {
				switch ($e->getCode()) {
					case 0 : $smarty->assign('auth_error', "Unspecified error."); break;
					case 1 : $smarty->assign('auth_error', "Hybriauth configuration error."); break;
					case 2 : $smarty->assign('auth_error', "Provider not properly configured."); break;
					case 3 : $smarty->assign('auth_error', "Unknown or disabled provider."); break;
					case 4 : $smarty->assign('auth_error', "Missing provider application credentials."); break;
					case 5 : $smarty->assign('auth_error', "Authentication failed. The user has canceled the authentication or the provider refused the connection."); break;
					case 6 : $smarty->assign('auth_error', "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.");
							 break;
					case 7 : $smarty->assign('auth_error', "User not connected to the provider.");
							 break;
				}

				//$smarty->assign('error', $e->getMessage()." ".$e->getTraceAsString());
			}
		}
	}

	function socialGet($provider, $error_redirect, $get_data) {
		global $config, $smarty;

		if (isset($provider) && $provider) {

			require_once($config['hybridauth_include']);

			try {
				$provider = trim(strip_tags($provider));
				$hybridauth = new Hybrid_Auth($config['hybridauth_config']);

				if (!$hybridauth->isConnectedWith($provider)) {
					redirect($error_redirect);
				}

				$adapter = $hybridauth->getAdapter($provider);

				if ($get_data == "profile") {
					return $adapter->getUserProfile();
				}
				elseif ($get_data == "contacts") {
					return $adapter->getUserContacts();
				}

			}
			catch (Exception $e) {
				switch ($e->getCode()) {
					case 0 : $smarty->assign('auth_error', "Unspecified error."); break;
					case 1 : $smarty->assign('auth_error', "Hybriauth configuration error."); break;
					case 2 : $smarty->assign('auth_error', "Provider not properly configured."); break;
					case 3 : $smarty->assign('auth_error', "Unknown or disabled provider."); break;
					case 4 : $smarty->assign('auth_error', "Missing provider application credentials."); break;
					case 5 : $smarty->assign('auth_error', "Authentication failed. The user has canceled the authentication or the provider refused the connection."); break;
					case 6 : $smarty->assign('auth_error', "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.");
							 break;
					case 7 : $smarty->assign('auth_error', "User not connected to the provider.");
							 break;
				}

				//$smarty->assign('error', $e->getMessage()." ".$e->getTraceAsString());
			}
		}
	}

	// ============================= Password functions ============================================

	function resetPassword($form_data) {
		global $smarty, $mailer_emails;

		$error = false;

		if (!isValidEmail($form_data['email'])) {
			$smarty->assign('error_email_invalid', 'email');
			$error = true;
		}

		if (empty($_SESSION['captcha']) || strtolower(trim($_REQUEST['captcha'])) != $_SESSION['captcha']) {
			$smarty->assign('error_captcha', 'captcha');
			$error = true;
		}

		if (!$error) {
			$user_info = $this->getByEmail($form_data['email']);

			if ($user_info['id']) {
				$user_info['subject'] = $form_data['subject'];
				$mailer_emails->addResetPasswordEmail($user_info);
				redirect($form_data['act_redirect']);
			}
			else {
				$smarty->assign('error_not_found', 'email');
			}
		}
	}

	function changePassword($form_data) {
		global $coreSQL, $smarty;

		$current_password = $form_data['current_password'];
		$new_password = clean($form_data['new_password']);
		$new_password2 = clean($form_data['new_password2']);

		$error = false;

		if (strlen($new_password) < 6) {
			$smarty->assign('error_password_length', 'new_password');
			$error = true;
		}

		if ($new_password != $new_password2) {
			$smarty->assign('error_password_confirm', 'new_password2');
			$error = true;
		}

		$user_info = $this->getById($this->id);
		if ($user_info['password'] != md5($current_password)) {
			$smarty->assign('error_current_password', 'current_password');
			$error = true;
		}

		if (!$error) {
			$coreSQL->query("UPDATE `".$this->table."` SET `password`='".md5($new_password)."' WHERE `id`=".(int)$this->id);
		}
	}

	function changePasswordOnReset($form_data) {
		global $coreSQL, $smarty, $coreSession;

		$new_password = clean($form_data['new_password']);
		$new_password2 = clean($form_data['new_password2']);

		$error = false;

		$user_info = $this->getById($coreSession->session['reset_password_user_id']);
		if (!$user_info) {
			$smarty->assign('error_reset_password', 'new_password');
			$error = true;
		}

		if (strlen($new_password) < 6) {
			$smarty->assign('error_password_length', 'new_password');
			$error = true;
		}

		if ($new_password != $new_password2) {
			$smarty->assign('error_password_confirm', 'new_password2');
			$error = true;
		}

		if (!$error) {
			$coreSQL->query("UPDATE `".$this->table."` SET `password`='".md5($new_password)."' WHERE `id`=".(int)$user_info['id']);
		}
	}

	// ============================= Add, edit, delete ============================================

	function edit($form_data) {
		global $coreSession;

		parent::edit($form_data);
		$coreSession->session[$this->session_user_data] = $this->userData = $this->getById($this->id);
	}

	function setAfterPayment($user_id, $billing_email, $payment) {
		if ($user_id) {
			$this->edit(array(
				'id' => (int)$user_id,
				'billing_email' => trim(strtolower($billing_email)),
			));
		}
	}
	
}