<?php

/**
 * Project CRM contacts class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class project_crm_contacts extends controller {

	function project_crm_contacts() {
		parent::controller("crm_contacts");

		$this->fields = array(
			"created" => "created",
			"first_name" => "string",
			"last_name" => "string",
			"email" => "string",
			"phone" => "string",
			"job_title" => "string",
			"company_id" => "int",
			"tag" => "string",
		);
	}

	function is($email) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `email`='".addslashes($email)."'");
	}

}

?>