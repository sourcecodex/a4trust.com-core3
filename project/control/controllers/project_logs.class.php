<?php

/**
 * Project Logs class
 *
 * @package Tefo core2
 * @version 2011.01.05
 * @author Tefo <tefo05@gmail.com>
 */

class project_logs extends controller {

	function project_logs() {
		parent::controller("action_logs");

		$this->fields = array (
			"created" => "created",
			"user_id" => "int",
			"user_ip" => "string",
			"url" => "string",
			"controller" => "string",
			"action" => "string",
			"info" => "text"
		);
		
		//$this->autoCreateOnChecker();
	}

	function getLogData($post_data) {
		global $config, $user, $coreRouter;

		$log_data = array();
		$log_data['user_id'] = (int)$user->id;
		$log_data['user_ip'] = getIp();
		$log_data['url'] = $config['site_url'].$coreRouter->url;
		$log_data['controller'] = $post_data['controller'];
		$log_data['action'] = $post_data['act'];

		if ($post_data['act'] == 'add' || $post_data['act'] == 'add_xajax') {
			$log_data['info'] = $post_data['action_result'];
		}
		elseif ($post_data['act'] == 'edit' || $post_data['act'] == 'edit_xajax' ||
				$post_data['act'] == 'delete' || $post_data['act'] == 'delete_xajax') {
			$log_data['info'] = $post_data['id'];
		}
		elseif ($post_data['act'] == 'deleteSelected' || $post_data['act'] == 'deleteSelected_xajax') {
			$log_data['info'] = serialize($post_data['list']);
		}

		return $log_data;
	}

}

?>