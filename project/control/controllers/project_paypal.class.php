<?php

/**
 * Project paypal class
 *
 * @package Tefo core3
 * @version 2014.05.21
 * @author Tefo <tefo05@gmail.com>
 */

class project_paypal extends controller {

	function project_paypal() {
		parent::controller("paypal");

		$this->fields = array (
			"created" => "created",
			"txn_id" => "string",
			"transaction_subject" => "string",
			"txn_type" => "string",
			"subscr_id" => "string",
			"business" => "string",
			"invoice" => "string",
			"first_name" => "string",
			"last_name" => "string",
			"residence_country" => "string",
			"payer_status" => "string",
			"payer_email" => "string",
			"payer_id" => "string",
			"receiver_email" => "string",
			"receiver_id" => "string",
			"item_name" => "string",
			"item_number" => "string",
			"payment_type" => "string",
			"payment_status" => "string",
			"pending_reason" => "string",
			"payment_fee" => "float",
			"payment_gross" => "float",
			"mc_currency" => "string",
			"mc_fee" => "float",
			"mc_gross" => "float",
			"charset" => "string",
			"notify_version" => "string",
			"ipn_track_id" => "string",
			"protection_eligibility" => "string",
			"verify_sign" => "string",
			"log" => "text",
		);

		$this->dev_mode = false;
		
		//$this->createTableStructure();
	}

	function isAlreadyValidated($txn_id) {
		global $coreSQL;
		return $coreSQL->queryValue("
			SELECT count(`id`) FROM `".$this->table."`
			WHERE `txn_id` = '".addslashes($txn_id)."' AND `payment_status` = 'Completed'");
	}

	function callback() {
		global $smarty, $coreSQL;
		
		if ($this->isAlreadyValidated($_POST['txn_id'])) {
			return; //all done earlier
		}

		$req = 'cmd=_notify-validate';
		foreach ($_POST as $key => $value) {
			$value = urlencode(stripslashes($value));
			$value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}', $value); // IPN fix
			$req .= "&$key=$value";
		}

		$fp = fsockopen (($this->dev_mode ? 'www.sandbox.paypal.com' : 'www.paypal.com'), 80, $errno, $errstr, 30);

		if (!$fp) {
			$smarty->assign('callback_error', "fsockopen error no. $errno: $errstr");
			fclose ($fp);
			return;
		}

		$header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n";
		$header .= "Host: www.paypal.com\r\n";
		$header .= "Connection: close\r\n\r\n";
		
		fputs ($fp, $header . $req);

		$res = '';
		while (!feof($fp)) {
			$res .= fgets($fp, 1024);
		}

		fclose ($fp);
		
		// testuoti jei nepraeina IPN
		$coreSQL->query("INSERT INTO `paypal_logs` (`id`, `req`, `log`) VALUES (NULL, '".addslashes($req)."', '".addslashes($res)."');");
		
		if (eregi("VERIFIED", $res)) {
			$this->add($_POST);
			$this->checkPaymentParams($_POST);

		} else {
			$smarty->assign('callback_error', 'IPN Validation Failed.');
		}
	}
	
	function checkPaymentParams($params) {
		global $smarty, $users, $accounting_payments, $accounting_products;

		if ($params['payment_status'] == 'Completed') {

			if ($accounting_payments->is($params['txn_id'])) {
				return;
			}

			$user_id = (int)strtok($params['invoice'], '-');

			$product = $accounting_products->getById($params['item_number']);

			$startFrom = $accounting_payments->getStartFrom($user_id);
			$startFrom = ($startFrom && strtotime($startFrom) > time()) ? strtotime($startFrom) : time();
			
			if ($product['product_type'] == 1) {

				$payment = array(
					'user_id' => $user_id,
					'product_id' => $product['id'],
					'quantity_id' => 1,
					'billing_date' => time(),
					'billing_processor' => 'paypal',
					'billing_amount' => $params['payment_gross'],
					'billing_total' => $params['payment_gross'],
					'billing_order_number' => $params['txn_id'],
					'billing_key' => $params['subscr_id'],
					'billing_status' => $params['payment_status'],
					'extra' => 'subscription',
					'date_start' => $startFrom,
					'date_end' => strtotime(date('Y-m-d H:i:s', $startFrom).' + '.$product['period'])
				);

				$payment_id = $accounting_payments->add($payment);
				$payment_info = $accounting_payments->getById($payment_id);

				$users->setAfterPayment($user_id, $params['payer_email'], $payment_info);

				$smarty->assign('subscribe_successful', 1);
			}
			else {
				$payment = array(
					'user_id' => $user_id,
					'product_id' => $product['id'],
					'quantity_id' => 1,
					'billing_date' => time(),
					'billing_processor' => 'paypal',
					'billing_amount' => $params['payment_gross'],
					'billing_total' => $params['payment_gross'],
					'billing_order_number' => $params['txn_id'],
					'billing_status' => $params['payment_status'],
					'extra' => 'payment',
					'date_start' => $startFrom,
					'date_end' => strtotime(date('Y-m-d H:i:s', $startFrom).' + '.$product['period'])
				);

				$payment_id = $accounting_payments->add($payment);
				$payment_info = $accounting_payments->getById($payment_id);

				$users->setAfterPayment($user_id, $params['payer_email'], $payment_info);

				$smarty->assign('payment_successful', 1);
			}
			
			return true;
		} else {
			return false;
		}

	}

}

?>