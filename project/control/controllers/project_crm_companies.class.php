<?php

/**
 * Project CRM companies class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class project_crm_companies extends controller {

	function project_crm_companies() {
		parent::controller("crm_companies");

		$this->fields = array(
			"created" => "created",
			"name" => "string",
			"code" => "string",
			"vat" => "string",
		);

		//$this->createTableStructure();
	}

}

?>