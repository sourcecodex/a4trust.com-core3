<?php

/**
 * Project Mailer emails class
 *
 * @package Tefo core3
 * @version 2017.05.09
 * @author Tefo <tefo05@gmail.com>
 */

class project_mailer_emails extends controller {

	function project_mailer_emails() {
		parent::controller("mailer_emails");

		$this->fields = array(
			"created" => "created",
			"hash" => "string",
			"type" => "string",
			"user_from" => "string",
			"user_name" => "string",
			"reply_to" => "string",
			"reply_to_name" => "string",
			"to" => "string",
			"subject" => "string",
			"body" => "text",
			"attachments" => "text",
			"images" => "text",
			"status" => "int",
			"sending_options" => "int",
			"priority" => "int",
			"opened" => "bool",
			"clicked" => "bool",
		);

		//$this->createTableStructure();
	}
	
	function add($form_data, $clean = true) {
		global $coreSQL;
		$item_id = parent::add($form_data, $clean);
		
		$mail_info = $this->getById($item_id);
		$body = str_replace("[id_hash]", md5($item_id), $mail_info['body']);
		$coreSQL->query("UPDATE `".$this->table."` SET `hash`='".md5($item_id)."', `body`='".addslashes($body)."' WHERE `id`=".(int)$item_id);
		
		return $item_id;
	}
	
	function setOpen($hash) {
		global $coreSQL;
		$coreSQL->query("UPDATE `".$this->table."` SET `opened`=1 WHERE `hash`='".addslashes($hash)."'");
	}
	
	function setClick($hash) {
		global $coreSQL;
		$coreSQL->query("UPDATE `".$this->table."` SET `clicked`=1 WHERE `hash`='".addslashes($hash)."'");
	}
	
	function addConfirmationEmail($form_data) {
		global $config, $smarty;

		$smarty->assign('config', $config);
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('lang', $form_data['default_lang']);
		$smarty->assign('code', $form_data['confirm_code']);
		$smarty->assign('hash', md5($form_data['password']));
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);

		$data = array();
		$data['type'] = 'Confirmation';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = $form_data['email'];
		$data['subject'] = $form_data['subject'];
		$data['body'] = $smarty->fetch('emails/confirmation.tpl');
		$data['status'] = 0;
		$data['priority'] = 0;
		
		$data['images'] = $form_data['images'];

		$this->add($data, false);
		
		return $data;
	}

	function addWelcomeEmail($form_data) {
		global $config, $smarty;

		$smarty->assign('config', $config);
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('lang', $form_data['default_lang']);
		$smarty->assign('code', $form_data['confirm_code']);
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);
		$smarty->assign('hash', $form_data['password']);

		$data = array();
		$data['type'] = 'Welcome';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = $form_data['email'];
		$data['subject'] = $form_data['subject'];
		$data['body'] = $smarty->fetch('emails/welcome.tpl');
		$data['status'] = 0;
		$data['priority'] = 1;
		
		$data['images'] = $form_data['images'];

		$this->add($data, false);
		
		return $data;
	}

	function addResetPasswordEmail($form_data) {
		global $config, $smarty;

		$smarty->assign('config', $config);
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('lang', $form_data['default_lang']);
		$smarty->assign('code', $form_data['confirm_code']);
		$smarty->assign('hash', $form_data['password']);
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);

		$data = array();
		$data['type'] = 'Reset Password';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = $form_data['email'];
		$data['subject'] = $form_data['subject'];
		$data['body'] = $smarty->fetch('emails/resetpass.tpl');
		$data['status'] = 0;
		$data['priority'] = 0;
		
		$data['images'] = $form_data['images'];

		$this->add($data, false);
		
		return $data;
	}

	function addContactEmail($form_data) {
		global $config, $smarty;

		$smarty->assign('subject', clean($form_data['subject']));
		$smarty->assign('message', clean($form_data['message']));
		$smarty->assign('name', clean($form_data['name']));
		$smarty->assign('phone', clean($form_data['phone']));
		$smarty->assign('email', clean($form_data['email']));
		$smarty->assign('user_ip', getIp());

		$data = array();
		$data['type'] = 'Contact us';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = $config['contacts_email'];
		$data['subject'] = $config['project_name'].' [Contact us]';
		$data['body'] = $smarty->fetch('emails/admin/contacts.tpl');
		$data['status'] = 0;
		$data['priority'] = 2;

		$this->add($data, false);
	}
	
	function addCancelAccountEmail($form_data) {
		global $config, $smarty;

		$smarty->assign('cancel_reason', clean($form_data['cancel_reason']));
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);
		$smarty->assign('last_name', $form_data['last_name']);
		$smarty->assign('plan', $form_data['plan']);

		$data = array();
		$data['type'] = 'Cancel Account';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = $config['contacts_email'];
		$data['subject'] = $config['project_name'].' [Account cancellation]';
		$data['body'] = $smarty->fetch('emails/admin/cancel_account.tpl');
		$data['status'] = 0;
		$data['priority'] = 2;

		$this->add($data, false);
	}

}

?>