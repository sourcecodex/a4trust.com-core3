<?php

/**
 * Project Mailer contacts class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class project_mailer_contacts extends controller {

	function project_mailer_contacts() {
		parent::controller("mailer_contacts");

		$this->fields = array(
			"created" => "created",
			"email" => "string",
			"last_sent" => "datetime",
			"sent_count" => "int",
			"user_id" => "int",
			"undelivered" => "int",
			"out_of_office" => "int",
			"ignore" => "bool",
			"unsubscribe" => "bool",
		);

		//$this->createTableStructure();
	}

	function is($email) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `email`='".addslashes($email)."'");
	}

	function addUniq($form_data) {
		global $coreSQL;

		$id = $this->is($form_data['email']);
		$user_id = (int)$coreSQL->queryValue("SELECT `id` FROM `users` WHERE `email`='".addslashes($form_data['email'])."'");

		if ($id) {
			$coreSQL->query("
				UPDATE `".$this->table."`
				SET `sent_count`=`sent_count`+1, `last_sent`='".date('Y-m-d H:i:s')."', `user_id`=".(int)$user_id."
				WHERE `id`=".(int)$id);
			return false;
		}
		else {
			$form_data['sent_count'] = 1;
			$form_data['last_sent'] = time();
			$form_data['user_id'] = (int)$user_id;
			$this->add($form_data);
			return true;
		}
	}

	function getByEmail($email) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `email`='".addslashes($email)."' LIMIT 1");
	}

}

?>