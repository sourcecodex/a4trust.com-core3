<?php

/**
 * coreImages class
 *
 * @package Tefo core2
 * @version 2011.08.11
 * @author Tefo <tefo05@gmail.com>
 */

class coreImages extends controller {

	/**
	 * Priskirtas kontroleris
	 *
	 * @var object
	 */
	var $assigned_controller;

	/**
	 * Laukas, kuriame saugomas image path
	 *
	 * @var string
	 */
	var $image_field;

	/**
	 * Direktorija, kurioje saugomi paveiksliukai
	 *
	 * @var string
	 */
	var $uploaddir;

	/**
	 * Talpinimo nustatymai
	 *
	 * @var array
	 */
	var $settings;

	/**
	 * Konstruktorius
	 *
	 * @param string $table
	 */
	function coreImages($table) {
		global $config;

		parent::controller($table);

		$this->fields = array (
			"created" => "created",
			"path" => "string",
			"name" => "string",
			"extension" => "string",
			"uploaddir" => "string",
			"item_id" => "int",
			"original_name" => "string",
			"width" => "int",
			"height" => "int",
			"size" => "int",
			"gallery_id" => "int",
			"category_id" => "int",
			"position" => "int",
		);

		$this->assigned_controller = null;
		$this->image_field = "image";
		$this->uploaddir = "data";
		$this->settings = array(
			"images" => array(
				"original" => array("original", 1000, 1000, true, false),
			)
		);
	}

	function insertImagesFromFiles() {
		global $coreSQL, $config;
		
		$items = scandir($config['root_dir'].$this->uploaddir);
		foreach ($items as $item_id) {
			if (($item_id != ".") && ($item_id != "..")) {
				$dir = $config['root_dir'].$this->uploaddir.'/'.$item_id;
				if (is_dir($dir)) {

					if (isEmptyDir($dir)) {
						// remove empty directories
						rmdir($dir);
					}
					else {
						$files = scandir($dir);
						foreach ($files as $file) {
							if (($file != ".") && ($file != "..")) {
								$file_parts = explode(".", $file);
								$name = $file_parts[0];
								$extension = ".".$file_parts[1];

								$uploadfile = $this->uploaddir.'/'.$item_id.'/'.$file;

								list ($width, $height) = getimagesize($config['root_dir'].$uploadfile);

								$data = array (
									"path" => $uploadfile,
									"name" => $name,
									"extension" => $extension,
									"uploaddir" => $this->uploaddir,
									"item_id" => $item_id,
									"original_name" => '',
									"width" => $width,
									"height" => $height,
									"size" => filesize($config['root_dir'].$uploadfile),
								);

								$image_row = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `path`='$uploadfile'");
								if ($image_row) {
									//$data['id'] = $image_row['id'];
									//$this->edit($data);
								}
								else $this->add($data);

							}
						}
					}

				}
			}
		}
	}

	function addImage($item_id) {
		global $coreSQL, $config;

		if (isset($_FILES[$this->image_field]['error']) && $_FILES[$this->image_field]['error'] == 0) {
			$tmp_name = $_FILES[$this->image_field]['tmp_name'];
			$original_name = $_FILES[$this->image_field]['name'];
			$original_type = $_FILES[$this->image_field]['type'];
			$original_size = $_FILES[$this->image_field]['size'];

			list ($original_width, $original_height, $original_image_type) = getimagesize($tmp_name);

			$extension = image_type_to_extension($original_image_type);

			if (!in_array($extension, array(".jpeg", ".jpg", ".png", ".gif"))) {
				// TODO error handling;
				return;
			}

			if (!file_exists($config['root_dir'].$this->uploaddir)) mkdir($config['root_dir'].$this->uploaddir, 0777);

			$uploaddir = $this->uploaddir.'/'.$item_id;
			if (!file_exists($config['root_dir'].$uploaddir)) mkdir($config['root_dir'].$uploaddir, 0777);

			if ($this->settings['images']) {
				foreach ($this->settings['images'] as $key => $img) {

					if ($img[4]) $extension = ".jpeg"; // output jpeg

					$uploadfile = $uploaddir.'/'.$img[0].$extension;

					$this->thumbnail($tmp_name, $config['root_dir'].$uploadfile, $img[1], $img[2], $img[3], $img[4]);

					list ($width, $height) = getimagesize($config['root_dir'].$uploadfile);

					$data = array (
						"path" => $uploadfile,
						"name" => $img[0],
						"extension" => $extension,
						"uploaddir" => $this->uploaddir,
						"item_id" => $item_id,
						"original_name" => $original_name,
						"width" => $width,
						"height" => $height,
						"size" => filesize($config['root_dir'].$uploadfile),
					);

					$image_row = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `path`='$uploadfile'");
					if ($image_row) {
						$data['id'] = $image_row['id'];
						$this->edit($data);
					}
					else $this->add($data);

					if ($key == "original") {
						if ($this->assigned_controller) {
							if (isset($this->assigned_controller->table) &&
								in_array($this->image_field, array_keys($this->assigned_controller->fields))) {

								$coreSQL->query("UPDATE `".$this->assigned_controller->table."` SET `".$this->image_field."`='$uploadfile' WHERE `id`=".(int)$item_id);
							}
						}
					}
				}
			}

		}
		else {
			// error output
		}

	}

	function addImage2($item_id, $filename) {
		global $coreSQL, $config;

		if (file_exists($filename)) {
			$tmp_name = $filename;
			$original_name = preg_replace('/^.+\\\\/', '', $filename);
			
			//$original_type = $_FILES[$this->image_field]['type'];
			//$original_size = $_FILES[$this->image_field]['size'];

			list ($original_width, $original_height, $original_image_type) = getimagesize($tmp_name);

			$extension = image_type_to_extension($original_image_type);

			if (!in_array($extension, array(".jpeg", ".jpg", ".png", ".gif"))) {
				// TODO error handling;
				return;
			}

			if (!file_exists($config['root_dir'].$this->uploaddir)) mkdir($config['root_dir'].$this->uploaddir, 0777);

			$uploaddir = $this->uploaddir.'/'.$item_id;
			if (!file_exists($config['root_dir'].$uploaddir)) mkdir($config['root_dir'].$uploaddir, 0777);

			if ($this->settings['images']) {
				foreach ($this->settings['images'] as $key => $img) {

					if ($img[4]) $extension = ".jpeg"; // output jpeg

					$uploadfile = $uploaddir.'/'.$img[0].$extension;

					$this->thumbnail($tmp_name, $config['root_dir'].$uploadfile, $img[1], $img[2], $img[3], $img[4]);

					list ($width, $height) = getimagesize($config['root_dir'].$uploadfile);

					$data = array (
						"path" => $uploadfile,
						"name" => $img[0],
						"extension" => $extension,
						"uploaddir" => $this->uploaddir,
						"item_id" => $item_id,
						"original_name" => $original_name,
						"width" => $width,
						"height" => $height,
						"size" => filesize($config['root_dir'].$uploadfile),
					);

					$image_row = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `path`='$uploadfile'");
					if ($image_row) {
						$data['id'] = $image_row['id'];
						$this->edit($data);
					}
					else $this->add($data);

					if ($key == "original") {
						if ($this->assigned_controller) {
							if (isset($this->assigned_controller->table) &&
								in_array($this->image_field, array_keys($this->assigned_controller->fields))) {

								$coreSQL->query("UPDATE `".$this->assigned_controller->table."` SET `".$this->image_field."`='$uploadfile' WHERE `id`=".(int)$item_id);
							}
						}
					}
				}
			}

		}
		else {
			// error output
		}

	}

	function addImages() {

	}

	function editImage($item_id) {
		global $coreSQL, $config;

		if (isset($_FILES[$this->image_field]['error']) && $_FILES[$this->image_field]['error'] == 0) {
			$tmp_name = $_FILES[$this->image_field]['tmp_name'];
			$original_name = $_FILES[$this->image_field]['name'];
			$original_type = $_FILES[$this->image_field]['type'];
			$original_size = $_FILES[$this->image_field]['size'];

			list ($original_width, $original_height, $original_image_type) = getimagesize($tmp_name);

			$extension = image_type_to_extension($original_image_type);

			if (!in_array($extension, array(".jpeg", ".jpg", ".png", ".gif"))) {
				// TODO error handling;
				return;
			}

			if (!file_exists($config['root_dir'].$this->uploaddir)) mkdir($config['root_dir'].$this->uploaddir, 0777);

			$uploaddir = $this->uploaddir.'/'.$item_id;
			if (!file_exists($config['root_dir'].$uploaddir)) mkdir($config['root_dir'].$uploaddir, 0777);

			if ($this->settings['images']) {
				foreach ($this->settings['images'] as $key => $img) {

					if ($img[4]) $extension = ".jpeg"; // output jpeg

					$uploadfile = $uploaddir.'/'.$img[0].$extension;

					// delete old file if exists
					if (file_exists($config['root_dir'].$uploadfile)) unlink($config['root_dir'].$uploadfile);
					$this->thumbnail($tmp_name, $config['root_dir'].$uploadfile, $img[1], $img[2], $img[3], $img[4]);

					list ($width, $height) = getimagesize($config['root_dir'].$uploadfile);

					$data = array (
						"path" => $uploadfile,
						"name" => $img[0],
						"extension" => $extension,
						"uploaddir" => $this->uploaddir,
						"item_id" => $item_id,
						"original_name" => $original_name,
						"width" => $width,
						"height" => $height,
						"size" => filesize($config['root_dir'].$uploadfile),
					);

					$image_row = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `path`='$uploadfile'");
					if ($image_row) {
						$data['id'] = $image_row['id'];
						$this->edit($data);
					}
					else $this->add($data);

					if ($key == "original") {
						if ($this->assigned_controller) {
							if (isset($this->assigned_controller->table) &&
								in_array($this->image_field, array_keys($this->assigned_controller->fields))) {

								$coreSQL->query("UPDATE `".$this->assigned_controller->table."` SET `".$this->image_field."`='$uploadfile' WHERE `id`=".(int)$item_id);
							}
						}
					}
				}
			}
			
		}
		else {
			// error output
		}

	}

	function deleteImage($item_id) {
		global $coreSQL, $config;

		$uploaddir = $this->uploaddir.'/'.(int)$item_id;

		if ($this->settings['images']) {

			foreach ($this->settings['images'] as $key => $img) {
				if ($key == "original") {
					if ($this->assigned_controller) {
						if (isset($this->assigned_controller->table) &&
							in_array($this->image_field, array_keys($this->assigned_controller->fields))) {

							$coreSQL->query("UPDATE `".$this->assigned_controller->table."` SET `".$this->image_field."`='' WHERE `id`=".(int)$item_id);
						}
					}
				}

				if (file_exists($config['root_dir'].$uploaddir)) {
					$image_info = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `uploaddir`='$this->uploaddir' AND `item_id`=".(int)$item_id." AND `name`='".$img[0]."'");
					if ($image_info) {
						if (file_exists($config['root_dir'].$image_info['path'])) unlink($config['root_dir'].$image_info['path']);
					}
				}

			}

			$coreSQL->query("DELETE FROM `".$this->table."` WHERE `uploaddir`='$this->uploaddir' AND `item_id`=".(int)$item_id);

		}
		
		if (file_exists($config['root_dir'].$uploaddir)) rmdir($config['root_dir'].$uploaddir);
	}

	function getImages($item_id) {
		global $coreSQL;
		return $coreSQL->queryData("SELECT * FROM `".$this->table."` WHERE `uploaddir`='$this->uploaddir' AND `item_id`=".(int)$item_id, "name");
	}

	function getPathByName($item_id, $name) {
		global $coreSQL;
		return $coreSQL->queryValue("SELECT `path` FROM `".$this->table."` WHERE `uploaddir`='$this->uploaddir' AND `item_id`=".(int)$item_id." AND `name`='$name'");
	}

	function countByName($name) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE `uploaddir`='$this->uploaddir' AND `name`='$name'");
	}

	function countGreaterByName($name, $field, $value) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE `uploaddir`='$this->uploaddir' AND `name`='$name' AND `$field`>'$value'");
	}

	function countLessByName($name, $field, $value) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE `uploaddir`='$this->uploaddir' AND `name`='$name' AND `$field`<'$value'");
	}

	function search($filter) {

		$search_sql = " AND `uploaddir`='$this->uploaddir'";

		return parent::search2($filter, $search_sql, "", true, true);
	}

	/**
	* Sukuria sumažintą paveiksliuko kopiją
	*
	* @param resource $original - paveiksliukas, kurio kopija daroma
	* @param resource $thumbnail - kur išsaugoti kopiją
	* @param int $width - kopijos plotis
	* @param int $height - kopijos aukštis
	* @param bool $proportion - ar išlaikyti proporcingus aukštį ir plotį
	* @param bool $output_jpeg - gražina jpeg formatu visada
	*/
	function thumbnail($original, $thumbnail, $width, $height, $proportion, $output_jpeg = false)
	{
		list ($width_orig, $height_orig) = getimagesize($original);

		if ($proportion) {
			if ($width && ($width_orig < $height_orig)) {
				$width = ($height / $height_orig) * $width_orig;
			}
			else {
				$height = ($width / $width_orig) * $height_orig;
			}
		}

		// !!! nesukuria didesnio nei orginalas
		if ($width_orig < $width) $width = $width_orig;
		if ($height_orig < $height) $height = $height_orig;

		$image_p = imagecreatetruecolor($width, $height);
		imagealphablending($image_p, false);
		imagesavealpha($image_p, true);

		$imageType = exif_imagetype($original);
		$image;
		switch ($imageType){
			case IMAGETYPE_GIF:
				$image = imagecreatefromgif($original);
			break;
			case IMAGETYPE_JPEG:
				$image = imagecreatefromjpeg($original);
			break;
			case IMAGETYPE_PNG:
				$image = imagecreatefrompng($original);
			break;
			default:
				$image = imagecreatetruecolor(1, 1);
		}

		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

		if ($output_jpeg) {

			switch ($imageType){
				case IMAGETYPE_GIF:
					imagejpeg($image_p, $thumbnail, 100);
				break;
				case IMAGETYPE_JPEG:
					imagejpeg($image_p, $thumbnail, 100);
				break;
				case IMAGETYPE_PNG:
					imagejpeg($image_p, $thumbnail, 100);
				break;
				default:
					// jei ne gif, jpeg ar png neishsaugo paveiksliuko
			}

		}
		else {

			switch ($imageType){
				case IMAGETYPE_GIF:
					imagegif($image_p, $thumbnail);
				break;
				case IMAGETYPE_JPEG:
					imagejpeg($image_p, $thumbnail, 100);
				break;
				case IMAGETYPE_PNG:
					imagepng($image_p, $thumbnail, 9);
				break;
				default:
					// jei ne gif, jpeg ar png neishsaugo paveiksliuko
			}

		}

		imagedestroy($image_p);

		return;
	}

	/**
	 * Pasuka paveiksliuka nurodytu kampu
	 *
	 * @param resource $original
	 * @param int $angle
	 */
	function rotate_image($original, $angle) {

		$imageType = exif_imagetype($original);
		$image;
		switch ($imageType){
			case IMAGETYPE_GIF:
				$image = imagecreatefromgif($original);
			break;
			case IMAGETYPE_JPEG:
				$image = imagecreatefromjpeg($original);
			break;
			case IMAGETYPE_PNG:
				$image = imagecreatefrompng($original);
			break;
		}

		$rotated = imagerotate($image, $angle, 0);

		switch ($imageType){
			case IMAGETYPE_GIF:
				imagegif($rotated, $original);
			break;
			case IMAGETYPE_JPEG:
				imagejpeg($rotated, $original, 100);
			break;
			case IMAGETYPE_PNG:
				imagepng($rotated, $original, 9);
			break;
			default:
				// jei ne gif, jpeg ar png neishsaugo paveiksliuko
		}

		imagedestroy($rotated);

		return;
	}

}

?>