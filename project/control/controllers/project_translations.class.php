<?php

/**
 * Project translations class
 * 
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com> 
 */

class project_translations extends controller {

	var $translations;
	
	function project_translations() {

		parent::controller("translations");
		
		$this->fields = array (
			"created" => "created",
			"updated" => "datetime",
			"lid" => "string",
			"t" => "text",
		);

	}

	function setTranslations() {
		global $languages;

		foreach ($languages->active_langs as $lang) {
			$this->fields[$lang] = "text";
		}
	}

	function getTranslations() {
		global $coreSQL, $coreSession;

		$lang = $coreSession->session['lang'];
		$data = $coreSQL->queryDataCached("SELECT `lid`, `$lang` AS `value` FROM `".$this->table."`");

		$this->translations = array();
		foreach ($data as $field) {
			$this->translations[$field['lid']] = $field['value'];
		}

		return true;
	}
	
	function t($value, $template = '') {
		global $coreSQL, $config;

		if (isset($this->translations[md5($value)])) {

			// Additional info, where translation placed
			//$this->setTranslationPages($value);

			//$this->updateTranslation($value, $template);
		
			return $this->translations[md5($value)];
		}
		else {
			$tt = $coreSQL->queryRow("SELECT `id` FROM `".$this->table."` WHERE `lid`='".md5($value)."' LIMIT 1");
			if (empty($tt)) {
				$this->add(array(
					"updated" => time(),
					"lid" => md5($value),
					"t" => $value,
					$config['code_lang'] => $value,
				));
			}
			return $value;
		}
	}

	function updateTranslation($value, $template) {
		global $coreSQL;

		$is_translation = (int)$coreSQL->queryValue("SELECT `id` FROM `translations_structure`
												WHERE `t`='".addslashes($value)."' AND `tpl`='".$template."' LIMIT 1");

		if ($is_translation == 0) {

			$count = (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `translations_structure` WHERE `tpl`='".$template."'");

			$coreSQL->query("INSERT INTO `translations_structure`
							(`t`, `tpl`, `pos`) VALUES
							('".addslashes($value)."', '".$template."', '".$count."')");

			$coreSQL->query("UPDATE `".$this->table."` SET
				`t`='".addslashes($value)."',
				`updated`='".date('Y-m-d H:i:s')."'
				WHERE `lid`='".md5($value)."' LIMIT 1");
		}
	}

	/*
	function getTranslations2() {
		global $coreSQL;

		$this->translations2 = array();
		$data = $coreSQL->queryDataCached("SELECT * FROM `".$this->table."` WHERE `t2`=1");

		foreach ($data as $field) {
			foreach ($this->active_langs as $lang) {
				$this->translations2[$field['lid']][$lang] = $field[$lang];
			}
		}

		return true;
	}

	function t2($value, $lang, $template = '') {
		global $coreSQL, $config;

		if (isset($this->translations2[md5($value)][$lang])) {

			$this->updateTranslation($value, $template);

			return $this->translations2[md5($value)][$lang];
		}
		else {
			$tt = $coreSQL->queryRow("SELECT `id` FROM `".$this->table."` WHERE `lid`='".md5($value)."' LIMIT 1");
			if (empty($tt)) {
				$coreSQL->query("INSERT INTO `".$this->table."` (`lid`, `".$config['code_lang']."`, `t2`) VALUES ('".md5($value)."', '".$value."', '1')");
			}
			else {
				$coreSQL->query("UPDATE `".$this->table."` SET `t2`=1 WHERE `lid`='".md5($value)."' LIMIT 1");
			}
			return $value."__";  // parodoma kad buvo itrauktas i translations
		}
	}
	*/
	
}

?>