<?php

/**
 * Project Mailer users class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class project_mailer_users extends controller {

	function project_mailer_users() {
		parent::controller("mailer_users");

		$this->fields = array(
			"user" => "string",
			"password" => "string",
			"from_email" => "string",
			"from_name" => "string",
		);

		//$this->createTableStructure();
	}

}

?>