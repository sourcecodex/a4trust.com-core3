<?php

/**
 * Project popups class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class project_popups extends controller {

	var $popups;

	function project_popups() {
		parent::controller("popups");

		$this->fields = array(
			"created" => "created",
			"admin" => "bool",
			"name" => "string",
			"default" => "bool",
		);

	}

	function setPopups() {
		global $coreSQL, $coreSession;

		$db_popups = $coreSQL->queryDataCached("SELECT * FROM `".$this->table."` WHERE `admin`=0");

		$this->popups = array();
		foreach ($db_popups as $popup) {
			$this->popups[$popup['name']] = $popup;
		}
	}

}

?>