<?php

/**
 * Project languages class
 * 
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com> 
 */

class project_languages extends controller {

	var $active_langs;

	function project_languages() {

		parent::controller("languages");
		
		$this->fields = array (
			"lang" => "string",
			"name" => "string",
			"active" => "bool",
			"pos" => "int",
		);

	}

	function createTableStructure() {
		global $coreSQL;

		parent::createTableStructure();

		$coreSQL->query("INSERT INTO `".$this->table."`
			(`id`, `lang`, `name`, `active`, `pos`) VALUES
			(1, 'en', 'English', 1, 1),
			(2, 'ru', 'Russian', 1, 2),
			(3, 'lt', 'Lithuanian', 1, 3);");
	}

	/**
	 * Nustato sesijoj kalba pagal GET, jei GET tuscias nustato default kalba
	 *
	 */
	function setLanguage() {
		global $config, $coreSession;
		
		if (!empty($_POST['lang']) && empty($_GET['lang'])) { $_GET['lang'] = $_POST['lang']; }
		
		if (!empty($_GET['lang'])) {
			if (in_array($_GET['lang'], $this->active_langs)) {
				$coreSession->session['lang'] = $_GET['lang'];
				setcookie('cookieLang', $_GET['lang'], time()+60*60*24*7, $config['cookie_path']);
			}
			else {
				$coreSession->session['lang'] = $config['default_lang'];
			}
		}
		elseif (isset($_COOKIE['cookieLang']) && !empty($_COOKIE['cookieLang'])) {
			if (in_array($_COOKIE['cookieLang'], $this->active_langs)) {
				$coreSession->session['lang'] = $_COOKIE['cookieLang'];
			}
			else {
				$coreSession->session['lang'] = $config['default_lang'];
			}
		}
		elseif (empty($coreSession->session['lang'])) {
			$coreSession->session['lang'] = $config['default_lang'];
		}
	}
	
	/**
	 * Nustato sesijoj turinio (administruojama) kalba pagal GET, jei GET tuscias nustato default kalba
	 *
	 */
	function setContentLanguage() {
		global $config, $coreSession;
		
		if (!empty($_POST['content_lang']) && empty($_GET['content_lang'])) {
			$_GET['content_lang'] = $_POST['content_lang'];
		}
		
		if (!empty($_GET['content_lang'])) {
			if (in_array($_GET['content_lang'], $this->active_langs)) {
				$coreSession->session['content_lang'] = $_GET['content_lang'];
			}
			else {
				$coreSession->session['content_lang'] = $config['default_lang'];
			}
		}
		elseif (empty($coreSession->session['content_lang'])) {
			$coreSession->session['content_lang'] = $config['default_lang'];
		}
	}
	
	function getActiveLangs() {
		global $coreSQL;

		$data = $coreSQL->queryDataCached("SELECT `lang` FROM `".$this->table."` WHERE `active`=1 ORDER BY `pos`");
		
		$result = array();
		foreach ($data as $field) {
			$result[] = $field['lang'];
		}
		
		return $result;
	}
	
}

?>