<?php

/**
 * Project pages class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class project_pages extends controller {

	/**
	 * uzklausos URL
	 *
	 * @var string
	 */
	var $request_url;

	/**
	 * URL suskirstytas i dalis
	 *
	 * @var array
	 */
	var $path;

	/**
	 * uzklausos URL be parametru
	 *
	 * @var string
	 */
	var $url;

	/**
	 * puslapis kuri atitinka uzklausos URL
	 *
	 * @var array
	 */
	var $current_page;

	/**
	 * Ar surastas puslapis, kuri uzkrauti
	 *
	 * @var bool
	 */
	var $page_found;

	/**
	 * Assign filename, kuri includinti
	 *
	 * @var string
	 */
	var $include_assign;

	/**
	 * Template filename, kuri includinti
	 *
	 * @var string
	 */
	var $include_page, $include_tpl;

	/**
	 * Error tpl, kuri includinti
	 *
	 * @var string
	 */
	var $error_tpl;


	function project_pages() {
		global $config;
		
		parent::controller("pages");
		
		$this->fields = array(
			"created" => "created",
			"admin" => "bool",
			"type" => "string",
			"layout" => "string",
			"path" => "string",
			"page_title" => "string",
			"page_description" => "text",
			"page_keywords" => "string",
		);

		$this->request_url = "";
		$this->url = "";
		$this->path = array();
		$this->current_page = array();
		$this->page_found = false;

		$this->include_assign = "assign/pages/index.assign.php";
		$this->include_page = "pages/index";
		$this->include_tpl = $this->include_page.$config['templates_extension'];
		$this->error_tpl = "error404".$config['templates_extension'];

	}

	function setPages() {
		global $coreSession;
		
		$this->request_url = substr($_SERVER['REQUEST_URI'], 1);

		$pos = strpos($this->request_url, "?");

		if ($pos !== false) {
			$get_params = substr($this->request_url, $pos+1);
			parse_str($get_params, $_GET);
			$this->url = substr($this->request_url, 0, $pos);
			
			$this->request_url = str_replace("&xajax_call=1", "", $this->request_url);
			$this->request_url = str_replace("?xajax_call=1", "", $this->request_url);
		}
		else {
			$this->url = $this->request_url;
		}

		$this->path = explode("/", $this->url);
		
		if (count($this->path) > 0) {
			if ($this->path[0] == "lt") {
				$coreSession->session['lang'] = "lt";
				
				// remove first element
				array_shift($this->path);
			}
		}

		$this->setCurrentPage($this->path);

	}

	function setCurrentPage($path_array) {
		global $config, $coreSQL;

		while (count($path_array) > 0) {

			$path = implode("/", $path_array);

			$this->current_page = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `admin`=0 AND `path`='".addslashes($path)."'");

			if ($this->current_page) {
				$this->page_found = true;
				$this->include_assign = "assign/pages/".($path == ""?"index":$path).".assign.php";
				$this->include_page = "pages/".($path == ""?"index":$path);
				$this->include_tpl = $this->include_page.$config['templates_extension'];
				break;
			}

			array_pop($path_array);
		}
	}
	
	function createTableStructure() {
		global $coreSQL, $config;

		parent::createTableStructure();

		// insert title paths
		/*$coreSQL->query("INSERT INTO `".$this->table."`
			(`id`, `created`, `admin`, `type`, `layout`, `path`, `page_title`)
			VALUES (NULL , NOW(), '1', 'core', 'admin', '', 'Admin ".$config['project_name']."'),
				   (NULL , NOW(), '0', 'smarty', 'page', '', '".$config['project_name']."');");*/

	}

	

	

	/*
	function getPathsByParent($parent) {
		global $coreSQL;
		return $coreSQL->queryColumn("SELECT `id`, `path` FROM `".$this->table."` WHERE `parent`=".(int)$parent, "path", "id");
	}

	function insertPath($parent, $path) {
		global $coreSQL;
		// TODO check if not exists
		$coreSQL->query("INSERT INTO `".$this->table."` (`parent`, `path`) VALUES ('".(int)$parent."', '".$path."');");
	}

	function checkTableByFiles($current_path, $current_parent) {
		global $config, $coreSession;

		// Check tree paths by views files

		$root_dir = $config['views_dir']."html/root";

		$files = scandir($root_dir.$current_path);
		$paths = $this->getPathsByParent($current_parent);

		foreach ($files as $file) {
			if (($file != ".") && ($file != "..") && ($file != ".svn") && ($file != "index".$config['templates_extension']) ) {
				if (is_dir($root_dir.$current_path.$file)) {
					if (!in_array($file, $paths)) {
						$coreSession->session['checker_logs'][] = "Missing path ".$current_path.$file." in table `".$this->table."`";
						if (isset($config['checker_sync_tree'])) {
							$this->insertPath($current_parent, $file);
							$coreSession->session['checker_logs'][] = "Added!";
						}
					}
					else {
						$this->checkTableByFiles($current_path.$file."/", array_search($file, $paths));
					}
				}
				else {
					if (strpos($file, $config['templates_extension']) !== false) {
						$file = substr($file, 0, -strlen($config['templates_extension']));
						if (is_dir($root_dir.$current_path.$file)) {
							// do nothing
						}
						else {
							if (!in_array($file, $paths)) {
								$coreSession->session['checker_logs'][] = "Missing path ".$current_path.$file." in table `".$this->table."`";
								if (isset($config['checker_sync_tree'])) {
									$this->insertPath($current_parent, $file);
									$coreSession->session['checker_logs'][] = "Added!";
								}
							}
						}
					}
					else {
						$coreSession->session['checker_logs'][] = "Invalid template extension ".$current_path.$file;
					}
				}
				//$coreSession->session['checker_logs'][] = ">> ".$current_path.$file;
			}
		}

	}

	function checkFilesByTable($current_path, $current_parent) {
		global $config, $coreSession;

		// Check views files by tree paths

		$root_dir = $config['views_dir']."html/root";

		$files = scandir($root_dir.$current_path);
		$paths = $this->getPathsByParent($current_parent);

		foreach ($paths as $key => $path) {
			$this->checker_paths[$key] = $path;
			
			if (in_array($path, $files) || in_array($path.$config['templates_extension'], $files)) {
				if (is_dir($root_dir.$current_path.$path)) {
					$this->checkFilesByTable($current_path.$path."/", array_search($path, $paths));
				}
			}
			else {
				$coreSession->session['checker_logs'][] = "Missing views file ".$current_path.$path.$config['templates_extension']." or overhead path id: ".$key." in table `".$this->table."`";
			}
		}

	}

	function checkAssignFilesOverhead($current_path, $current_parent) {
		global $config, $coreSession;

		// Check assign files overhead by tree paths

		$root_dir = $config['control_dir']."assign";
		$files = scandir($root_dir.$current_path);

		$paths = $this->getPathsByParent($current_parent);

		foreach ($files as $file) {
			if (($file != ".") && ($file != "..") && ($file != ".svn") && ($file != "assign.php") && ($file != "_global.php") && ($file != "_global_after.php") ) {
				if (is_dir($root_dir.$current_path.$file)) {
					if (!in_array($file, $paths)) {
						$coreSession->session['checker_logs'][] = "Overhead assign directory ".$current_path.$file."";
					}
					else {
						$this->checkAssignFilesOverhead($current_path.$file."/", array_search($file, $paths));
					}
				}
				else {
					$file = substr($file, 0, -strlen(".assign.php"));
					if (!in_array($file, $paths)) {
						$coreSession->session['checker_logs'][] = "Overhead assign file ".$current_path.$file.".assign.php";
					}
				}
			}
		}

	}
	*/

}

?>