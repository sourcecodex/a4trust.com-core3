<?php

/**
 * Project accounting products class
 *
 * @package Tefo core3
 * @version 2014.05.21
 * @author Tefo <tefo05@gmail.com>
 */

class project_accounting_products extends controller {

    function project_accounting_products() {
		parent::controller("accounting_products");

		$this->fields = array (
			"amount" => "float",
			"currency" => "string",
			"period" => "string",
			"period_in_days" => "int",
			"plan" => "string",
			"description" => "string",
			"description_public" => "string",
			"product_type" => "int",
			"ccbill_product_id" => "int",
		);
		
		//$this->createTableStructure();
	}
	
	function getAllByType($product_type) {
		global $coreSQL;
		
		return $coreSQL->queryData("
			SELECT * FROM `".$this->table."`
			WHERE `product_type`=".(int)$product_type."
			ORDER BY `".$this->table."`.`amount` DESC");
	}

	function generatePaypalSubscriptionForm($id, $form_name, $business) {
		global $config, $users, $paypal;

		$product = $this->getById($id);

		$invoice_random_id = substr(strtoupper(md5(uniqid(rand()))), 0, 5);

		$result = '<form action="https://'.($paypal->dev_mode ? 'www.sandbox.paypal.com' : 'www.paypal.com').'/cgi-bin/webscr" method="post" name="'.$form_name.'" />
						<input type="hidden" name="cmd" value="_xclick'.($product['product_type'] == 1?'-subscriptions':'').'" />
						<input type="hidden" name="business" value="'.$business.'" />
						<input type="hidden" name="item_name" value="'.$product['description'].'" />
						<input type="hidden" name="item_number" value="'.$product['id'].'" />
						<input type="hidden" name="no_shipping" value="1" />
						<input type="hidden" name="no_note" value="1" />
						<input type="hidden" name="currency_code" value="'.$product['currency'].'" />
						<input type="hidden" name="lc" value="US" />';
		if ($product['product_type'] == 1) {
			$result .= '<input type="hidden" name="bn" value="PP-SubscriptionsBF" />
						<input type="hidden" name="a3" value="'.$product['amount'].'" />
						<input type="hidden" name="p3" value="'.$product['period_in_days'].'" />
						<input type="hidden" name="t3" value="D" />
						<input type="hidden" name="src" value="1" />
						<input type="hidden" name="sra" value="1" />
						<input type="hidden" name="return" value="'.$config['site_url'].'paypal/return" />
						<input type="hidden" name="cancel_return" value="'.$config['site_url'].'paypal/cancel" />
						<input type="hidden" name="notify_url" value="'.$config['site_url'].'paypal/ipn" />';
		}
		else {
			$result .= '<input type="hidden" name="amount" value="'.$product['amount'].'" />
						<input type="hidden" name="rm" value="2" />
						<input type="hidden" name="return" value="'.$config['site_url'].'paypal/return" />
						<input type="hidden" name="cancel_return" value="'.$config['site_url'].'paypal/cancel" />
						<input type="hidden" name="notify_url" value="'.$config['site_url'].'paypal/ipn" />';
		}
						
		$result .= '	<input type="hidden" name="invoice" value="'.$users->id.'-'.$invoice_random_id.'" />
					</form>';

		return $result;
	}
	
	function generatePaypalSubscriptionForm2($id, $form_name, $business, $period_type, $period_length) {
		global $config, $users, $paypal;

		$product = $this->getById($id);

		$invoice_random_id = substr(strtoupper(md5(uniqid(rand()))), 0, 5);

		$result = '<form action="https://'.($paypal->dev_mode ? 'www.sandbox.paypal.com' : 'www.paypal.com').'/cgi-bin/webscr" method="post" name="'.$form_name.'" />
						<input type="hidden" name="cmd" value="_xclick'.($product['product_type'] == 1?'-subscriptions':'').'" />
						<input type="hidden" name="business" value="'.$business.'" />
						<input type="hidden" name="item_name" value="'.$product['description'].'" />
						<input type="hidden" name="item_number" value="'.$product['id'].'" />
						<input type="hidden" name="no_shipping" value="1" />
						<input type="hidden" name="no_note" value="1" />
						<input type="hidden" name="currency_code" value="'.$product['currency'].'" />
						<input type="hidden" name="lc" value="US" />';
		if ($product['product_type'] == 1) {
			$result .= '<input type="hidden" name="bn" value="PP-SubscriptionsBF" />
						<input type="hidden" name="a3" value="'.$product['amount'].'" />
						<input type="hidden" name="p3" value="'.$period_length.'" />
						<input type="hidden" name="t3" value="'.$period_type.'" />
						<input type="hidden" name="src" value="1" />
						<input type="hidden" name="sra" value="1" />
						<input type="hidden" name="return" value="'.$config['site_url'].'paypal/return" />
						<input type="hidden" name="cancel_return" value="'.$config['site_url'].'paypal/cancel" />
						<input type="hidden" name="notify_url" value="'.$config['site_url'].'paypal/ipn" />';
		}
		else {
			$result .= '<input type="hidden" name="amount" value="'.$product['amount'].'" />
						<input type="hidden" name="rm" value="2" />
						<input type="hidden" name="return" value="'.$config['site_url'].'paypal/return" />
						<input type="hidden" name="cancel_return" value="'.$config['site_url'].'paypal/cancel" />
						<input type="hidden" name="notify_url" value="'.$config['site_url'].'paypal/ipn" />';
		}
						
		$result .= '	<input type="hidden" name="invoice" value="'.$users->id.'-'.$invoice_random_id.'" />
					</form>';

		return $result;
	}
	
	function generateCheckoutSubscriptionForm($id, $form_name, $business) {
		global $users;

		$invoice_random_id = substr(strtoupper(md5(uniqid(rand()))), 0, 5);
		
		$result = '<form action="https://www.2checkout.com/checkout/purchase" method="post" name="'.$form_name.'" />
						<input type="hidden" name="sid" value="'.$business.'" />
						<input type="hidden" name="quantity" value="1" />
						<input type="hidden" name="product_id" value="'.$id.'" />
						<input type="hidden" name="subscription_id" value="'.$users->id.'-'.$invoice_random_id.'" />
					</form>';

		return $result;
	}

}

?>