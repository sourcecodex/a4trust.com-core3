<?php

/**
 * Project options class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class project_options extends controller {

	function project_options() {
		parent::controller("options");

		$this->fields = array(
			"name" => "string",
			"value" => "text",
		);
	}

}

?>