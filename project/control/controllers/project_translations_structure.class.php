<?php

/**
 * Project translations structure class
 * 
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com> 
 */

class project_translations_structure extends controller {

	function project_translations_structure() {

		parent::controller("translations_structure");
		
		$this->fields = array (
			"t" => "text",
			"tpl" => "string",
			"pos" => "int",
		);

	}
	
}

?>