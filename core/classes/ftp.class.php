<?php

/**
 * FTP class
 * 
 * @package Tefo core2
 * @version 2010.05.05
 * @author Tefo <tefo05@gmail.com> 
 */

class coreFTP {
	
	var $user;
	var $password;
	var $upload_dir;
	
	function coreFTP() {
		global $config;
		$this->user = isset($config['ftp_root_user'])?$config['ftp_root_user']:"";
		$this->password = isset($config['ftp_root_password'])?$config['ftp_root_password']:"";
		$this->upload_dir = isset($config['ftp_upload_dir'])?$config['ftp_upload_dir']:"";
	}
	
	function create_file($dirname, $filename, $data, $mkdir = false) {
		global $config;

		$ftpstream = ftp_connect('localhost');
		$login = ftp_login($ftpstream, $this->user, $this->password);

		if ($login) {
			if ($mkdir) @ftp_mkdir($ftpstream, $this->upload_dir.'/'.$dirname);

			$temp = tmpfile();
			ftp_fput($ftpstream, $this->upload_dir.'/'.$dirname.'/'.$filename, $temp, FTP_ASCII);

			ftp_site($ftpstream, 'CHMOD 0777 '.$this->upload_dir.'/'.$dirname.'/'.$filename);
			$fp = fopen($config['main_dir'].$dirname.'/'.$filename, 'w') or die("can't open file");
			fputs($fp, $data);
			fclose($fp);
			ftp_site($ftpstream, 'CHMOD 0644 '.$this->upload_dir.'/'.$dirname.'/'.$filename);
		}
		ftp_close($ftpstream);
	}

	function make_dir($dirname, $mode = null) {
		$ftpstream = ftp_connect('localhost');
		$login = ftp_login($ftpstream, $this->user, $this->password);

		if ($login) {
			ftp_mkdir($ftpstream, $this->upload_dir.'/'.$dirname);
			if ($mode) ftp_chmod($ftpstream, $mode, $this->upload_dir.'/'.$dirname);
		}
		ftp_close($ftpstream);
	}

	function copy_file($source_file, $dest_file) {

		$ftpstream = ftp_connect('localhost');
		$login = ftp_login($ftpstream, $this->user, $this->password);

		if ($login) {

			$temp = tempnam(sys_get_temp_dir(), 'Tux');

			if (ftp_get($ftpstream, $temp, $this->upload_dir.'/'.$source_file, FTP_BINARY)) {
				if (ftp_put($ftpstream, $this->upload_dir.'/'.$dest_file, $temp, FTP_BINARY)) {
					unlink($temp);
				} else {
					return false;
				}
			} else {
				return false;
			}
			
			return true;

		}
		ftp_close($ftpstream);
	}

	function put_file($data, $filename) {
		$ftpstream = ftp_connect('localhost');
		$login = ftp_login($ftpstream, $this->user, $this->password);

		if ($login) {

			$temp = tmpfile();
			$meta_data = stream_get_meta_data($temp);

			file_put_contents($meta_data["uri"], $data);
			
			if (ftp_put($ftpstream, $this->upload_dir.'/'.$filename, $meta_data["uri"], FTP_BINARY)) {
				unlink($meta_data["uri"]);
			} else {
				return false;
			}

			return true;

		}
		ftp_close($ftpstream);
	}

	function delete_file($filename) {
		$ftpstream = ftp_connect('localhost');
		$login = ftp_login($ftpstream, $this->user, $this->password);

		if ($login) {
			 ftp_delete($ftpstream, $this->upload_dir.'/'.$filename);
		}
		ftp_close($ftpstream);
	}

	
}

?>
