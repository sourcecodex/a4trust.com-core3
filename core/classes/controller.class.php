<?php

/**
 * Controller class
 *
 * @package Tefo core3
 * @version 2014.04.22
 * @author Tefo <tefo05@gmail.com>
 */

class controller {

	/**
	 * Klases/kontrolerio pavadinimas
	 *
	 * @var string
	 */
	var $name;

	/**
	 * Lenteles pavadinimas
	 *
	 * @var string
	 */
	var $table;

	/**
	 * Lenteles laukai
	 *
	 * @var array
	 */
	var $fields;

	/**
	 * Viesi veiksmai/funkcijos
	 *
	 * @var array
	 */
	var $public_acts;

	/**
	 * Prisijungusio vartotojo veiksmai/funkcijos
	 *
	 * @var array
	 */
	var $logged_acts;

	/**
	 * Konstruktorius
	 *
	 * @param string $table
	 */
	function controller($table) {
		$this->name = get_class($this);
		$this->table = $table;
		$this->fields = array();
		$this->public_acts = array();
		$this->logged_acts = array();
	}

	/**
	 * Patikrina ar yra toks laukas
	 *
	 * @param string $field
	 */
	function isField($field) {
		return (in_array($field, array_keys($this->fields)));
	}

	/**
	 * Prideda stulpeli i DB lentele
	 *
	 * @global coreSQL $coreSQL
	 * @param string $field
	 * @param string $type
	 */
	function addFieldInTable($field, $type) {
		global $coreSQL;

		if ($type == "string" || $type == "string_array" || $type == "password") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` VARCHAR( 255 ) NOT NULL;");
		}
		elseif ($type == "text" || $type == "array") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` TEXT NOT NULL;");
		}
		elseif ($type == "int") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` INT( 11 ) NOT NULL;");
		}
		elseif ($type == "float") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` FLOAT( 7, 4 ) NOT NULL;");
		}
		elseif ($type == "double") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` DOUBLE NOT NULL;");
		}
		elseif ($type == "price") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` DECIMAL( 10, 2 ) NOT NULL;");
		}
		elseif ($type == "bool") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` BOOL NOT NULL;");
		}
		elseif ($type == "created" || $type == "now" || $type == "updated" || $type == "datetime") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` DATETIME NOT NULL;");
		}
		elseif ($type == "date") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` DATE NOT NULL;");
		}
		elseif ($type == "time") {
			$coreSQL->query("ALTER TABLE `".$this->table."` ADD `".$field."` TIME NOT NULL;");
		}
		else {
			// nothing
		}
	}

	/**
	 * Sukuria lentele su laukais
	 */
	function createTableStructure() {
		global $coreSQL;

		$coreSQL->query("CREATE TABLE IF NOT EXISTS `".$this->table."` (`id` int(11) NOT NULL auto_increment, PRIMARY KEY  (`id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");

		foreach ($this->fields as $field => $type) {
			$this->addFieldInTable($field, $type);
		}
	}

	/**
	 * Jei lenteles nera automatiskai ja sukuria
	 */
	function autoCreateOnChecker() {
		global $coreSQL, $coreSession;

		if (isset($coreSession->session['checker'])) {

			$table_exists = (bool)$coreSQL->query("SHOW TABLES LIKE '".$this->table."'");
			if (!$table_exists) {
				$this->createTableStructure();
				$coreSession->session['checker_logs'][] = "Created table structure for `".$this->table."`";
			}
		}
	}

	/**
	 * Patikrina ar lauko tipas atitinka SQL DB tipa
	 *
	 * @param string $type
	 * @param string $sql_type
	 */
	function checkFieldType($type, $sql_type) {

		$sql_type = strtoupper($sql_type);

		if ($type == "string" || $type == "string_array" || $type == "password") {
			return substr($sql_type, 0, 7) == "VARCHAR";
		}
		elseif ($type == "text" || $type == "array") {
			return $sql_type == "TEXT";
		}
		elseif ($type == "int") {
			return $sql_type == "INT(11)";
		}
		elseif ($type == "float") {
			return substr($sql_type, 0, 5) == "FLOAT";
		}
		elseif ($type == "double") {
			return $sql_type == "DOUBLE";
		}
		elseif ($type == "price") {
			return $sql_type == "DECIMAL(10,2)";
		}
		elseif ($type == "bool") {
			return $sql_type == "TINYINT(1)";
		}
		elseif ($type == "created" || $type == "now" || $type == "updated" || $type == "datetime") {
			return $sql_type == "DATETIME";
		}
		elseif ($type == "date") {
			return $sql_type == "DATE";
		}
		elseif ($type == "time") {
			return $sql_type == "TIME";
		}
		else {
			return false;
		}
	}

	/**
	 * Tikrina lenteles strukturos modifikacijas
	 */
	function checkTableStructure() {
		global $config, $coreSQL, $coreSession;

		$columns = $coreSQL->queryData("SHOW COLUMNS FROM `".$this->table."`", "Field");

		if ($this->fields) {
			foreach ($this->fields as $field => $type) {
				if (isset($columns[$field])) {
					if (!$this->checkFieldType($type, $columns[$field]['Type'])) {
						$coreSession->session['checker_logs'][] = "Field `".$field."` doesn't match ( ".$type." as ".$columns[$field]['Type']." ) in table `".$this->table."`";
					}
				}
				else {
					$coreSession->session['checker_logs'][] = "Missing column `".$field."` in table `".$this->table."`";
					if (isset($config['checker_sync_tree'])) {
						$this->addFieldInTable($field, $type);
						$coreSession->session['checker_logs'][] = "Added!";
					}
				}
			}

			foreach ($columns as $column) {
				if ($column['Field'] != "id") {
					if (!isset($this->fields[$column['Field']])) {
						$coreSession->session['checker_logs'][] = "Overhead column `".$column['Field']."` in table `".$this->table."`";
					}
				}
			}
		}

	}

	/**
	 * Prideti elementa
	 *
	 * @global coreSQL $coreSQL
	 * @param array $form_data
	 * @param bool $clean
	 * @return int - prideto elemento id
	 */
	function add($form_data, $clean = true) {
		global $coreSQL;

		if ($this->fields) {
			$data = array();

			foreach ($this->fields as $field => $type) {
				if (isset($form_data[$field]) || $type == "created" || $type == "updated" || $type == "now" || $type == "date" || $type == "time") {
					if ($type == "string") {
						if ($clean) { $data[$field] = clean($form_data[$field]); }
						else { $data[$field] = addslashes($form_data[$field]); }
					}
					elseif ($type == "string_array") {
						$data[$field] = implode(",", $form_data[$field]);
					}
					elseif ($type == "password") {
						$data[$field] = md5($form_data[$field]);
					}
					elseif ($type == "text") {
						if ($clean) { $data[$field] = clean($form_data[$field]); }
						else { $data[$field] = addslashes($form_data[$field]); }
					}
					elseif ($type == "array") {
						if ($clean) { $data[$field] = serialize(cleanArray($form_data[$field])); }
						else { $data[$field] = serialize($form_data[$field]); }
					}
					elseif ($type == "int") {
						$data[$field] = (int)$form_data[$field];
					}
					elseif ($type == "float") {
						$data[$field] = (float)$form_data[$field];
					}
					elseif ($type == "double") {
						$data[$field] = (double)$form_data[$field];
					}
					elseif ($type == "price") {
						$data[$field] = (float)$form_data[$field];
					}
					elseif ($type == "bool") {
						$data[$field] = (bool)$form_data[$field];
					}
					elseif ($type == "created" || $type == "now") {
						$data[$field] = date('Y-m-d H:i:s');
					}
					elseif ($type == "updated") {
						$data[$field] = date('Y-m-d H:i:s');
					}
					elseif ($type == "date") {
						if (isset($form_data[$field."_day"]) && isset($form_data[$field."_month"]) && isset($form_data[$field."_year"])) {
							if ((int)$form_data[$field."_month"] == 0) { $form_data[$field."_month"] = 1; }
							if ((int)$form_data[$field."_day"] == 0) { $form_data[$field."_day"] = 1; }
							$date = mktime(0, 0, 0, (int)$form_data[$field."_month"], (int)$form_data[$field."_day"], (int)$form_data[$field."_year"]);
							$data[$field] = date('Y-m-d', $date);
						}
						elseif (isset($form_data[$field])) {
							if ($clean) { $data[$field] = clean($form_data[$field]); }
							else { $data[$field] = addslashes($form_data[$field]); }
						}
					}
					elseif ($type == "time") {
						if (isset($form_data[$field."_hour"]) && isset($form_data[$field."_minute"]) && isset($form_data[$field."_second"])) {
							$time = mktime((int)$form_data[$field."_hour"], (int)$form_data[$field."_minute"], (int)$form_data[$field."_second"], 0, 0, 0);
							$data[$field] = date('H:i:s', $time);
						}
						elseif (isset($form_data[$field])) {
							if ($clean) { $data[$field] = clean($form_data[$field]); }
							else { $data[$field] = addslashes($form_data[$field]); }
						}
					}
					elseif ($type == "datetime") {
						$data[$field] = date('Y-m-d H:i:s', $form_data[$field]);
					}
					else {
						// nothing
					}
				}
			}

			$result = $coreSQL->insertArray($data, $this->table);
			if ((int)$result == 1) {
				return $coreSQL->insertId();
			}
			else {
				return 0;
			}
		}
	}

	/**
	 * Redaguoti elementa
	 *
	 * @global coreSQL $coreSQL
	 * @param array $form_data
	 * @param int $form_data['id'] - privaloma nurodyti id elemento kuri redaguojam
	 * @param bool $clean
	 * @param bool $check_user
	 */
	function edit($form_data, $clean = true, $check_user = false) {
		global $coreSQL;

		if ($check_user) {
			$user_id = (int)$form_data['user_id'];
			unset($form_data['user_id']);
		}

		if ($this->fields) {
			$data = array();

			foreach ($this->fields as $field => $type) {
				if (isset($form_data[$field]) || $type == "created" || $type == "updated" || $type == "now" || $type == "date" || $type == "time") {
					if ($type == "string") {
						if ($clean) { $data[$field] = clean($form_data[$field]); }
						else { $data[$field] = addslashes($form_data[$field]); }
					}
					elseif ($type == "string_array") {
						$data[$field] = implode(",", $form_data[$field]);
					}
					elseif ($type == "password") {
						$data[$field] = md5($form_data[$field]);
					}
					elseif ($type == "text") {
						if ($clean) { $data[$field] = clean($form_data[$field]); }
						else { $data[$field] = addslashes($form_data[$field]); }
					}
					elseif ($type == "array") {
						if ($clean) { $data[$field] = serialize(cleanArray($form_data[$field])); }
						else { $data[$field] = serialize($form_data[$field]); }
					}
					elseif ($type == "int") {
						$data[$field] = (int)$form_data[$field];
					}
					elseif ($type == "float") {
						$data[$field] = (float)$form_data[$field];
					}
					elseif ($type == "double") {
						$data[$field] = (double)$form_data[$field];
					}
					elseif ($type == "price") {
						$data[$field] = (float)$form_data[$field];
					}
					elseif ($type == "bool") {
						$data[$field] = (bool)$form_data[$field];
					}
					elseif ($type == "created" || $type == "now") {
						//$data[$field] = date('Y-m-d H:i:s');
					}
					elseif ($type == "updated") {
						$data[$field] = date('Y-m-d H:i:s');
					}
					elseif ($type == "date") {
						if (isset($form_data[$field."_day"]) && isset($form_data[$field."_month"]) && isset($form_data[$field."_year"])) {
							if ((int)$form_data[$field."_month"] == 0) { $form_data[$field."_month"] = 1; }
							if ((int)$form_data[$field."_day"] == 0) { $form_data[$field."_day"] = 1; }
							$date = mktime(0, 0, 0, (int)$form_data[$field."_month"], (int)$form_data[$field."_day"], (int)$form_data[$field."_year"]);
							$data[$field] = date('Y-m-d', $date);
						}
						elseif (isset($form_data[$field])) {
							if ($clean) { $data[$field] = clean($form_data[$field]); }
							else { $data[$field] = addslashes($form_data[$field]); }
						}
					}
					elseif ($type == "time") {
						if (isset($form_data[$field."_hour"]) && isset($form_data[$field."_minute"]) && isset($form_data[$field."_second"])) {
							$time = mktime((int)$form_data[$field."_hour"], (int)$form_data[$field."_minute"], (int)$form_data[$field."_second"], 0, 0, 0);
							$data[$field] = date('H:i:s', $time);
						}
						elseif (isset($form_data[$field])) {
							if ($clean) { $data[$field] = clean($form_data[$field]); }
							else { $data[$field] = addslashes($form_data[$field]); }
						}
					}
					elseif ($type == "datetime") {
						$data[$field] = date('Y-m-d H:i:s', $form_data[$field]);
					}
					else {
						// nothing
					}
				}
			}

			if ($check_user) {
				$coreSQL->updateArray($data, $this->table, "`id`=".(int)$form_data['id']." AND `user_id`=".$user_id);
			}
			else {
				$coreSQL->updateArray($data, $this->table, "`id`=".(int)$form_data['id']);
			}
		}
	}

	/**
	 * Istrinti elementa
	 *
	 * @global coreSQL $coreSQL
	 * @param array $form_data
	 * @param int $form_data['id'] - privaloma nurodyti id elemento kuri istrinam
	 * @return int
	 */
	function delete($form_data) {
		global $coreSQL;
		$id = (int)$form_data['id'];
		return $coreSQL->query("DELETE FROM `".$this->table."` WHERE `id`=$id");
	}

	/**
	 * Istrinti elementa pagal id
	 *
	 * @global coreSQL $coreSQL
	 * @param int $id
	 * @return int
	 */
	function deleteById($id) {
		global $coreSQL;
		return $coreSQL->query("DELETE FROM `".$this->table."` WHERE `id`=".(int)$id);
	}

	/**
	 * istrinti elementus pagal lauka
	 *
	 * @global coreSQL $coreSQL
	 * @param string $field
	 * @param string $value
	 */
	function deleteByField($field, $value) {
		global $coreSQL;

		if ($this->isField($field)) {
			$value = clean($value);
			$coreSQL->query("DELETE FROM `".$this->table."` WHERE `$field`='$value'");
		}
	}

	/**
	 * Istrinti elementu sarasa
	 *
	 * @global coreSQL $coreSQL
	 * @param array $list - istrinamu elementu ids
	 */
	function deleteList($list) {
		global $coreSQL;

		$ids = array();

		if ($list) {
			foreach ($list as $id => $value) {
				array_push($ids, $id);
			}
		}

		if ($ids) {
			$delete_sql = $coreSQL->sqlValues("id", $ids, "OR");
			$coreSQL->query("DELETE FROM `".$this->table."` WHERE ( $delete_sql ) ");
		}

	}

	/**
	 * Istrinti pasirinktu elementu sarasa
	 *
	 * @global coreSQL $coreSQL
	 * @param array $form_data['list'] - istrinamu elementu ids
	 */
	function deleteSelected($form_data) {
		$list = $form_data['list'];
		$this->deleteList($list);
	}

	/**
	 * Gauti elementa pagal id
	 *
	 * @global coreSQL $coreSQL
	 * @param int $id
	 * @return array
	 */
	function getById($id) {
		global $coreSQL;
		$id = (int)$id;
		$result = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `id`=$id");

		if ($result) {
			foreach ($this->fields as $field => $type) {
				if ($type == "string_array") {
					$result[$field] = explode(",", $result[$field]);
				}
				elseif ($type == "array") {
					$result[$field] = unserialize($result[$field]);
				}
			}
		}
		
		return $result;
	}

	/**
	 * Gauti elementa pagal lauka
	 *
	 * @param string $field
	 * @param string $value
	 * @return array
	 */
	function getByField($field, $value, $random = false) {
		global $coreSQL;

		if ($this->isField($field)) {
			$value = clean($value);
			$result = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `$field`='$value'".($random?" ORDER BY RAND()":""));
		}
		else { $result = false; }

		if ($result) {
			foreach ($this->fields as $field => $type) {
				if ($type == "string_array") {
					$result[$field] = explode(",", $result[$field]);
				}
				elseif ($type == "array") {
					$result[$field] = unserialize($result[$field]);
				}
			}
		}

		return $result;
	}

	/**
	 * Gauti visus elementus
	 *
	 * @global coreSQL $coreSQL
	 * @return array
	 */
	function getAll() {
		global $coreSQL;
		return $coreSQL->queryData("SELECT * FROM `".$this->table."`");
	}

	/**
	 * Gauti visus elementu sudetus pagal id
	 *
	 * @global coreSQL $coreSQL
	 * @return array
	 */
	function getAllById() {
		global $coreSQL;
		return $coreSQL->queryData("SELECT * FROM `".$this->table."`", "id");
	}

	/**
	 * Gauti elementus pagal lauka
	 *
	 * @global coreSQL $coreSQL
	 * @param string $field
	 * @param string $value
	 * @return array
	 */
	function getAllByField($field, $value) {
		global $coreSQL;

		if ($this->isField($field)) {
			$value = clean($value);
			$result = $coreSQL->queryData("SELECT * FROM `".$this->table."` WHERE `$field`='$value'");
		}
		else { $result = false; }

		return $result;
	}

	/**
	 * Gauti visa kolona
	 *
	 * @global coreSQL $coreSQL
	 * @param string $column
	 * @return array
	 */
	function getColumn($column) {
		global $coreSQL;
		return $coreSQL->queryColumn("SELECT `$column` FROM `".$this->table."`", $column);
	}

	/**
	 * Gauti visa kolona sudeta pagal id
	 *
	 * @global coreSQL $coreSQL
	 * @param string $column
	 * @return array
	 */
	function getColumnById($column) {
		global $coreSQL;
		return $coreSQL->queryColumn("SELECT `id`, `$column` FROM `".$this->table."`", $column, "id");
	}

	/**
	 * Elementu kiekis
	 *
	 * @global coreSQL $coreSQL
	 * @return int
	 */
	function count() {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `".$this->table."`");
	}

	/**
	 * Elementu kiekis pagal lauka
	 *
	 * @global coreSQL $coreSQL
	 * @param string $field
	 * @param string $value
	 * @return array
	 */
	function countByField($field, $value) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE `$field`='$value'");
	}

	/**
	 * Didesniu/naujesniu elementu uz nurodyta value kiekis
	 *
	 * @global coreSQL $coreSQL
	 * @param string $field
	 * @param string $value
	 * return int
	 */
	function countGreater($field, $value) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE `$field`>'$value'");
	}

	/**
	 * Mazesniu/senesniu elementu uz nurodyta value kiekis
	 *
	 * @global coreSQL $coreSQL
	 * @param string $field
	 * @param string $value
	 * return int
	 */
	function countLess($field, $value) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE `$field`<'$value'");
	}

	/**
	 * Grazina siandien pridetus elementus
	 *
	 * @global coreSQL $coreSQL
	 * @return array
	 */
	function addedToday() {
		global $coreSQL;
		return (int)$coreSQL->queryValue("
			SELECT COUNT(*) FROM `".$this->table."`
			WHERE FLOOR((UNIX_TIMESTAMP(CURDATE()) - UNIX_TIMESTAMP(`created`)) / 86400) < 0");
	}

	/**
	 * Papildo search_sql pagal filtra
	 *
	 * @global coreSQL $coreSQL
	 * @param string $search_sql
	 * @param array $filter
	 * @param bool $string_strict
	 */
	function appendSearchSQLByFilter(&$search_sql, $filter, $string_strict) {
		global $coreSQL;

		if ($this->fields) {

			$id_field = array("id" => "int");
			$fields = array_merge($id_field, $this->fields);

			foreach ($fields as $field => $type) {
				if (isset($filter[$field])) {
					if ($type == "string" || $type == "text") {
						if ($string_strict) {
							if (!empty($filter[$field])) { $search_sql .= " AND `".$this->table."`.`$field`='".clean($filter[$field])."'"; }
						}
						else {
							if (!empty($filter[$field])) { $search_sql .= " AND `".$this->table."`.`$field` LIKE '%".clean($filter[$field])."%'"; }
						}
					}
					elseif ($type == "string_array") {
						if (is_array($filter[$field])) {
							$values = array();
							foreach ($filter[$field] as $value) {
								if (!empty($value)) { $values[] = clean($value); }
							}
							if ($values) { $search_sql .= " AND (".$coreSQL->sqlSearchArrayValues("`".$this->table."`.`$field`", $values, "OR").")"; }
						}
						elseif (!empty($filter[$field])) {
							$values = array();
							$values[] = clean($filter[$field]);
							if ($values) { $search_sql .= " AND (".$coreSQL->sqlSearchArrayValues("`".$this->table."`.`$field`", $values, "OR").")"; }
						}
					}
					elseif ($type == "int") {
						if (is_array($filter[$field])) {
							$values = array();
							foreach ($filter[$field] as $value) {
								if ($value != "") { $values[] = (int)$value; }
							}
							if ($values) { $search_sql .= " AND (".$coreSQL->sqlValues("`".$this->table."`.`$field`", $values, "OR").")"; }
						}
						elseif ($filter[$field] != "") { $search_sql .= " AND `".$this->table."`.`$field`=".(int)$filter[$field]; }
					}
					elseif ($type == "float") {
						if (is_array($filter[$field])) {
							$values = array();
							foreach ($filter[$field] as $value) {
								if ($value != "") { $values[] = (float)$value; }
							}
							if ($values) { $search_sql .= " AND (".$coreSQL->sqlValues("`".$this->table."`.`$field`", $values, "OR").")"; }
						}
						elseif ($filter[$field] != "") { $search_sql .= " AND `".$this->table."`.`$field`=".(float)$filter[$field]; }
					}
					elseif ($type == "double" || $type == "price") {
						if (is_array($filter[$field])) {
							$values = array();
							foreach ($filter[$field] as $value) {
								if ($value != "") { $values[] = (double)$value; }
							}
							if ($values) { $search_sql .= " AND (".$coreSQL->sqlValues("`".$this->table."`.`$field`", $values, "OR").")"; }
						}
						elseif ($filter[$field] != "") { $search_sql .= " AND `".$this->table."`.`$field`=".(double)$filter[$field]; }
					}
					elseif ($type == "bool") {
						if ((bool)$filter[$field] == true) {
							$search_sql .= " AND `".$this->table."`.`$field`='1'";
						}
						elseif ($filter[$field] == "0") {
							$search_sql .= " AND `".$this->table."`.`$field`='0'";
						}
					}
					elseif ($type == "date" || $type == "datetime") {
						if (!empty($filter[$field])) { $search_sql .= " AND `".$this->table."`.`$field`='".clean($filter[$field])."'"; }
					}
					else {
						// todo other types
					}
				}
				if (isset($filter[$field."_from"])) {
					if ($type == "int") {
						if (!empty($filter[$field."_from"])) { $search_sql .= " AND `".$this->table."`.`$field`>=".(int)$filter[$field."_from"]; }
					}
					elseif ($type == "float") {
						if (!empty($filter[$field."_from"])) { $search_sql .= " AND `".$this->table."`.`$field`>=".(float)$filter[$field."_from"]; }
					}
					elseif ($type == "double" || $type == "price") {
						if (!empty($filter[$field."_from"])) { $search_sql .= " AND `".$this->table."`.`$field`>=".(double)$filter[$field."_from"]; }
					}
					elseif ($type == "date" || $type == "datetime") {
						if (!empty($filter[$field."_from"])) { $search_sql .= " AND `".$this->table."`.`$field`>='".clean($filter[$field."_from"])."'"; }
					}
					else {
						// todo other types
					}
				}
				if (isset($filter[$field."_to"])) {
					if ($type == "int") {
						if (!empty($filter[$field."_to"])) { $search_sql .= " AND `".$this->table."`.`$field`<=".(int)$filter[$field."_to"]; }
					}
					elseif ($type == "float") {
						if (!empty($filter[$field."_to"])) { $search_sql .= " AND `".$this->table."`.`$field`<=".(float)$filter[$field."_to"]; }
					}
					elseif ($type == "double" || $type == "price") {
						if (!empty($filter[$field."_to"])) { $search_sql .= " AND `".$this->table."`.`$field`<=".(double)$filter[$field."_to"]; }
					}
					elseif ($type == "date" || $type == "datetime") {
						if (!empty($filter[$field."_to"])) { $search_sql .= " AND `".$this->table."`.`$field`<='".clean($filter[$field."_to"])."'"; }
					}
					else {
						// todo other types
					}
				}
				if (isset($filter["!".$field])) {
					if ($type == "int") {
						$search_sql .= " AND `".$this->table."`.`$field`!=".(int)$filter["!".$field];
					}
					else {
						// todo other types
					}
				}
			}
		}

	}

	/**
	 * Paieshka pagal uzklausa
	 *
	 * @param string $query
	 */
	function searchQuery($filter, $order = array("id" => "ASC"), $per_page = 50, $search_sql = "", $string_strict = false, 
						$result_fields = "*", $join_tables = null, $on = null, $group_by = "", $key_field = "id") {

		global $coreSQL, $coreSession, $pages;

		unset($filter['controller']);
		unset($filter['act']);
		unset($filter['act_redirect']);

		$page_info = $coreSession->session['page_info'][$pages->url];

		// set items per page
		if (isset($_GET['per_page'])) {
			$per_page = (int)$_GET['per_page'];
		}
		elseif (isset($_GET['show_all']) && isset($page_info['result_count'])) {
			$per_page = $page_info['result_count'];
		}
		elseif (isset($page_info['per_page'])) {
			$per_page = $page_info['per_page'];
		}

		// set items ordering
		if (isset($_GET['order_by'])) {
			if (is_array($_GET['order_by'])) {
				$order = $_GET['order_by'];
			}
		}
		elseif (isset($page_info['order'])) {
			$order = $page_info['order'];
		}

		// set items filtering
		if ((isset($_GET['page']) || isset($_GET['per_page']) || isset($_GET['show_all']) || isset($_GET['order_by']))
			&& isset($page_info['filter'])) {
			$filter = $page_info['filter'];
			$search_sql = $page_info['search_sql'];
		}

		$coreSession->setFilterInfo($filter, $order, $per_page, $search_sql);

		if ($this->fields) {

			$this->appendSearchSQLByFilter($search_sql, $filter, $string_strict);

			$search_sql = substr($search_sql, 5);
			if ($search_sql == "") { $search_sql = "1"; }

			if (!isset($_GET['page'])) { $_GET['page'] = 1; }

			if ($per_page > 0) {

				$page = (int)$_GET['page'];
				if ($page < 1) { return array(); }

				/*
				$result_count = (int)$coreSQL->queryValue("
					SELECT COUNT(*)
					FROM `".$this->table."`".(isset($join_tables)?", `".implode("`, `", $join_tables)."`":"")."
					WHERE ".(isset($on)?" $on AND ":"")." $search_sql $group_by 
				");
				*/

				$limit_sql = $coreSQL->sqlLimit($page-1, $per_page);
			}
			else {
				$limit_sql = "";
			}

			if ($order) {
				$id_field = array("id" => "int");
				$fields = array_merge($id_field, $this->fields);

				$order_sql = "ORDER BY";
				$order_count = 0;
				foreach ($order as $orderby => $ordertype) {
					if (in_array($orderby, array_keys($fields)) &&
						(strtolower($ordertype) == "desc" || strtolower($ordertype) == "asc")
						) {
						$order_count++;
						$order_sql .= ($order_count > 1?",":"")." `".$this->table."`.`$orderby` $ordertype";
					}
				}
				if ($order_count == 0) { $order_sql = ""; }
			}
			else {
				$order_sql = "";
			}

			$result = $coreSQL->queryData("
					SELECT SQL_CALC_FOUND_ROWS $result_fields
					FROM `".$this->table."`".(isset($join_tables)?", `".implode("`, `", $join_tables)."`":"")."
					WHERE ".(isset($on)?" $on AND ":"")." $search_sql $group_by $order_sql $limit_sql", $key_field);

			//$result_count = $coreSQL->queryValue("SELECT FOUND_ROWS()");
			$result_count = (int)$coreSQL->queryValue("
					SELECT COUNT(*)
					FROM `".$this->table."`".(isset($join_tables)?", `".implode("`, `", $join_tables)."`":"")."
					WHERE ".(isset($on)?" $on AND ":"")." $search_sql $group_by");

			$pages_info = paging($result_count, $per_page);
			$coreSession->setPagingInfo($pages_info, $result_count);

			return $result;
		}

	}

	/**
	 * Paieshka pagal uzklausa 2 versija
	 *
	 * @param string $query
	 */
	function searchQuery2($filter, $order = array("id" => "ASC"), $per_page = 50, $search_sql = "", $string_strict = false,
						$result_fields = "*", $from_sql = "", $group_by = "", $key_field = "id") {

		global $coreSQL, $coreSession, $pages;

		unset($filter['controller']);
		unset($filter['act']);
		unset($filter['act_redirect']);

		$page_info = $coreSession->session['page_info'][$pages->url];

		// set items per page
		if (isset($_GET['per_page'])) {
			$per_page = (int)$_GET['per_page'];
		}
		elseif (isset($_GET['show_all']) && isset($page_info['result_count'])) {
			$per_page = $page_info['result_count'];
		}
		elseif (isset($page_info['per_page'])) {
			$per_page = $page_info['per_page'];
		}

		// set items ordering
		if (isset($_GET['order_by'])) {
			if (is_array($_GET['order_by'])) {
				$order = $_GET['order_by'];
			}
		}
		elseif (isset($page_info['order'])) {
			$order = $page_info['order'];
		}

		// set items filtering
		if ((isset($_GET['page']) || isset($_GET['per_page']) || isset($_GET['show_all']) || isset($_GET['order_by']))
			&& isset($page_info['filter'])) {
			$filter = $page_info['filter'];
			$search_sql = $page_info['search_sql'];
		}

		$coreSession->setFilterInfo($filter, $order, $per_page, $search_sql);

		if ($this->fields) {

			$this->appendSearchSQLByFilter($search_sql, $filter, $string_strict);

			$search_sql = substr($search_sql, 5);
			if ($search_sql == "") { $search_sql = "1"; }

			if (!isset($_GET['page'])) { $_GET['page'] = 1; }

			if ($per_page > 0) {

				$page = (int)$_GET['page'];
				if ($page < 1) { return array(); }

				$limit_sql = $coreSQL->sqlLimit($page-1, $per_page);
			}
			else {
				$limit_sql = "";
			}

			if ($order) {
				$id_field = array("id" => "int");
				$fields = array_merge($id_field, $this->fields);

				$order_sql = "ORDER BY";
				$order_count = 0;
				foreach ($order as $orderby => $ordertype) {
					if (in_array($orderby, array_keys($fields)) &&
						(strtolower($ordertype) == "desc" || strtolower($ordertype) == "asc")
						) {
						$order_count++;
						$order_sql .= ($order_count > 1?",":"")." `".$this->table."`.`$orderby` $ordertype";
					}
				}
				if ($order_count == 0) { $order_sql = ""; }
			}
			else {
				$order_sql = "";
			}

			$result = $coreSQL->queryData("
					SELECT SQL_CALC_FOUND_ROWS $result_fields FROM $from_sql
					WHERE $search_sql $group_by $order_sql $limit_sql", $key_field);

			$result_count = $coreSQL->queryValue("SELECT FOUND_ROWS()");
			$pages_info = paging($result_count, $per_page);
			$coreSession->setPagingInfo($pages_info, $result_count);
			
			return $result;
		}

	}

}

?>