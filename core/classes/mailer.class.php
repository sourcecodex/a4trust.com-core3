<?php

require_once $config['core_dir']."libs/phpmailer/class.phpmailer.php";

class coreMailer {

	var $_phpMailer = null;

	function coreMailer($mailer_user) {
		global $config;

		$this->_phpMailer = new PHPMailer;

		// mailer configuration
		$this->_phpMailer->Mailer = 'smtp';

		//if SMTP debug
		if ($config['mailer_smtp_debug']) {
			$this->_phpMailer->SMTPDebug = true;
		}

		// host and port
		$this->_phpMailer->Host = $config['mailer_smtp_host'];
		$this->_phpMailer->Port = $config['mailer_smtp_port'];

		// SMTP AUTH
		$this->_phpMailer->SMTPAuth  = true;
		//$this->_phpMailer->SMTPKeepAlive = true;
		$this->_phpMailer->Username  = $mailer_user['user'];
		$this->_phpMailer->Password  = $mailer_user['password'];

		// default settings
		$this->_phpMailer->From = $mailer_user['from_email'];
		$this->_phpMailer->FromName = $mailer_user['from_name'];

		// character encoding
		$this->_phpMailer->CharSet = 'UTF-8';

		return true;
	}

	function &getPhpMailer() {
		return $this->_phpMailer;
	}

	function sendEmail($to, $subject, $body, $attachments = null, $reply_to = null, $reply_to_name = null, $htmlBody = null, $embeddedImages = null) {

		$this->resetMailer();

		if (is_array($to)) {
			foreach($to as $recipient) {
				$this->addRecipients($recipient);
			}
		} else {
			$this->addRecipients($to);
		}

		if ($reply_to) {
			$this->_phpMailer->AddReplyTo($reply_to, $reply_to_name);
		}
		
		$this->_phpMailer->Subject = $subject;

		if (!is_null($htmlBody)) {
			$this->_phpMailer->IsHTML(true);
			$this->_phpMailer->Body = $htmlBody;
		} else {
			$this->_phpMailer->Body .= $body;
		}

		$this->_phpMailer->AltBody = strip_tags($body);

		if (is_array($embeddedImages)) {
			foreach ($embeddedImages as $key => $embeddedImage) {
				$this->_phpMailer->AddEmbeddedImage($embeddedImage['filename'], $embeddedImage['cid'], $embeddedImage['name'], $embeddedImage['encoding'], $embeddedImage['type']);
			}
		}

		if (!is_null($attachments)) {
			if (is_array($attachments)) {
				foreach($attachments as $attachment) {
					$this->_phpMailer->AddAttachment($attachment);
				}
			}
		}

		$send = $this->_phpMailer->send();

		return $send;
	 }

	 function addRecipients($to) {
		global $config;

		if (!is_array($to)) {
			$to = (array) $to;
		}

		// recipients check
		if (count($to) < 1) {
			return false;
		}

		if ($config['mailer_mail_flow_enabled']) {
			foreach ($to as $key => $value) {
				if (is_string($key)) {
					$this->_phpMailer->AddAddress($value, $key);
				} else if (is_numeric($key)) {
					$this->_phpMailer->AddAddress($value);
				}
			}
		} else {
			$debugTo = $config['mailer_redirect'];

			// recipients list
			$originalRecipients = 'Original recipients:'."\n";
			$originalRecipients .= '--------------------------------'."\n";
			foreach ($to as $key => $value) {
				if (is_string($key)) {
					$originalRecipients .= $key.', '.$value."\n";
				} else if (is_numeric($key)) {
					$originalRecipients .= $value."\n";
				}
			}
			$originalRecipients .= '--------------------------------'."\n";

			// developers or testers
			if (!is_array($debugTo)) {
				$debugTo = (array) $debugTo;
			}

			if (count($debugTo) < 1) {
				return false;
			}

			foreach ($debugTo as $key => $value) {
				if (is_string($key)) {
					$this->_phpMailer->AddAddress($value, $key);
				} else if (is_numeric($key)) {
					$this->_phpMailer->AddAddress($value);
				}
			}

			$this->_phpMailer->Body .= $originalRecipients;
		}
	 }

	 function resetMailer() {
		$this->_phpMailer->Body = '';
		$this->_phpMailer->ClearAllRecipients();
		$this->_phpMailer->ClearAttachments();
		$this->_phpMailer->ClearReplyTos();
	 }

	 function changeFromName($name) {
		 $this->_phpMailer->FromName = $name;
	 }

 }

?>