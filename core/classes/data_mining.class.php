<?php

/**
 * Data mining class
 *
 * @package Tefo core3
 * @version 2015.03.28
 * @author Tefo <tefo05@gmail.com>
 */

require_once $config['core_dir']."core/classes/html_dom.class.php";

class dataMining {

	/**
	 * Html dom object
	 *
	 * @var data array
	 */
	var $html;

	/**
	 * Laikas per kuri parsiuncia html duomenis
	 *
	 * @var int
	 */
	var $get_time;

	/**
	 * Turinio HTML kodo dydis
	 *
	 * @var int
	 */
	var $content_size;

	/**
	 * Isgauti tekstiniai elementai is html
	 *
	 * @var array
	 */
	var $text_elements_array;

	/**
	 * Isgauta lentele (masyvas) is html
	 *
	 * @var array
	 */
	var $table_array;

	/**
	 * Isgauti headeriai is html
	 *
	 * @var array
	 */
	var $headers_array;

	/**
	 * Isgautos vidines nuorodos is html
	 *
	 * @var array
	 */
	var $inner_links_array;

	/**
	 * Isgautos isorines nuorodos is html
	 *
	 * @var array
	 */
	var $outer_links_array;

	/**
	 * Isgauta kontaktine info
	 *
	 * @var array
	 */
	var $contacts_table_array;

	/**
	 * Klaidos isgaunant duomenis
	 *
	 * @var array
	 */
	var $errors;

    function dataMining($url, $context=null, $html=null) {
		$start_time = microtime_float();
		if ($html) {
			$this->html = str_get_html($html);
		}
		else {
			$this->html = file_get_html($url, false, $context);
		}
		$this->content_size = strlen($this->html->outertext);
		$this->get_time = intval((microtime_float()-$start_time)*1000000);
		$this->errors = array();
	}

	function createTextElements() {
		$this->text_elements_array = array();
		$elements = $this->html->find('text');
		foreach ($elements as $element) {

			array_push($this->text_elements_array, preg_replace('/\s+/', ' ', $element->plaintext));

			echo $element->parent->tag.", [".preg_replace('/\s+/', ' ', $element->plaintext)."]<br/>";
		}
	}

	function createTableArray($table_node, $columns, $row_from = 1, $row_to = null) {
		$this->table_array = array();
		/*
		$this->table_array[0] = array();
		foreach ($columns as $column) {
			if (isset($column)) array_push($this->table_array[0], $column);
		}
		*/
		if ($table_node->tag == "table") {
			$row = 0;
			foreach ($table_node->children as $tr_node) {
				if ($tr_node->tag == "tr") {

					$row++;

					if ($row < $row_from) continue;

					if (count($tr_node->children) != count($columns)) {
						array_push($this->errors, "Warning: Columns count [".count($columns)."] doesnt match table's `td` count [".count($tr_node->children)."]");
					}
					
					$this->table_array[$row] = array();
					$col = 0;
					foreach ($tr_node->children as $td_node) {
						if ($td_node->tag == "td") {
							if (isset($columns[$col])) {
								array_push($this->table_array[$row], trim(preg_replace('/\s+/', ' ', $td_node->plaintext)));
							}
							$col++;
						}
						else {
							array_push($this->errors, "Warning: Unexpected tag `".$td_node->tag."` instead of `td`");
						}
					}

				}
				else {
					array_push($this->errors, "Warning: Unexpected tag `".$tr_node->tag."` instead of `tr`");
				}
			}
		}
		else {
			array_push($this->errors, "Error: It's not `table` node given, but `".$table_node->tag."`");
		}
	}

	function findPageHeaders() {
		$this->headers_array = array();

		if ($this->html) {

			$base_tag = $this->html->find('base', 0);
			if (isset($base_tag)) $this->headers_array["page_base"] = trim($base_tag->href);

			$title_tag = $this->html->find('title', 0);
			if (isset($title_tag)) $this->headers_array["page_title"] = trim($title_tag->plaintext);

			$meta = $this->html->find('meta');
			foreach ($meta as $key => $meta_tag) {
				$http_equiv = $meta_tag->getAttribute('http-equiv');
				if (isset($meta_tag->name) && isset($meta_tag->content)) {
					if (strtolower($meta_tag->name) == "description") {
						$this->headers_array["page_description"] = trim($meta_tag->content);
					}
					elseif (strtolower($meta_tag->name) == "keywords" || strtolower($meta_tag->name) == "keyword") {
						$this->headers_array["page_keywords"] = trim($meta_tag->content);
					}
					elseif (strtolower($meta_tag->name) == "creator") {
						$this->headers_array["page_creator"] = trim($meta_tag->content);
					}
					elseif (strtolower($meta_tag->name) == "author") {
						$this->headers_array["page_author"] = trim($meta_tag->content);
					}
					elseif (strtolower($meta_tag->name) == "generator") {
						$this->headers_array["page_generator"] = trim($meta_tag->content);
					}
					elseif (strtolower($meta_tag->name) == "robots" ||
							strtolower($meta_tag->name) == "abstract" ||
							strtolower($meta_tag->name) == "refresh" ||
							strtolower($meta_tag->name) == "classification" ||
							strtolower($meta_tag->name) == "revisit" ||
							strtolower($meta_tag->name) == "googlebot" ||
							strtolower($meta_tag->name) == "revisit-after" ||
							strtolower($meta_tag->name) == "google-site-verification" ||
							strtolower($meta_tag->name) == "y_key" ||
							strtolower($meta_tag->name) == "msvalidate.01") {
						// ignore
					}
					else {
						array_push($this->errors, "Warning: Not recognised meta tag name: `".$meta_tag->name."`");
					}
				}
				elseif (isset($http_equiv) && isset($meta_tag->content)) {
					if (strtolower($http_equiv) == "content-type") {
						$this->headers_array["content_type"] = trim($meta_tag->content);
					}
					elseif (strtolower($http_equiv) == "content-language") {
						$this->headers_array["content_lang"] = trim($meta_tag->content);
					}
					elseif (strtolower($http_equiv) == "refresh") {
						$this->headers_array["meta_refresh"] = trim($meta_tag->content);
					}
					elseif (strtolower($meta_tag->property) == "fb:app_id" ||
							strtolower($http_equiv) == "content-style-type" ||
							strtolower($http_equiv) == "imagetoolbar" ||
							strtolower($http_equiv) == "x-ua-compatible") {
						// ignore
					}
					else {
						array_push($this->errors, "Warning: Not recognised meta tag equiv: `".$http_equiv."`");
					}
				}
				else {
					array_push($this->errors, "Warning: Not recognised meta tag key [".$key."]");
				}
			}
		}
		else {
			array_push($this->errors, "Error: No html content found");
		}
	}

	function findPageLinks($base_url) {
		$this->inner_links_array = array();
		$this->outer_links_array = array();

		if ($this->html) {

			$links = $this->html->find('a');
			foreach ($links as $key => $link_tag) {

				if (strtolower(substr($link_tag->href, 0, 7)) == "http://" ||
					strtolower(substr($link_tag->href, 0, 8)) == "https://") {
					if (strtolower(substr($link_tag->href, 0, strlen($base_url))) == $base_url) {
						$link = array();
						$link['title'] = trim(preg_replace('/\s+/', ' ', $link_tag->plaintext));
						$link['url'] = trim($link_tag->href);
						array_push($this->inner_links_array, $link);
					}
					else {
						$link = array();
						$link['title'] = trim(preg_replace('/\s+/', ' ', $link_tag->plaintext));
						$link['url'] = trim($link_tag->href);
						array_push($this->outer_links_array, $link);
					}
				}
				elseif (strtolower(substr($link_tag->href, 0, 11)) == "javascript:") {
					// javascript code
				}
				elseif (strtolower(substr($link_tag->href, 0, 7)) == "mailto:") {
					// mailto link
				}
				elseif (strtolower(substr($link_tag->href, 0, 6)) == "skype:") {
					// skype link
				}
				else {
					$link = array();
					$link['title'] = trim(preg_replace('/\s+/', ' ', $link_tag->plaintext));
					if (substr($link_tag->href, 0, 1) == "/") {
						$link['url'] = $base_url.trim(substr($link_tag->href, 1));
					}
					else $link['url'] = $base_url.trim($link_tag->href);
					array_push($this->inner_links_array, $link);
				}

			}
		}
	}

	function findContactsPage() {
		$contacts_page = "";
		if ($this->inner_links_array) {
			foreach ($this->inner_links_array as $link) {
				if (trim(mb_strtolower($link['title'], "UTF-8")) == "kontaktai") {
					$contacts_page = trim($link['url']);
					break;
				}
				elseif (strpos($link['url'], "kontaktai") !== false) {
					$contacts_page = trim($link['url']);
				}
			}
		}
		return $contacts_page;
	}

}

?>
