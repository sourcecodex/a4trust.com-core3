<?php

/**
 * Session class
 * 
 * @package Tefo core3
 * @version 2014.04.22
 * @author Tefo <tefo05@gmail.com> 
 */

class coreSession {
	
	var $session_path;
	var $session;
	
	// =================== Session Structure =========================================
	
	/**
	 * [lang] => language
	 * [content_lang] => language
	 * [translations_by_page] => data
	 * 
	 * [coreUser] => user_id
	 * [coreUserData] => data
	 * 
	 * [page_info][$page_url] => Array (
	 *      [pages] => array
	 *		[pages_count] => int
	 *		[result_count] => int
	 *      [filter] => array
	 *      [order] => array
	 *      [per_page] => int
	 * )
	 * 
	 * [cache] => Array (
	 *      [sql] => data
	 *      [list_fields] => data
	 *      ...
	 * )
	 *
	 * [debugger] => int
	 * [debug] => Array (
	 *      [sql_queries] => data
	 * )
	 * 
	 */

	// ============================= Base ============================================
	
	/**
	 * Konstruktorius, kuris pradeda sesija
	 *
	 * @return coreSession
	 */
	function coreSession() {
		$this->open();
	}

	/**
	 * Issaugo sesijoj puslapiavimo informacija (puslapiai, paieshkos rezultatai, url)
	 *
	 * @param array $pages_info
	 * @param string $url
	 * @param int $result_count
	 */
	function setPagingInfo($pages_info, $result_count) {
		global $pages;
		$this->session['page_info'][$pages->url]['pages'] = $pages_info;
		$this->session['page_info'][$pages->url]['pages_count'] = count($pages_info);
		$this->session['page_info'][$pages->url]['result_count'] = $result_count;
	}

	/**
	 * Issaugo sesijoj filtravimo informacija
	 *
	 * @param array $filter
	 */
	function setFilterInfo($filter, $order, $per_page, $search_sql) {
		global $pages;
		$this->session['page_info'][$pages->url]['filter'] = $filter;
		$this->session['page_info'][$pages->url]['order'] = $order;
		$this->session['page_info'][$pages->url]['per_page'] = $per_page;
		$this->session['page_info'][$pages->url]['search_sql'] = $search_sql;
	}
	
	/**
	 * Atidaro sesija
	 *
	 */
	function open() {
		global $config;

		session_start();
		/*
		header('Cache-control: private'); // IE 6 FIX
		header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', false);
		header('Pragma: no-cache');
		*/
		$this->session_path = $config['session_path'];
		$this->session = $_SESSION[$this->session_path];

		if (!$config['session_cache']) {
			unset($this->session['cache']);
		}
	}
	
	/**
	 * Uzdaro sesija
	 *
	 */
	function close() {
		$_SESSION[$this->session_path] = $this->session;
		session_write_close();
	}
	
}

?>