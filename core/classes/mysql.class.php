<?php

/**
 * MySQL class
 * 
 * @package Tefo core2
 * @version 2008.11.26
 * @author Tefo <tefo05@gmail.com> 
 */

class coreSQL {
	
	var $host, $user, $pass, $database;
	var $con, $rez, $data, $nr, $debug, $queries, $query_class;

	// ============================= Base ============================================
	
	/**
	 * Konstruktorius
	 *
	 * @param string $host
	 * @param string $user
	 * @param string $pass
	 * @param string $database
	 * @return coreSQL
	 */
	function coreSQL($host, $user, $pass, $database) {
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->database = $database;
		$this->nr = 0;
		$this->debug = false;
		$this->queries = array();
		$this->query_class = "coreSQL";
		$this->start_time = 0;
	}

	/**
	 * Prijungia prie duomenu bazes
	 *
	 * @return bool
	 */
	function connect() {
		$this->con = mysql_pconnect($this->host, $this->user, $this->pass);
		if ($this->con) {
			mysql_select_db($this->database, $this->con);
			return true;
		}
		return false;
	}
	
	/**
	 * Iraso uzklausa i debuger'i ir paskaiciuoja atlikimo laika
	 *
	 * @param string $sql
	 */
	function debuger($sql) {
		if ($this->debug) {
			array_push($this->queries, array(
				$sql,
				$this->affectedRows(),
				$this->errno().' '.$this->error(),
				intval((microtime_float()-$this->start_time)*1000000),
				$this->query_class
			));
		}
	}
	
	/**
	 * Vykdo SQL uzklausa
	 *
	 * @param string $sql
	 * @return int
	 */
	function query($sql) {
		$this->start_time = microtime_float();
		$r = mysql_query($sql, $this->con);
		$nr = mysql_affected_rows($this->con);
		$this->debuger($sql);
		return $nr;
	}
	
	/**
	 * Vykdo SQL uzklausa, rezultatas - laukas
	 *
	 * @param string $sql
	 * @param string $name
	 * @return mixed
	 */
	function queryValue($sql, $name = null) {
		$this->start_time = microtime_float();
		$this->rez = mysql_query($sql, $this->con);
		
		if ($this->rez === false) {
			$this->debuger($sql);
			return false;
		}
		if (mysql_num_rows($this->rez) == 0) {
			$this->debuger($sql);
			return false;
		}
		
		$this->data = "";

		if ($name == null) {
			list ($this->data) = mysql_fetch_array($this->rez, MYSQL_NUM);
		} else {
			$this->data = mysql_fetch_assoc($this->rez);
			$this->data = $this->data[$name];
		}
		
		$this->debuger($sql);
		return $this->data;
	}

	/**
	 * Vykdo SQL uzklausa, rezultatas - eilute
	 *
	 * @param string $sql
	 * @return array
	 */
	function queryRow($sql) {
		$this->start_time = microtime_float();
		$this->rez = mysql_query($sql, $this->con);
		
		if ($this->rez === false) {
			$this->debuger($sql);
			return array();
		}
		if (mysql_num_rows($this->rez) == 0) {
			$this->debuger($sql);
			return array();
		}

		$this->data = mysql_fetch_assoc($this->rez);
		$this->debuger($sql);
		return $this->data;
	}

	/**
	 * Vykdo SQL uzklausa, rezultatas - stulpelis
	 *
	 * @param string $sql
	 * @param string $column
	 * @param string $id
	 * @return array
	 */
	function queryColumn($sql, $column, $id = null) {
		$this->start_time = microtime_float();
		$this->rez = mysql_query($sql, $this->con);
		
		if ($this->rez === false) {
			$this->debuger($sql);
			return array();
		}
		if (mysql_num_rows($this->rez) == 0) {
			$this->debuger($sql);
			return array();
		}

		$this->data = array();

		if ($id != null) {
			while ($row = mysql_fetch_assoc($this->rez)) {
				$this->data[$row[$id]] = $row[$column];
			}
		} else {
			while ($row = mysql_fetch_assoc($this->rez)) {
				$this->data[] = $row[$column];
			}
		}

		$this->debuger($sql);
		return $this->data;
	}

	/**
	 * Vykdo SQL uzklausa, rezultatas - lentele
	 *
	 * @param string $sql
	 * @param string $id
	 * @return array
	 */
	function queryData($sql, $id = null) {
		$this->start_time = microtime_float();
		$this->rez = mysql_query($sql, $this->con);
		
		if ($this->rez === false) {
			$this->debuger($sql);
			return array();
		}
		if (mysql_num_rows($this->rez) == 0) {
			$this->debuger($sql);
			return array();
		}

		$this->data = array();
		
		if ($id != null) {
			while ($row = mysql_fetch_assoc($this->rez)) {
				$this->data[$row[$id]] = $row;
			}
		} else {
			while ($row = mysql_fetch_assoc($this->rez)) {
				$this->data[] = $row;
			}
		}
		
		$this->debuger($sql);
		return $this->data;
	}

	/**
	 * funkcijos queryData versija su kesavimu
	 *
	 * @param string $sql
	 * @return array
	 */
	function queryDataCached($sql, $id = null) {
		global $coreSession;
		
		if (!empty($coreSession->session['cache']['sql'][$sql])) {
			return $coreSession->session['cache']['sql'][$sql];
		}
		else {
			$result = $this->queryData($sql, $id);
			$coreSession->session['cache']['sql'][$sql] = $result;
			return $result;
		}
	}
	
	/**
	 * Grazina paskutines INSERT uzklausos id
	 *
	 * @return int
	 */
	function insertId() {
		return mysql_insert_id($this->con);
	}

	/**
	 * Grazina paskutines uzhklausos "paliestu" eiluciu skaiciu
	 *
	 * @return unknown
	 */
	function affectedRows() {
		return mysql_affected_rows($this->con);
	}
	
	/**
	 * Grazina uzklausos klaida
	 *
	 * @return string
	 */
	function error() {
		return mysql_error($this->con); 
	}

	/**
	 * Grazina uzklausos klaidos numeri
	 *
	 * @return int
	 */
	function errno() {
		return mysql_errno($this->con); 
	}

	/**
	 * Atjungia is duomenu bazes
	 *
	 */
	function disconect() {
		mysql_close($this->con);
	}

	// ============================= Extension ============================================
	
	// Arrays
	
	/**
	 * Ideda masyva i lentele
	 *
	 * @param array $array
	 * @param string $table
	 */
	function insertArray($array, $table) {
		
		$counter = 0;
		$count = count($array);
		
		$sql1 = "";
		$sql2 = "";
		foreach ($array as $field => $value) {
			$counter++;
			if ($counter == $count) {
				$sql1 .= "`" . $field . "`";
				$sql2 .= "'" . $value . "'";
			}
			else {
				$sql1 .= "`" . $field . "`, ";
				$sql2 .= "'" . $value . "', ";
			}
		}
		
		if ($counter != 0) {
			return $this->query("INSERT INTO `".$table."` (".$sql1.") VALUES (".$sql2.")");
		}
		else { return 0; }
	}

	/**
	 * Ideda masyva i lentele
	 *
	 * @param array $array
	 * @param string $table
	 */
	function replaceArray($array, $table) {

		$counter = 0;
		$count = count($array);

		$sql1 = "";
		$sql2 = "";
		foreach ($array as $field => $value) {
			$counter++;
			if ($counter == $count) {
				$sql1 .= "`" . $field . "`";
				$sql2 .= "'" . $value . "'";
			}
			else {
				$sql1 .= "`" . $field . "`, ";
				$sql2 .= "'" . $value . "', ";
			}
		}

		if ($counter != 0) {
			return $this->query("REPLACE INTO `".$table."` (".$sql1.") VALUES (".$sql2.")");
		}
		else { return 0; }
	}

	/**
	 * Atnaujina masyva
	 *
	 * @param array $array
	 * @param string $table
	 * @param string $where
	 */
	function updateArray($array, $table, $where) {
		
		$counter = 0;
		$count = count($array);
		
		$sql = "";
		foreach ($array as $field => $value) {
			$counter++;
			if ($counter == $count) {
				$sql .= "`" . $field . "`='" . $value . "' ";
			}
			else {
				$sql .= "`" . $field . "`='" . $value . "', ";
			}
		}
		
		if ($counter != 0) {
			return $this->query("UPDATE `".$table."` SET ".$sql." WHERE ".$where);
		}
		else { return 0; }
	}
	
	// SQL generation
	
	/**
	 * Generuoja SQL sakinio WHERE dali formatu: " `field`='value' operation ... " pagal values masyva
	 *
	 * @param string $field
	 * @param array $values
	 * @param string $operation
	 * @return string
	 */
	function sqlValues($field, $values, $operation) {
		$sql = "";
		$count = count($values);
		$i = 0;
		foreach ($values as $value) {
			$i++;
			if ($i != $count) { $sql .= "$field='$value' $operation "; }
			else { $sql .= "$field='$value'"; }
		}
		return $sql;
	}

	/**
	 * Generuoja SQL paieskai atlikti tarp masyvo reiksmiu
	 *
	 * @param string $field
	 * @param array $values
	 * @param string $operation
	 * @return string
	 */
	function sqlSearchArrayValues($field, $values, $operation) {
		$sql = "";
		$count = count($values);
		$i = 0;
		foreach ($values as $value) {
			$i++;
			if ($i != $count) { $sql .= "($field='$value' OR $field LIKE '%,$value' OR $field LIKE '$value,%' OR $field LIKE '%,$value,%') $operation "; }
			else { $sql .= "($field='$value' OR $field LIKE '%,$value' OR $field LIKE '$value,%' OR $field LIKE '%,$value,%')"; }
		}
		return $sql;
	}

	/**
	 * Generuoja SQL sakinio WHERE dali formatu: " `field`='value' operation ... " pagal fields masyva
	 *
	 * @param array $fields
	 * @param string $value
	 * @param string $operation
	 * @return string
	 */
	function sqlFields($fields, $value, $operation) {
		$sql = "";
		$count = count($fields);
		$i = 0;
		foreach ($fields as $field) {
			$i++;
			if ($i != $count) { $sql .= "`$field`='$value' $operation "; }
			else { $sql .= "`$field`='$value'"; }
		}
		return $sql;
	}
	
	/**
	 * Generuoja SQL sakinio LIMIT dali pagal page ir per_page
	 *
	 * @param int $page
	 * @param int $per_page
	 * @return string
	 */
	function sqlLimit($page, $per_page) {
		$sql = "";
		if ($per_page != 0) {
			$sql = "LIMIT ".($page*$per_page).", ".$per_page;
		}
		return $sql;
	}
	
	/**
	 * Generuoja SQL sakinio ORDER BY dali pagal order
	 *
	 * @param array $order
	 * @return string
	 */
	function sqlOrder($orderby, $ordertype) {
		$sql = "";
		if ($orderby) {
			$sql = "ORDER BY `$orderby` $ordertype";
		}
		return $sql;
	}
	
	
}

?>