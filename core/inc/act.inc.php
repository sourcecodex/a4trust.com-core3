<?php

/**
 * Act.inc.php
 *
 * @package Tefo core3
 * @version 2014.03.30
 * @author Tefo <tefo05@gmail.com>
 */

// ========== GLOBAL acts ==========================================================

$act_file_name = $config['control_dir'].'act/_global.php';
if (file_exists($act_file_name)) { include_once $act_file_name; }

// ========== Default GET acts =====================================================

if (!empty($_GET['act'])) {

	if ($_GET['act'] == "debug") {
		$coreSession->session['debugger'] = 1;
		redirect($pages->url);
	}

	if ($_GET['act'] == "stopdebug") {
		unset($coreSession->session['debugger']);
		redirect($pages->url);
	}

	if ($_GET['act'] == "check") {
		$coreSession->session['checker'] = 1;
		redirect($pages->url);
	}

	if ($_GET['act'] == "stopcheck") {
		unset($coreSession->session['checker']);
		redirect($pages->url);
	}

}

// ========== POST acts ===============================================================

if (!empty($_POST['controller']) && !empty($_POST['act'])) {

	if (isset($GLOBALS[$_POST['controller']])) {

		$errors = array();

		if (in_array($_POST['act'], $GLOBALS[$_POST['controller']]->public_acts) ||
			(in_array($_POST['act'], $GLOBALS[$_POST['controller']]->logged_acts) && $users->isLogged)) {

			$coreSQL->query_class = $_POST['controller'].'->'.$_POST['act'];

			$_POST['action_result'] = $GLOBALS[$_POST['controller']]->$_POST['act']($_POST);

			foreach ($smarty->get_template_vars() as $var => $value) {
				if (substr($var, 0, 6) == "error_") {
					$errors[$value][] = $var;
				}
			}

			//if ($config['action_logs']) {
			//	$log_data = $coreLogs->getLogData($_POST);
			//	$coreSQL->query_class = "coreLogs";
			//	$coreLogs->add($log_data);
			//}

			//if (isset($_POST['act_redirect'])) {
			//	redirect(clean($_POST['act_redirect']));
			//}

		}
		else {
			echo "Error: Act '".clean($_POST['act'])."' is not public in '".clean($_POST['controller'])."' controller.";
		}
		
		$errors_return = array();
		foreach ($_POST as $form_field => $value) {
			if ($form_field != "controller" && $form_field != "act" &&
				$form_field != "act_redirect" && $form_field != "action_result") {

				if (isset($errors[$form_field])) {
					foreach ($errors[$form_field] as $error) {
						$errors_return[] = array($form_field, $error, $translations->t($error));
					}
				}
				else {
					$errors_return[] = array($form_field, "ok");
				}
			}
		}

		$smarty->assign('act_errors', json_encode($errors_return));

	} else {
		echo "Error: No such controller '".clean($_POST['controller'])."'.";
	}

}

?>