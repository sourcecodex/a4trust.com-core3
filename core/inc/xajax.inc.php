<?php

/**
 * Xajax.inc.php
 *
 * @package Tefo core3
 * @version 2014.03.24
 * @author Tefo <tefo05@gmail.com>
 */

if (isset($config['xajax_ext'])) {

	$xajax_functions_dir = $config['core_dir']."project/control/xajax/";

	include_once $xajax_functions_dir."checkForm.xajax.php";
	include_once $xajax_functions_dir."createPopup.xajax.php";
	include_once $xajax_functions_dir."tableAddItem.xajax.php";
	include_once $xajax_functions_dir."tableEditItem.xajax.php";
	include_once $xajax_functions_dir."tableDeleteItem.xajax.php";
	include_once $xajax_functions_dir."tableDeleteItems.xajax.php";
	include_once $xajax_functions_dir."tableFunctionCall.xajax.php";

	if ($config['project_type'] == "admin") {
		include_once $xajax_functions_dir."getMailerTemplate.xajax.php";
	}
}

if ($dhandle = opendir($config['control_dir']."xajax")) {
	while (false !== ($file = readdir($dhandle))) {
		if ($file != "." && $file != ".." && $file != ".svn") {

			$current_file = $config['control_dir']."xajax/".$file;
			if (is_dir($current_file)) {
				$dir_files = scandir($current_file);
				foreach ($dir_files as $dir_file) {
					if ($dir_file != "." && $dir_file != ".." && $dir_file != ".svn") {
						if (substr($dir_file, -10, 10) == ".xajax.php") {
							include_once $current_file."/".$dir_file;
						}
					}
				}
			}
			elseif (substr($file, -10, 10) == ".xajax.php") {
				include_once $current_file;
			}
		}
	}
	closedir($dhandle);
}


?>