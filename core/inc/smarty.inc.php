<?php

/**
 * Smarty.inc.php
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

require_once $config['core_dir']."libs/smarty/Smarty.class.php";
$smarty = new Smarty;

$smarty->debugging = false;
$smarty->caching = false;
$smarty->config_dir = $config['core_dir']."libs/smarty/configs";
$smarty->template_dir = $config['views_dir']."tpl";
$smarty->compile_dir = $config['views_dir']."smarty";

// Default Modifiers

$smarty->register_modifier('in_array', 'smarty_modifier_in_array');
function smarty_modifier_in_array($item, $array) {
	return in_array($item, $array);
}

$smarty->register_modifier('print_float', 'smarty_print_float');
function smarty_print_float($item) {
	return $item = (float)$item."";
}

$smarty->register_modifier('print_time', 'smarty_print_time');
function smarty_print_time($seconds) {
	$hours = floor($seconds / 3600);
	$mins = floor(($seconds - ($hours*3600)) / 60);
	$secs = floor($seconds % 60);
	return ($hours>0?$hours.':':'').($mins>0?str_pad($mins, 2, "0", STR_PAD_LEFT).':':'').str_pad($secs, 2, "0", STR_PAD_LEFT);
}

$smarty->register_modifier('highlight', 'smarty_modifier_highlight');
function smarty_modifier_highlight(&$text='', $word='') {
	$new_text = $text;
	if ($word) {
	   $new_text = str_ireplace($word, "<span class='hilight'>{$word}</span>", $text);
	}
	return($new_text);
}

$smarty->register_modifier('dashed_param', 'smarty_dashed_param');
function smarty_dashed_param($item) {
	$specialCharacters = array(
		'&quot;' => '',
		'&amp;' => '',
		'?' => '',
		'<' => '',
		'>' => '',
		':' => '',
		'|' => '',
		"\\" => '',
		'#' => '',
		'$' => '',
		'%' => '',
		'&' => '',
		'@' => '',
		'.' => '',
		'€' => '',
		'+' => '',
		'=' => '',
		'§' => '',
		'/' => '',
		'*' => '',
		';' => '',
		',' => '',
		'"' => '',
		"'" => '',
		'»' => '',
		'«' => '',
		' ' => '-'
	);

	while (list($character, $replacement) = each($specialCharacters)) {
		$item = str_replace($character, $replacement, $item);
		$item = mb_strtolower($item, "UTF-8");
	}

	return $item;
}

$smarty->register_modifier('removeZeros', 'smarty_modifier_removeZeros');
function smarty_modifier_removeZeros($str) {
	global $config;

	$array = explode(".", $str);
	if ($array[1] == "00") return $array[0];
	return $str;
}

$smarty->register_modifier('translate', 'smarty_translate');
function smarty_translate($html) {
	global $coreLangs;

	preg_match_all('/<t>(.*?)<\/t>/s', $html, $matches);
	$phrases = $matches[1];

	foreach ($phrases as $phrase) {
		$translation = $coreLangs->t($phrase);
		$html = str_replace($phrase, $translation, $html);
	}

	$html = str_replace("<t>", "", $html);
	$html = str_replace("</t>", "", $html);

	return $html;
}

$smarty->register_modifier('add_links', 'smarty_add_links');
function smarty_add_links($str) {
	if (strpos($str, "<a") !== false) {
		// dont change
	}
	else {
		$str = preg_replace('@((www|http://)[^ ]+)@', '<a href="http://\1" target="_blank">\1</a>', $str);
		$str = preg_replace('~<a[^>]*?href="(.*?(\.gif|\.jpeg|\.jpg|\.png))".*?</a>~', 
							'<img src="$1" style="max-width:600px;" />', $str);
		$str = str_replace('http://http://', 'http://', $str);
	}
	return $str;
}

// Default Functions

$smarty->register_function('pre', 'smarty_pre');
function smarty_pre($params, &$smarty) {
	echo '<pre>'; print_r($params['var']); echo '</pre>';
}

$smarty->register_function('assign_by_name', 'smarty_assign_by_name');
function smarty_assign_by_name($params, &$smarty) {
	$smarty->_tpl_vars[$params['var']] = $smarty->get_template_vars($params['value']);
}

$smarty->register_function('assign_array', 'smarty_assign_array');
function smarty_assign_array($params, &$smarty) {
	extract($params);
	$smarty->_tpl_vars[$assign][$key] = $value;
}

$smarty->register_function('explode', 'smarty_explode');
function smarty_explode($params, &$smarty) {
	extract($params);
	$smarty->_tpl_vars[$assign] = explode($separator, $string);
}

$smarty->register_function('t', 'smarty_t');
function smarty_t($params, &$smarty) {
	global $coreLangs;
	return $coreLangs->t($params['t'], $smarty->_current_included_file);
}

$smarty->register_function('t2', 'smarty_t2');
function smarty_t2($params, &$smarty) {
	global $coreLangs;
	return $coreLangs->t2($params['t'], $params['lang'], $smarty->_current_included_file);
}

$smarty->register_function('file_exists', 'smarty_file_exists');
function smarty_file_exists($params, &$smarty) {
	global $config;
	$smarty->_tpl_vars["file_exists"] = file_exists($config['main_dir'].$params['filename']);
}

$smarty->register_function('is_file_exists', 'smarty_is_file_exists');
function smarty_is_file_exists($params, &$smarty) {
	$smarty->_tpl_vars["is_file_exists"] = file_exists($params['filename']);
}

$smarty->register_function('str_pad', 'smarty_str_pad');
function smarty_str_pad($params, &$smarty) {
	return str_pad((int)$params['var'], (int)$params['length'], "0", STR_PAD_LEFT);
}

$smarty->register_function('print_price', 'smarty_print_price');
function smarty_print_price($params, &$smarty) {
	return sprintf('%.2f', $params['value']);
}

$smarty->register_function('get_age', 'smarty_get_age');
function smarty_get_age($params, &$smarty) {
	return getAge($params['date']);
}

$smarty->register_function('get_time_past', 'smarty_time_past');
function smarty_time_past($params, &$smarty) {
	return secondsToTime(time() - strtotime($params['time']));
}

$smarty->register_function('trans_id', 'smarty_trans_id');
function smarty_trans_id($params, &$smarty) {
	if ((int)$params['id'] != 0) {
		return str_pad((int)$params['id'], 5, "0", STR_PAD_LEFT);
	}
}

$smarty->register_function('phpinfo', 'smarty_phpinfo');
function smarty_phpinfo($params, &$smarty) {
	return phpinfo();
}

?>