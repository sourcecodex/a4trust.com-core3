<?php

/**
 * Init.inc.php
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

$coreControllers = array();

function init_controller($controller_name, $path = '') {
	global $config, $coreSQL, $coreSession, $coreControllers;

	if (isset($coreControllers[$controller_name])) {
		echo "Error: Duplicate controller `".$controller_name."`";
	}
	else {
		require_once $config['control_dir']."controllers/".$path.$controller_name.'.class.php';
		$coreSQL->query_class = $controller_name.".class.php";
		$GLOBALS[$controller_name] = new $controller_name();
		$coreControllers[$controller_name] = $GLOBALS[$controller_name];

		if (isset($coreSession->session['checker'])) {
			$table_exists = (bool)$coreSQL->query("SHOW TABLES LIKE '".$GLOBALS[$controller_name]->table."'");
			if (!$table_exists) {
				$GLOBALS[$controller_name]->createTableStructure();
				$coreSession->session['checker_logs'][] = "Created table structure for `".$GLOBALS[$controller_name]->table."`";
			}
			else {
				$GLOBALS[$controller_name]->checkTableStructure();
			}
		}
	}
}

$coreSQL->query_class = "core_init";

foreach ($config['init_controllers'] as $controller_name => $on) {
	if ($on) {
		if (isset($coreControllers[$controller_name])) {
			echo "Error: Duplicate controller `".$controller_name."`";
		}
		else {
			require_once $config['core_control_dir']."controllers/".$controller_name.".class.php";
			$coreSQL->query_class = "core/".$controller_name.".class.php";
			$GLOBALS[$controller_name] = new $controller_name();
			$coreControllers[$controller_name] = $GLOBALS[$controller_name];
		}
	}
}

$coreSQL->query_class = "project_init";

// GLOBAL inits

$init_file_name = $config['control_dir'].'init/_global.php';
if (file_exists($init_file_name)) { include_once $init_file_name; }

if (isset($config['disable_auto_init'])) {
	// Do nothing
}
else {

	$init_first = array("languages", "translations", "users", "pages", "popups");

	foreach ($init_first as $controller_name) {
		$init_file_name = $config['control_dir'].'controllers/'.$controller_name.'.class.php';
		if (file_exists($init_file_name)) {
			init_controller($controller_name);
		}
	}

	$files = scandir($config['control_dir']."controllers");
	foreach ($files as $file) {
		if ($file != "." && $file != ".." && $file != ".svn") {
			$init_file_name = $config['control_dir']."controllers/".$file;
			if (is_dir($init_file_name)) {
				$dir_files = scandir($init_file_name);
				foreach ($dir_files as $dir_file) {
					if ($dir_file != "." && $dir_file != ".." && $dir_file != ".svn") {
						if (substr($dir_file, -10, 10) == ".class.php") {
							$controller_name = str_replace(".class.php", "", $dir_file);
							if (!in_array($controller_name, $init_first)) {
								init_controller($controller_name, $file.'/');
							}
						}
					}
				}
			}
			else {
				if (substr($file, -10, 10) == ".class.php") {
					$controller_name = str_replace(".class.php", "", $file);
					if (!in_array($controller_name, $init_first)) {
						init_controller($controller_name);
					}
				}
			}
		}
	}

}

// fix redirects
if ($config['project_type'] == "admin" && isset($pages)) {
	if (isset($languages)) {
		$languages->add_form['redirect'] = $languages->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($translations)) {
		$translations->add_form['redirect'] = $translations->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($users)) {
		$users->add_form['redirect'] = $users->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($pages)) {
		$pages->add_form['redirect'] = $pages->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($popups)) {
		$popups->add_form['redirect'] = $popups->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($mailer_emails)) {
		$mailer_emails->add_form['redirect'] = $mailer_emails->edit_form['redirect'] = $mailer_emails->send_form['redirect'] = $mailer_emails->send_to_users_form['redirect'] = $pages->request_url;
	}
	if (isset($mailer_templates)) {
		$mailer_templates->add_form['redirect'] = $mailer_templates->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($options)) {
		$options->add_form['redirect'] = $options->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($crm_contacts)) {
		$crm_contacts->add_form['redirect'] = $crm_contacts->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($crm_companies)) {
		$crm_companies->add_form['redirect'] = $crm_companies->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($seo_contents)) {
		$seo_contents->add_form['redirect'] = $seo_contents->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($seo_links)) {
		$seo_links->add_form['redirect'] = $seo_links->edit_form['redirect'] = $pages->request_url;
	}
	if (isset($bots)) {
		$bots->add_form['redirect'] = $bots->edit_form['redirect'] = $pages->request_url;
	}
}

$coreSQL->query_class = "project_init_after";
$init_file_name = $config['control_dir'].'init/_global_after.php';
if (file_exists($init_file_name)) { include_once $init_file_name; }

$coreSQL->query_class = "coreSQL";

?>