<?php

/**
 * Misc.inc.php
 *
 * @package Tefo core3
 * @version 2014.04.23
 * @author Tefo <tefo05@gmail.com>
 */

/**
 * Spausdina kintamojo turini
 *
 * @param mixed $result
 */
function pre($result) {
	echo '<pre>'; print_r($result); echo '</pre>';
}

/**
 * Grazina microtime
 *
 * @return float
 */
function microtime_float() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

/**
 * Nukreipia nurodytu URL, i sesija issaugo buvusio puslapio uzklausas (naudojama debugeri)
 *
 * @param string $url
 */
function redirect($url, $absolute_url = false) {
	global $coreSQL, $coreSession, $config;
	
	$coreSession->session['debug']['sql_queries'] = $coreSQL->queries;
	$coreSession->close();

	if ($absolute_url) {
		header("Location: ".$url);
	}
	else header("Location: ".$config['site_url'].$url);

	die();
}

function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

function is_valid_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL)
        && preg_match('/@.+\./', $email);
}

function isValidInetAddress($data, $strict = false) {
	$regex = $strict?
	  '/^([.0-9a-z_-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,4})$/i' :
	   '/^([*+!.&#$¦\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,4})$/i'
	;
	if (preg_match($regex, trim($data), $matches)) {
		return array($matches[1], $matches[2]);
	} else {
		return false;
	}
}

/**
 * Isvalo kintamaji nuo sisteminiu simboliu pries dedant i DB
 *
 * @param mixed $var
 * @return mixed
 */
function clean($var) {
	$var = str_replace("<", "&lt;", $var);
	$var = str_replace(">", "&gt;", $var);
	return addslashes($var);
}

function br2nl($str) {
	$str = preg_replace("/(\r\n|\n|\r)/", "", $str);
	return preg_replace("=<br */?>=i", "\n", $str);
}

/**
 * Isvalo masyva
 *
 * @param array $array
 * @return array
 */
function cleanArray($array) {
	$clean = array();
	if ($array) {
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$clean[$key] = cleanArray($value);
			}
			else {
				$clean[$key] = clean($value);
			}
		}
	}
	return $clean;
}

function array_insert(&$array, $position, $insert_array) {
	$first_array = array_splice($array, 0, $position);
	$array = array_merge($first_array, $insert_array, $array);
}

function valueRange($value, $low, $high) {
	if ($value < $low) return $low;
	else if ($value > $high) return $high;
	else return $value;
}

/**
 * Paruosia puslapiavimo informacija
 *
 * @param int $result_count
 * @param int $count
 * @return array
 */
function paging($result_count, $count) {
	if ($count > 0) {
		$pages_count = ceil($result_count / $count);
		$pages = array();
		for ($i=1; $i <= $pages_count; $i++) {
			$pages[$i]['number'] = $i;
			$pages[$i]['from'] = ($i-1)*$count+1;
			if ($i*$count > $result_count) $pages[$i]['to'] = $result_count;
			else $pages[$i]['to'] = $i*$count;
		}
		return $pages;
	}
	else return array();
}

function downloadFile($filename, $path) {
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="'.$filename.'"');
	readfile($path); 
}

function isEmptyDir($dir) {
	if ($dh = @opendir($dir)) {
		while ($file = readdir($dh)) {
			if ($file != '.' && $file != '..') {
				closedir($dh);
				return false;
			}
		}
		closedir($dh);
		return true;
	}
	else return false; // whatever the reason is : no such dir, not a dir, not readable
}

if (!function_exists('exif_imagetype')) {
	function exif_imagetype($filename) {
		if ((list($width, $height, $type, $attr) = getimagesize($filename)) !== false) {
			return $type;
		}
	return false;
	}
}

/**
* Sukuria sumažintą paveiksliuko kopiją
*
* @param resource $original - paveiksliukas, kurio kopija daroma
* @param resource $thumbnail - kur išsaugoti kopiją
* @param int $width - kopijos plotis
* @param int $height - kopijos aukštis
* @param bool $proportion - ar išlaikyti proporcingus aukštį ir plotį
*/
function create_thumbnail($original, $thumbnail, $width, $height, $proportion) 
{
	list ($width_orig, $height_orig) = getimagesize($original);
	
	if ($proportion) {
		if ($width && ($width_orig < $height_orig)) {
			$width = ($height / $height_orig) * $width_orig;
		}
		else {
			$height = ($width / $width_orig) * $height_orig;
		}
	}
	
	// !!! nesukuria didesnio nei orginalas
	if ($width_orig < $width) $width = $width_orig;
	if ($height_orig < $height) $height = $height_orig;
	
	$image_p = imagecreatetruecolor($width, $height);
	imagealphablending($image_p, false);
	imagesavealpha($image_p, true);
		
	$imageType = exif_imagetype($original);
	$image;
	switch ($imageType){
		case IMAGETYPE_GIF:
			$image = imagecreatefromgif($original);
		break;
		case IMAGETYPE_JPEG:
			$image = imagecreatefromjpeg($original);
		break;
		case IMAGETYPE_PNG:
			$image = imagecreatefrompng($original);
		break;
		default:
			$image = imagecreatetruecolor(1, 1);
	}
	
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	imagejpeg($image_p, $thumbnail, 100);
	imagedestroy($image_p);
	
	return;
}

function create_thumbnail_png($original, $thumbnail, $width, $height, $proportion) 
{
	list ($width_orig, $height_orig) = getimagesize($original);
	
	if ($proportion) {
		if ($width && ($width_orig < $height_orig)) {
			$width = ($height / $height_orig) * $width_orig;
		}
		else {
			$height = ($width / $width_orig) * $height_orig;
		}
	}
	
	// !!! nesukuria didesnio nei orginalas
	if ($width_orig < $width) $width = $width_orig;
	if ($height_orig < $height) $height = $height_orig;
	
	$image_p = imagecreatetruecolor($width, $height);
	imagealphablending($image_p, false);
	imagesavealpha($image_p, true);
	
	$imageType = exif_imagetype($original);
	$image;
	switch ($imageType){
		case IMAGETYPE_GIF:
			$image = imagecreatefromgif($original);
		break;
		case IMAGETYPE_JPEG:
			$image = imagecreatefromjpeg($original);
		break;
		case IMAGETYPE_PNG:
			$image = imagecreatefrompng($original);
		break;
		default:
			$image = imagecreatetruecolor(1, 1);
	}
	
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	imagepng($image_p, $thumbnail, 9);
	imagedestroy($image_p);
	
	return;
}

function getIp() {
	$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
	return $ip;
}

function send_mail($to, $from, $from_name, $subject, $html) {
	$headers_html = 'Content-type: text/html; charset="utf-8"'."\n";
	$headers_html .= 'MIME-Version: 1.0'."\n";
	$headers_html .= 'Reply-To: ' . $from_name . '<' . $from . '>'."\n";
	$headers_html .= 'From: ' . $from_name . '<' . $from . '>'."\n\r";
	return mail($to, $subject, $html, $headers_html);
}

function send_mail_with_attachment($to, $from, $from_name, $subject, $html, $filename) {
	$random_hash = md5(date('r', time()));

	$headers_html = 'Content-Type: multipart/mixed; boundary="PHP-mixed-'.$random_hash.'"'."\n";
	$headers_html .= 'MIME-Version: 1.0'."\n";
	$headers_html .= 'Reply-To: ' . $from_name . '<' . $from . '>'."\n";
	$headers_html .= 'From: ' . $from_name . '<' . $from . '>'."\n\r";

	$attachment = chunk_split(base64_encode(file_get_contents($filename)));

	$message = "--PHP-mixed-$random_hash"."\n";
	$message .= 'Content-Type: multipart/alternative; boundary="PHP-alt-'.$random_hash.'"'."\n";
	$message .= "--PHP-alt-$random_hash"."\n";
	$message .= 'Content-Type: text/html; charset="utf-8"'."\n";
	$message .= 'Content-Transfer-Encoding: 8bit'."\n\n".$html."\n\n";
	$message .= "--PHP-mixed-$random_hash"."\n";
	$message .= 'Content-Type: application/zip; name="'.basename($filename).'"'."\n";
	$message .= 'Content-Transfer-Encoding: base64'."\n";
	$message .= 'Content-Disposition: attachment'."\n\n".$attachment."\n\n";
	$message .= "--PHP-mixed-$random_hash"."\n";

	return mail($to, $subject, $message, $headers_html);
}

function send_template_mail($to, $from, $from_name, $subject, $template) {
	global $smarty;
	return send_mail($to, $from, $from_name, $subject, $smarty->fetch($template));
}

function generatePassword($length = 8) {
	$password = "";
	$possible = "0123456789bcdfghjkmnpqrstvwxyz";
	$i = 0;
	while ($i < $length) {
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
		if (!strstr($password, $char)) {
			$password .= $char;
			$i++;
		}
	}
	return $password;
}

function getAge($date_of_birth) {
	list($Y, $m, $d) = explode("-", $date_of_birth);
	return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
}

function parseTextForEmail($text) {
	$email = array();
	$invalid_email = array();

	$text = preg_replace("/[^A-Za-z._0-9@ \-]/"," ",$text);

	$token = trim(strtok($text, " "));

	while($token !== "") {

		if(strpos($token, "@") !== false) {

			$token = preg_replace("/[^A-Za-z._0-9@\-]/","", $token);

			//checking to see if this is a valid email address
			if (isValidEmail($token)) {
				$email[] = strtolower($token);
			}
			else {
				$invalid_email[] = strtolower($token);
			}
		}

		$token = trim(strtok(" "));
	}

	$email = array_unique($email);
	$invalid_email = array_unique($invalid_email);

	return array("valid_email"=>$email, "invalid_email" => $invalid_email);

}

if(!function_exists('mime_content_type')) {

    function mime_content_type($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
}

/*
function smarty_truncate($string, $length = 80, $etc = '...',
                                  $break_words = false, $middle = false)
{
    if ($length == 0)
        return '';

    if (strlen($string) > $length) {
        $length -= min($length, strlen($etc));
        if (!$break_words && !$middle) {
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));
        }
        if(!$middle) {
            return substr($string, 0, $length) . $etc;
        } else {
            return substr($string, 0, $length/2) . $etc . substr($string, -$length/2);
        }
    } else {
        return $string;
    }
}
*/

function isWordChar($char) {
	return (
		// English
		ctype_alpha($char) || /*$char == "-" || $char == "'" ||*/
		// Lithuanian
		(strpos("ĄąĘęĖėĮįŲųŪūČčŠšŽž", $char) !== false) ||
		// Spanish
		(strpos("ÁÉÍÓÚÑÜáéíóúñü", $char) !== false) ||
		// French
		(strpos("ÀÂÄÈÉÊËÎÏÔŒÙÛÜŸàâäèéêëîïôœùûüÿÇç", $char) !== false) ||
		// German
		(strpos("ÄäÖöÜüß", $char) !== false) ||
		// Italian
		(strpos("ÀÈÉÌÒÓÙàèéìòóù", $char) !== false) ||
		// Portuguese
		(strpos("ÀÁÂÃÉÊÍÓÔÕÚÜàáâãéêíóôõúüÇç", $char) !== false) ||
		// Russian
		(strpos("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщьъыэюя", $char) !== false)
	);
}

function strToHex($string) {
	$hex = '';
	for ($i = 0; $i < strlen($string); $i++) {
		$hex .= dechex(ord($string[$i]));
	}
	return $hex;
}

function countryCityFromIP($ipAddr) {
	
	//function to find country and city from IP address
	//Developed by Roshan Bhattarai [url]http://roshanbh.com.np[/url]

	//verify the IP address for the
	if (ip2long($ipAddr)== -1 || ip2long($ipAddr) === false) {
		return array();
	}

	$ipDetail=array(); //initialize a blank array

	//get the XML result from hostip.info
	$xml = file_get_contents("http://api.hostip.info/?ip=".$ipAddr);

	//get the city name inside the node <gml:name> and </gml:name>
	preg_match("@<Hostip>(\s)*<gml:name>(.*?)</gml:name>@si",$xml,$match);

	//assing the city name to the array
	$ipDetail['city']=$match[2];

	//get the country name inside the node <countryName> and </countryName>
	preg_match("@<countryName>(.*?)</countryName>@si",$xml,$matches);

	//assign the country name to the $ipDetail array
	$ipDetail['country']=$matches[1];

	//get the country name inside the node <countryName> and </countryName>
	preg_match("@<countryAbbrev>(.*?)</countryAbbrev>@si",$xml,$cc_match);
	$ipDetail['country_code']=$cc_match[1]; //assing the country code to array

	//return the array containing city, country and country code
	return $ipDetail;

}

function getPercent($number, $total) {
	if ($number > 0 && $total > 0) {
		$percentage = round($number * 100 / $total)."%";
	}
	else $percentage = "0%";

	return $percentage;
}

function cleanSpecialChars($string) {
	$specialCharacters = array(
		'&quot;' => '',
		'&amp;' => '',
		'?' => '',
		'<' => '',
		'>' => '',
		':' => '',
		'|' => '',
		"\\" => '',
		'/' => '',
		'#' => '',
		'$' => '',
		'%' => '',
		'&' => '',
		'@' => '',
		'.' => '',
		'€' => '',
		'+' => '',
		'=' => '',
		'§' => '',
		'*' => '',
		';' => '',
		',' => '',
		'"' => '',
		"'" => '',
		'»' => '',
		'«' => '',
		' ' => '',
	);

	while (list($character, $replacement) = each($specialCharacters)) {
		$string = str_replace($character, $replacement, $string);
	}

	return $string;
}

function get_string_between($string, $start, $end) {
    $string = " ".$string;
    $ini = strpos($string, $start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function secondsToTime($seconds) {
	$dtF = new \DateTime('@0');
	$dtT = new \DateTime("@$seconds");
	return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes');
}

?>