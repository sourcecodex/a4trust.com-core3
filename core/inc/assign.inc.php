<?php

/**
 * Assign.inc.php
 *
 * @package Tefo core3
 * @version 2014.03.24
 * @author Tefo <tefo05@gmail.com>
 */

// ========== Default styles, scripts and libs =====================================================

$smarty->assign('default_css_styles', array("default.css"));
$smarty->assign('default_js_scripts', array("misc_functions.js", "validation.js", "popup.js"));
$smarty->assign('default_libs', array("jquery", "bootstrap", "font-awesome"));

// ========== Xajax Javascript =====================================================================

if (isset($config['project_url'])) {
	$smarty->assign('xajax_js', $xajax->getJavascript($config['project_url']."views/js/", "xajax.js"));
}
elseif (isset($config['views_url'])) {
	$smarty->assign('xajax_js', $xajax->getJavascript($config['views_url']."js/", "xajax.js"));
}
else {
	$smarty->assign('xajax_js', $xajax->getJavascript($config['site_url']."views/js/", "xajax.js"));
}

// ========== Assign by pages ======================================================================

$coreSQL->query_class = 'assign/_global.php';
$assign_file_name = $config['control_dir'].'assign/_global.php';
if (file_exists($assign_file_name)) { include_once $assign_file_name; }

if (isset($pages)) {
	if ($pages->page_found) {
		if ($pages->current_page['type'] == "core") {
			$coreSQL->query_class = 'core/'.$pages->include_assign;
			$assign_file_name = $config['core_control_dir'].$pages->include_assign;
		}
		else {
			$coreSQL->query_class = $pages->include_assign;
			$assign_file_name = $config['control_dir'].$pages->include_assign;
		}
		if (file_exists($assign_file_name)) {
			include_once $assign_file_name;
		}
	}
}

$coreSQL->query_class = 'assign/_global_after.php';
$assign_file_name = $config['control_dir'].'assign/_global_after.php';
if (file_exists($assign_file_name)) {
	include_once $assign_file_name;
}

?>