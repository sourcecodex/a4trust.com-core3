<?php
/**
 * Config.inc.php
 *
 * @package Tefo core3
 * @version 2014.04.16
 * @author Tefo <tefo05@gmail.com>
 */

if (empty($config['project_type'])) { $config['project_type'] = 'project'; }

// paths inner
if (empty($config['main_dir'])) {
	$config['main_dir'] = $_SERVER['DOCUMENT_ROOT'].'/'.(isset($config['admin_path'])?$config['admin_path'].'/':'');
}

$config['control_dir'] = $config['main_dir'].'control/';
$config['views_dir'] = $config['main_dir'].'views/';
$config['tpl_dir'] = $config['views_dir'].'tpl/';

if ($config['project_type'] == 'admin') {
	$config['core_control_dir'] = $config['core_dir'].'admin/control/';
	$config['core_views_dir'] = $config['core_dir'].'admin/views/';
	$config['core_tpl_dir'] = $config['core_views_dir'].'tpl/';
}
elseif ($config['project_type'] == "project" || $config['project_type'] == "app") {
	$config['core_control_dir'] = $config['core_dir'].'project/control/';
	$config['core_views_dir'] = $config['core_dir'].'project/views/';
	$config['core_tpl_dir'] = $config['core_views_dir'].'tpl/';
}

$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

// paths outer
if ($config['project_type'] == "admin") {
	if (isset($config['admin_path'])) {
		$config['site_url'] = $protocol.$config['project'].'/'.$config['admin_path'].'/';
	}
	else {
		$config['site_url'] = $protocol.'admin.'.$config['project'].'/';
	}

	$config['project_url'] = $protocol.$config['project'].'/';
}
elseif ($config['project_type'] == "project") {
	$config['site_url'] = $protocol.$config['project'].'/';
	$config['project_url'] = $protocol.$config['project'].'/';
}
elseif ($config['project_type'] == "app") {
	$config['site_url'] = $protocol.'app.'.$config['project'].'/';
	$config['project_url'] = $protocol.$config['project'].'/';
}
$config['views_url'] = $config['site_url'].'views/';

// session params

if (empty($config['session_cache'])) { $config['session_cache'] = false; }
if (empty($config['session_path'])) { $config['session_path'] = '/'; }
if (empty($config['user_cookie'])) { $config['user_cookie'] = true; }
if (empty($config['user_cookie_time'])) { $config['user_cookie_time'] = time()+60*60*24*14; } // 14 days
if (empty($config['cookie_path'])) { $config['cookie_path'] = '/'; }

// language params

if (empty($config['default_lang'])) { $config['default_lang'] = 'en'; }
if (empty($config['code_lang'])) { $config['code_lang'] = 'en'; }

// mysql params

if (empty($config['mysql_server'])) { $config['mysql_server'] = 'localhost'; }


// modules

if ($config['project_type'] == "admin") {

	// default

	$config['init_controllers'] = array(
		'admin_pages' => true,
		'admin_popups' => true,
		'admin_users' => true,
		'admin_languages' => true,
		'admin_translations' => true,
		'admin_options' => true,
		'admin_mailer_emails' => true,
		'admin_mailer_templates' => true,
		'admin_mailer_contacts' => true,
		'admin_mailer_users' => true,
		'admin_countries' => true,
		'admin_adclicks' => true,
		'admin_refclicks' => true,
		'admin_bots' => true,
	);

	// payments

	if ($config['module_payments']) {
		$config['init_controllers'] += array(
			'admin_accounting_payments' => true,
			//'admin_accounting_products' => true,
			//'admin_paypal' => true,
		);
	}

	// CRM

	if ($config['module_crm']) {
		$config['init_controllers'] += array(
			'admin_crm_contacts' => true,
			'admin_crm_companies' => true,
		);
	}
	
	// SEO

	if ($config['module_seo']) {
		$config['init_controllers'] += array(
			'admin_seo_contents' => true,
			'admin_seo_links' => true,
		);
	}
}
elseif ($config['project_type'] == "project" || $config['project_type'] == "app") {

	// default

	$config['init_controllers'] = array(
		'project_pages' => true,
		'project_popups' => true,
		'project_users' => true,
		'project_languages' => true,
		'project_translations' => true,
		'project_options' => true,
		'project_mailer_emails' => true,
		'project_mailer_contacts' => true,
		'project_mailer_users' => true,
		'project_countries' => true,
	);

	// payments

	if ($config['module_payments']) {
		$config['init_controllers'] += array(
			'project_accounting_payments' => true,
			'project_accounting_products' => true,
			'project_paypal' => true,
		);
	}

	// CRM

	if ($config['module_crm']) {
		$config['init_controllers'] += array(
			'project_crm_contacts' => true,
			'project_crm_companies' => true,
		);
	}
	
	// hybridauth
	
	$config['hybridauth_include'] = $config['main_dir'].'libs/hybridauth/Hybrid/Auth.php';
	$config['hybridauth_config'] = $config['main_dir'].'libs/hybridauth/config.php';
	
}

// mailer params

if (empty($config['mailer_smtp_debug'])) { $config['mailer_smtp_debug'] = false; }
if (empty($config['mailer_smtp_host'])) { $config['mailer_smtp_host'] = 'localhost'; }
if (empty($config['mailer_smtp_port'])) { $config['mailer_smtp_port'] = null; }
if (empty($config['mailer_mail_flow_enabled'])) { $config['mailer_mail_flow_enabled'] = true; }
if (empty($config['mailer_redirect'])) { $config['mailer_redirect'] = 'admin@'.$config['project']; }

if (empty($config['mailer_user_from'])) { $config['mailer_user_from'] = 'service@'.$config['project']; }
if (empty($config['mailer_user_name'])) { $config['mailer_user_name'] = $config['project']; }

if (empty($config['contacts_email'])) { $config['contacts_email'] = 'admin@'.$config['project']; }

// other

if (empty($config['templates_extension'])) { $config['templates_extension'] = ".tpl"; }
if (empty($config['xajax_ext'])) { $config['xajax_ext'] = true; }

$config['date'] = date("Y-m-d");
$config['time'] = date("H:i:s");

?>
