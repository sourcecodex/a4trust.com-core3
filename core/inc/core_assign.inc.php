<?php

/**
 * Core_assign.inc.php
 *
 * @package Tefo core3
 * @version 2014.03.24
 * @author Tefo <tefo05@gmail.com>
 */

$template_config = $config;
unset($template_config['mysql_server']);
unset($template_config['mysql_user']);
unset($template_config['mysql_pass']);
unset($template_config['mysql_database']);

$smarty->assign('config', $template_config);
$smarty->assign('core_com', $config['core_dir'].'project/views/components/');
$smarty->assign('core_controllers', $coreControllers);
$smarty->assign('core_session', $coreSession->session);

if (isset($pages)) {
	$smarty->assign('core_pages', $pages);
	$smarty->assign('page_info', $coreSession->session['page_info'][$pages->url]);
}
if (isset($popups)) { $smarty->assign('core_popups', $popups); }
if (isset($languages)) { $smarty->assign('core_langs', $languages); }
if (isset($users)) { $smarty->assign('core_user', $users); }

if ($coreSQL->debug) {
	$smarty->assign('debug', true);
	$smarty->assign('debug_sql', $coreSQL->queries);
	//$smarty->assign('debug_sql2', $coreSQL2->queries);
	$smarty->assign('debug_session_sql', $coreSession->session['debug']['sql_queries']);
}

?>
