<?php

/**
 * Load.php
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

if (isset($_GET['xajax_call'])) {
	error_reporting(E_ERROR | E_PARSE);
}

// ======== Load config, base functions, Controller system, Session and MySQL =========

include_once $config['core_dir']."core/inc/config.inc.php";

include_once $config['core_dir']."core/inc/misc.inc.php";

$START_TIME = microtime_float();

require_once $config['core_dir']."core/classes/controller.class.php";

require_once $config['core_dir']."core/classes/session.class.php";
$coreSession = new coreSession();

require_once $config['core_dir']."core/classes/mysql.class.php";
$coreSQL = new coreSQL($config['mysql_server'], $config['mysql_user'], $config['mysql_pass'], $config['mysql_database']);
$coreSQL->connect();
$coreSQL->query("SET NAMES utf8");

//$coreSQL2 = new coreSQL('localhost', $config['mysql2_user'], $config['mysql2_pass'], $config['mysql2_database']);
//$coreSQL2->query_class = "coreSQL2";
//$coreSQL2->connect();
//$coreSQL2->query("SET NAMES utf8");

if ($coreSession->session['debugger'] == 1) {
	$coreSQL->debug = 1;
	//$coreSQL2->debug = 1;
}

// ======== Create custom controllers ==========================================

include_once $config['core_dir']."core/inc/init.inc.php";

// ======== Load Smarty and Xajax ==============================================

include_once $config['core_dir']."core/inc/smarty.inc.php";

require_once $config['core_dir']."libs/xajax/xajax.inc.php";

$pos = strpos($pages->request_url, "?");
if ($pos !== false) {
	$xajax = new xajax(str_replace('"', '', $pages->request_url).'&xajax_call=1');
}
else {
	$xajax = new xajax(str_replace('"', '', $pages->request_url).'?xajax_call=1');
}

// ======== Process Xajax call or display Smarty page ==========================

if (isset($_GET['xajax_call'])) {
	// Xajax call
	include_once $config['core_dir']."core/inc/xajax.inc.php";
	include_once $config['core_dir']."core/inc/core_assign.inc.php";

	$xajax->processRequests();

	$coreSession->close();
	$coreSQL->disconect();
//	$coreSQL2->disconect();
}
else {
	// Default page display
	include_once $config['core_dir']."core/inc/act.inc.php";
	include_once $config['core_dir']."core/inc/xajax.inc.php";
	include_once $config['core_dir']."core/inc/assign.inc.php";
	include_once $config['core_dir']."core/inc/core_assign.inc.php";
	
	$coreSession->close();
	$output = $smarty->display('index'.$config['templates_extension']);
	$coreSQL->disconect();
//	$coreSQL2->disconect();
}

?>
