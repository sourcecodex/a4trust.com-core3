<?php

if ($users->isLogged) {
	$languages->list_table['rows'] = $languages->searchQuery($_POST, array("pos" => "ASC"));
}
else {
	redirect('');
}

?>