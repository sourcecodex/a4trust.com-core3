<?php

if ($users->isLogged) {
	$seo_contents->list_table['rows'] = $seo_contents->searchQuery($_POST, array("created" => "DESC"));
}
else {
	redirect('');
}

?>