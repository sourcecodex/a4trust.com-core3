<?php

if ($users->isLogged) {
	$mailer_templates->list_table['rows'] = $mailer_templates->searchQuery($_POST, array("created" => "DESC"));
}
else {
	redirect('');
}

?>