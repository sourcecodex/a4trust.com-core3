<?php

if ($users->isLogged) {
	$users->list_table['rows'] = $users->searchQuery($_POST);
}
else {
	redirect('');
}

?>