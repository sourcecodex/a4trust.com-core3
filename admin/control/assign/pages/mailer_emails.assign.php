<?php

if ($users->isLogged) {
	$mailer_emails->list_table['rows'] = $mailer_emails->searchQuery($_POST, array("created" => "DESC"));
}
else {
	redirect('');
}

?>