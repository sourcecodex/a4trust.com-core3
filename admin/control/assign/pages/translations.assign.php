<?php

if ($users->isLogged) {
	$translations->list_table['rows'] = $translations->searchQuery($_POST, array("id" => "ASC"));
}
else {
	redirect('');
}

?>