<?php

if ($users->isLogged) {
	$_POST = $users->getById($users->id);
	unset($_POST['password']);
}
else {
	redirect('');
}

?>