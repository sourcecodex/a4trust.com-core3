<?php

if ($users->isLogged) {
	
	$smarty->assign('user_ips', $coreSQL->queryColumn("SELECT `user_ip` FROM `users` WHERE 1", "user_ip"));
	
	$grouped_ips = $coreSQL->queryData("SELECT `user_ip`, COUNT(`user_ip`) AS `cc` FROM `adclicks` "
			. "WHERE `creative`!='' AND (`location`!='' OR `type`='bing ads') GROUP BY `user_ip` ORDER BY COUNT(`user_ip`) DESC");
	
	$ip_counts = array();
	foreach ($grouped_ips as $ip) {
		$ip_counts[$ip['user_ip']] = $ip['cc'];
	}
	
	$smarty->assign('ip_counts', $ip_counts);
	$smarty->assign('creatives', $creatives->getCreatives());
	
	$adclicks->list_table['rows'] = $adclicks->searchQuery($_POST, array("created" => "DESC"));
}
else {
	redirect('');
}

?>