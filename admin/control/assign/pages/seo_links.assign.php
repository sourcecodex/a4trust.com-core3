<?php

if ($users->isLogged) {
	
	if ($_GET['act'] == "test") {
		/*
		for ($i = 661; $i <= 686; $i++) {

			$seo_links->checkIsConfirmed(array(
				'id' => $i,
				'domain' => "serpyou.com",
			));
		}
		*/
	}
	
	$seo_links->list_table['rows'] = $seo_links->searchQuery($_POST, array("created" => "DESC"));
	
	foreach ($seo_links->list_table['rows'] as $key => $row) {
		if ($seo_links->isSubmittedDomain($row['domain'])) {
			$seo_links->list_table['rows'][$key]['submitted_domain'] = 1;
		}
		if ($seo_links->isConfirmedDomain($row['domain'])) {
			$seo_links->list_table['rows'][$key]['confirmed_domain'] = 1;
		}
	}
}
else {
	redirect('');
}

?>