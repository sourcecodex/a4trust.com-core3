<?php

if ($users->isLogged) {
	$options->list_table['rows'] = $options->searchQuery($_POST, array("name" => "ASC"));
}
else {
	redirect('');
}

?>