<?php

if ($users->isLogged) {
	$sending_options = array(
		0 => "All (no filter)",
		1 => "All except undelivered and members",
		2 => "All Except undelivered",
		3 => "All Except members",
		4 => "Unique contacts",
	);
	$smarty->assign("sending_options", $sending_options);
}
else {
	$create_popup = false;
}

?>