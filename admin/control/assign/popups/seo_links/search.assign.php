<?php

if ($users->isLogged) {
	$types = array(
		"new" => "new",
		"web blog" => "web blog",
		"blog post" => "blog post",
		"blog comment" => "blog comment",
		"seo tools" => "seo tools",
		"press release" => "press release",
		"q&a" => "q&a",
		"profile" => "profile",
		"audit profile" => "audit profile",
		"webdir profile" => "webdir profile",
		"listing" => "listing",
		"similar listing" => "similar listing",
		"webdir listing" => "webdir listing",
		"bookmark" => "bookmark",
		"forum post" => "forum post",
		"bad link" => "bad link",
	);
	$smarty->assign("types", $types);
	
	$_POST = $coreSession->session['page_info'][$pages->url]['filter'];
}
else {
	$create_popup = false;
}

?>