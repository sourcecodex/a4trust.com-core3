<?php

if ($users->isLogged) {
	$format = array(
		"text" => "Text",
		"html" => "HTML",
	);
	$smarty->assign("format", $format);

	$_POST = $mailer_templates->getById($form_data);
}
else {
	$create_popup = false;
}

?>