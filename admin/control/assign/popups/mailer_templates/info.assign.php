<?php

if ($users->isLogged) {

	$template_info = $mailer_templates->getById($form_data);

	$mailer_templates->info_modal['title'] .= $template_info['name'];

	$smarty->assign('template', $template_info);
}
else {
	$create_popup = false;
}

?>