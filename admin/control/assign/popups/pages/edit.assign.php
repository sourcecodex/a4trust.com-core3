<?php

if ($users->isLogged) {
	$page_types = array(
		"smarty" => "Smarty",
		"html" => "HTML",
		"core" => "Core",
	);
	$smarty->assign('page_types', $page_types);
	
	$_POST = $pages->getById($form_data);
}
else {
	$create_popup = false;
}

?>