<?php

if ($users->isLogged) {
	$page_types = array(
		"smarty" => "Smarty",
		"html" => "HTML",
		"core" => "Core",
	);
	$smarty->assign('page_types', $page_types);
}
else {
	$create_popup = false;
}

?>