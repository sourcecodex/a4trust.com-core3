<?php

if ($users->isLogged) {
	$page_types = array(
		"smarty" => "Smarty",
		"html" => "HTML",
		"core" => "Core",
	);
	$smarty->assign('page_types', $page_types);

	$_POST = $coreSession->session['page_info'][$pages->url]['filter'];
}
else {
	$create_popup = false;
}

?>