<?php

/**
 * Admin Users class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_users extends controller {

	/**
	 * Vartotojo id
	 *
	 * @var int
	 */
	var $id;

	/**
	 * Duomenys apie vartotoja
	 *
	 * @var array
	 */
	var $userData;

	/**
	 * Ar vartotojas prisijunges
	 *
	 * @var bool
	 */
	var $isLogged;

	/**
	 * Sesijos ir COOKIE lauku pavadinimai
	 *
	 */
	var $session_user, $session_user_data, $cookie_user, $cookie_hash;


	function admin_users() {
		parent::controller("users");

		$this->fields = array(
			"created" => "created",
			"last_online" => "datetime",
			"user_ip" => "string",
			"admin" => "bool",
			"first_name" => "string",
			"last_name" => "string",
			"email" => "string",
			"username" => "string",
			"password" => "password",
			"active" => "int",
			"confirm_code" => "string",
			"email_confirmed" => "bool",
			"default_lang" => "string",
		);

		$this->public_acts = array("signIn", "signIn_xajax", "signOut");
		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax", "addPlan_xajax");

		$this->sign_in_form = array(
			"layout" => "default",
			"method" => "post",
			"fields" => array(
				"email" => array(
					"type" => "email",
					"title" => "Email address",
					"placeholder" => "Enter email address",
				),
				"password" => array(
					"type" => "password",
					"title" => "Password",
					"placeholder" => "Enter password",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Log in",
					"align" => "right",
					"remember_me" => 1,
				),
			),
			"redirect" => "pages",
		);

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"row_attributes" => "users",
			"columns" => array(
				"created" => array(
					"type" => "date",
					"title" => "Created",
					"sorting" => 1,
					"format" => "%Y-%m-%d",
				),
				"last_online" => array(
					"type" => "date",
					"title" => "Logged",
					"sorting" => 1,
					"format" => "%Y-%m-%d",
				),
				"default_lang" => array(
					"type" => "text",
					"title" => "Lang",
					"sorting" => 1,
				),
				"user_ip" => array(
					"type" => "text",
					"title" => "User IP",
					"sorting" => 1,
				),
				"first_name" => array(
					"type" => "text",
					"title" => "First",
					"sorting" => 1,
				),
				"last_name" => array(
					"type" => "text",
					"title" => "Last",
					"sorting" => 1,
				),
				"email" => array(
					"type" => "email",
					"title" => "Email",
					"sorting" => 1,
				),
				/*"admin" => array(
					"type" => "yes_no",
					"title" => "Admin",
					"sorting" => 1,
				),*/
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "users/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this user?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add user",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"admin" => array(
					"type" => "yes_no",
					"title" => "Admin",
				),
				"default_lang" => array(
					"type" => "text",
					"title" => "Language",
				),
				"first_name" => array(
					"type" => "text",
					"title" => "First name",
				),
				"last_name" => array(
					"type" => "text",
					"title" => "Last name",
				),
				"email" => array(
					"type" => "text",
					"title" => "Email",
					"required" => 1,
					"autocomplete" => "off",
				),
				"password" => array(
					"type" => "password",
					"title" => "Password",
					"required" => 1,
					"autocomplete" => "off",
				),
				"active" => array(
					"type" => "yes_no",
					"title" => "Active",
				),
				"email_confirmed" => array(
					"type" => "yes_no",
					"title" => "Email confirmed",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit user",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"admin" => array(
					"type" => "yes_no",
					"title" => "Admin",
				),
				"default_lang" => array(
					"type" => "text",
					"title" => "Language",
				),
				"first_name" => array(
					"type" => "text",
					"title" => "First name",
				),
				"last_name" => array(
					"type" => "text",
					"title" => "Last name",
				),
				"email" => array(
					"type" => "text",
					"title" => "Email",
					"required" => 1,
					"autocomplete" => "off",
				),
				"password" => array(
					"type" => "password",
					"title" => "Password",
					"required" => 1,
					"autocomplete" => "off",
				),
				"active" => array(
					"type" => "yes_no",
					"title" => "Active",
				),
				"email_confirmed" => array(
					"type" => "yes_no",
					"title" => "Email confirmed",
				),
			),
		);

		$this->profile_edit_form = array(
			"title" => "Edit profile",
			"layout" => "default",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"admin" => array(
					"type" => "yes_no",
					"title" => "Admin",
				),
				"default_lang" => array(
					"type" => "text",
					"title" => "Language",
				),
				"first_name" => array(
					"type" => "text",
					"title" => "First name",
				),
				"last_name" => array(
					"type" => "text",
					"title" => "Last name",
				),
				"email" => array(
					"type" => "text",
					"title" => "Email",
					"required" => 1,
					"autocomplete" => "off",
				),
				"password" => array(
					"type" => "password",
					"title" => "Password",
					"required" => 1,
					"autocomplete" => "off",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Save",
					"align" => "right",
				),
			),
			"redirect" => "profile",
		);

		$this->search_form = array(
			"title" => "Search users",
			"layout" => "modal",
			"method" => "post",
			"action" => "users",
			"submit_title" => "Search",
			"fields" => array(
				"email" => array(
					"type" => "text",
					"title" => "Email",
					"autocomplete" => "off",
				),
			),
		);
		
		$this->add_plan_form = array(
			"title" => "Add plan (manual)",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"user_id" => array(
					"type" => "hidden",
				),
				"product_id" => array(
					"type" => "select",
					"options" => "products",
					"title" => "Products",
					"required" => 1,
				),
			),
			"redirect" => "users",
		);
		
		$this->add_plan2_form = array(
			"title" => "Add plan with payment (manual)",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"user_id" => array(
					"type" => "hidden",
				),
				"product_id" => array(
					"type" => "select",
					"options" => "products",
					"title" => "Products",
					"required" => 1,
				),
			),
			"redirect" => "users",
		);
		
		$this->search_query_fields = array("user_ip", "first_name", "email");
		
		$this->id = 0;
		$this->userData = array();
		$this->isLogged = 0;

		$this->session_user = 'coreUser';
		$this->session_user_data = 'coreUserData';
		$this->cookie_user = 'cookieUser';
		$this->cookie_hash = 'cookieHash';
	}

	function getByEmail($email) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `admin`=1 AND `email`='".addslashes($email)."' LIMIT 1");
	}

	function getByEmailPass($email, $password) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `admin`=1 AND `email`='".addslashes($email)."' AND `password`='".md5($password)."' LIMIT 1");
	}

	function getByEmailHash($email, $password) {
		global $coreSQL;
		return $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `admin`=1 AND `email`='".addslashes($email)."' AND `password`='".addslashes($password)."' LIMIT 1");
	}

	function updateIp($id) {
		global $coreSQL;
		$coreSQL->query("UPDATE `".$this->table."` SET `user_ip`='".clean(getIp())."' WHERE `id`=".(int)$id);
	}

	// ============================= Authentication ============================================

	function logUser() {
		global $config, $coreSession;

		// Log by COOKIE
		if ($config['user_cookie']) {
			if (!isset($coreSession->session[$this->session_user]) || empty($coreSession->session[$this->session_user])) {
				if (isset($_COOKIE[$this->cookie_user]) && !empty($_COOKIE[$this->cookie_user])) {
					$user_info = $this->getById($_COOKIE[$this->cookie_user]);
					if ($user_info['password'] == $_COOKIE[$this->cookie_hash] && $user_info['active'] == 1) {
						$coreSession->session[$this->session_user] = $user_info['id'];
						$coreSession->session[$this->session_user_data] = $user_info;
					}
				}
			}
		}

		if ($coreSession->session[$this->session_user] == 0) {
			unset($coreSession->session[$this->session_user]);
		}

		// Log by SESSION
		if (isset($coreSession->session[$this->session_user]) && !empty($coreSession->session[$this->session_user])) {
			$this->id = $coreSession->session[$this->session_user];
			$this->userData = $coreSession->session[$this->session_user_data];
			$this->isLogged = 1;
		}
	}

	function signIn($form_data) {
		global $config, $coreSQL, $coreSession, $smarty;

		$email = trim($form_data['email']);
		$password = trim($form_data['password']);

		if ($by_hash) {
			$rec = $this->getByEmailHash($email, $password);
		}
		else {
			$rec = $this->getByEmailPass($email, $password);
		}
		
		if (!empty($rec)) {
			if ($rec['active'] == 1) {
				$coreSession->session[$this->session_user] = $this->id = $rec['id'];
				$coreSession->session[$this->session_user_data] = $this->userData = $rec;
				$this->isLogged = 1;

				if (isset($form_data['disable_update_ip'])) {
					// Do nothing
				}
				else {
					$this->updateIp($this->id);
				}
				
				if ($config['user_cookie']) {
					if ($form_data['autologin']) {
						setcookie($this->cookie_user, $this->id, $config['user_cookie_time'], $config['cookie_path']);
						setcookie($this->cookie_hash, md5($password), $config['user_cookie_time'], $config['cookie_path']);
					}
				}

				//$coreSQL->query("UPDATE `".$this->table."` SET `user_login_count`=`user_login_count`+1 WHERE `id`=".(int)$this->id);

				if (isset($form_data['act_redirect'])) {
					$coreSession->close();
					redirect(clean($form_data['act_redirect']));
				}

			}
			else {
				$smarty->assign('error_active', 'email');
			}
		}
		else {
			$smarty->assign('error_login', 'email');
			$smarty->assign('error_login_pass', 'password');
		}
	}

	function signOut($form_data) {
		global $config, $coreSession;

		if (isset($coreSession->session[$this->session_user])) { unset($coreSession->session[$this->session_user]); }
		if (isset($coreSession->session[$this->session_user_data])) { unset($coreSession->session[$this->session_user_data]); }

		if ($config['user_cookie']) {
			setcookie($this->cookie_user, '', time()-1, $config['cookie_path']);
			setcookie($this->cookie_hash, '', time()-1, $config['cookie_path']);
		}

		$this->id = 0;
		$this->userData = array();
		$this->isLogged = 0;
	}

	// ============================= Add, edit, delete ============================================

	function add($form_data) {
		global $smarty;

		$form_data = cleanArray($form_data);

		$form_data['email'] = trim($form_data['email']);
		$form_data['password'] = trim($form_data['password']);

		$error = false;

		if (strlen($form_data['first_name']) < 2) {
			$smarty->assign('error_first_name_length', 'first_name');
			$error = true;
		}

		if (strlen($form_data['password']) < 6) {
			$smarty->assign('error_password_length', 'password');
			$error = true;
		}

		$user_info = $this->getByEmail($form_data['email']);
		if ($user_info) {
			$smarty->assign('error_email_exists', 'email');
			$error = true;
		}

		if (!$error) {

			$form_data['confirm_code'] = strtoupper(substr(md5(uniqid(rand())), 0, 6));
			$form_data['active'] = 1;

			parent::add($form_data);
		}
	}

	function edit($form_data) {
		global $smarty;

		$form_data = cleanArray($form_data);

		$form_data['email'] = trim($form_data['email']);

		$error = false;

		if (strlen($form_data['first_name']) < 2) {
			$smarty->assign('error_first_name_length', 'first_name');
			$error = true;
		}

		$user_info = $this->getById($form_data['id']);

		if ($user_info['email'] != $form_data['email']) {
			$user_info = $this->getByEmail($form_data['email']);
			if ($user_info) {
				$smarty->assign('error_email_exists', 'email');
				$error = true;
			}
		}

		if (!$error) {
			parent::edit($form_data);
		}
	}

	// ============================= Search ============================================

	function searchQuery($filter) {
		$search_sql = "";
		if ($filter['plan_subscribed'] == "1") {
			$search_sql .= " AND `plan`!='' ";
		}
		if ($filter['plan_ended'] == "1") {
			$search_sql .= " AND `payments`>0 AND `plan`='' ";
		}
		if (!empty($filter['search_query'])) {
			if ($this->search_query_fields) {
				$search_sql .= " AND (";
				foreach ($this->search_query_fields as $field) {
					$search_sql .= "`$field` LIKE '%".addslashes($filter['search_query'])."%'";
					if ($field !== end($this->search_query_fields)) { $search_sql .= " OR "; }
				}
				$search_sql .= ")";
			}
		}
		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

	function addPlan($form_data) {
		global $coreSQL;

		$user_id = (int)$form_data['user_id'];

		$product = $coreSQL->queryRow("SELECT * FROM `accounting_products` WHERE `id` = '".(int)$form_data['product_id']."'");
		
		if ($product) {
			
			$startFrom = time();
			
			parent::edit(array(
				"id" => $user_id,
				"billing_period" => $product['period'],
				"plan" => $product['plan'],
				"plan_start" => $startFrom,
				"plan_end" => strtotime(date('Y-m-d H:i:s', $startFrom).' + '.$product['period']),
				"trial_expired" => "0",
			));
		}
	}
	
	function addPlanWithPayment($form_data) {
		global $coreSQL, $accounting_payments;

		$user_id = (int)$form_data['user_id'];

		$product = $coreSQL->queryRow("SELECT * FROM `accounting_products` WHERE `id` = '".(int)$form_data['product_id']."'");
		$user_info = $this->getById($user_id);
		
		if ($product) {
			
			$startFrom = time();
			
			$accounting_payments->add(array(
				'user_id' => $user_id,
				'product_id' => $product['id'],
				'quantity_id' => 1,
				'billing_date' => time(),
				'billing_processor' => 'stripe',
				'billing_amount' => $product['amount'],
				'billing_total' => $product['amount'],
				'billing_order_number' => '',
				'billing_key' => '',
				'billing_status' => 'Completed',
				'extra' => 'subscription',
				'date_start' => $startFrom,
				'date_end' => strtotime(date('Y-m-d H:i:s', $startFrom).' + '.$product['period']),
			));
			
			parent::edit(array(
				"id" => $user_id,
				"billing_period" => $product['period'],
				"plan" => $product['plan'],
				"plan_start" => $startFrom,
				"plan_end" => strtotime(date('Y-m-d H:i:s', $startFrom).' + '.$product['period']),
				"trial_expired" => "0",
				"billing_email" => $user_info['email'],
				"payments" => (int)$user_info['payments'] + 1,
			));
		}
	}
	
}

?>