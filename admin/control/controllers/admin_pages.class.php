<?php

/**
 * Admin pages class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_pages extends controller {

	/**
	 * uzklausos URL
	 *
	 * @var string
	 */
	var $request_url;

	/**
	 * URL suskirstytas i dalis
	 *
	 * @var array
	 */
	var $path;

	/**
	 * uzklausos URL be parametru
	 *
	 * @var string
	 */
	var $url;

	/**
	 * puslapis kuri atitinka uzklausos URL
	 *
	 * @var array
	 */
	var $current_page;

	/**
	 * Ar surastas puslapis, kuri uzkrauti
	 *
	 * @var bool
	 */
	var $page_found;

	/**
	 * Assign filename, kuri includinti
	 *
	 * @var string
	 */
	var $include_assign;

	/**
	 * Template filename, kuri includinti
	 *
	 * @var string
	 */
	var $include_page, $include_tpl;

	/**
	 * Error tpl, kuri includinti
	 *
	 * @var string
	 */
	var $error_tpl;

	function admin_pages() {
		global $config;

		parent::controller("pages");

		$this->fields = array(
			"created" => "created",
			"admin" => "bool",
			"type" => "string",
			"layout" => "string",
			"path" => "string",
			"page_title" => "string",
			"page_description" => "text",
			"page_keywords" => "string",
		);
		
		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax");
		
		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"sorting" => 1,
				),
				"layout" => array(
					"type" => "text",
					"title" => "Layout",
					"sorting" => 1,
				),
				"path" => array(
					"type" => "text",
					"title" => "Path",
					"sorting" => 1,
				),
				"page_title" => array(
					"type" => "text",
					"title" => "Title",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "pages/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this page?",
				),
			),
			"rows" => array(),
		);
		
		$this->add_form = array(
			"title" => "Add page",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"type" => array(
					"type" => "select",
					"options" => "page_types",
					"title" => "Type",
					"default" => "smarty",
					"required" => 1,
				),
				"layout" => array(
					"type" => "text",
					"title" => "Layout",
					"default" => "page",
					"required" => 1,
				),
				"path" => array(
					"type" => "text",
					"title" => "Path",
				),
				"page_title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"page_description" => array(
					"type" => "textarea",
					"title" => "Description",
				),
				"page_keywords" => array(
					"type" => "text",
					"title" => "Keywords",
				),
			),
		);
		
		$this->edit_form = array(
			"title" => "Edit page",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"type" => array(
					"type" => "select",
					"options" => "page_types",
					"title" => "Type",
					"required" => 1,
				),
				"layout" => array(
					"type" => "text",
					"title" => "Layout",
					"required" => 1,
				),
				"path" => array(
					"type" => "text",
					"title" => "Path",
				),
				"page_title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"page_description" => array(
					"type" => "textarea",
					"title" => "Description",
				),
				"page_keywords" => array(
					"type" => "text",
					"title" => "Keywords",
				),
			),
		);

		$this->search_form = array(
			"title" => "Search pages",
			"layout" => "modal",
			"method" => "post",
			"action" => "pages",
			"submit_title" => "Search",
			"fields" => array(
				"type" => array(
					"type" => "select",
					"options" => "page_types",
					"title" => "Type",
				),
				"layout" => array(
					"type" => "text",
					"title" => "Layout",
				),
				"path" => array(
					"type" => "text",
					"title" => "Path",
				),
				"page_title" => array(
					"type" => "text",
					"title" => "Title",
				),
			),
		);
		
		$this->request_url = "";
		$this->url = "";
		$this->path = array();
		$this->current_page = array();
		$this->page_found = false;

		$this->include_assign = "assign/pages/index.assign.php";
		$this->include_page = "pages/index";
		$this->include_tpl = $this->include_page.$config['templates_extension'];
		$this->error_tpl = "error404".$config['templates_extension'];
	}

	function setPages() {
		global $config, $coreSQL;

		$this->request_url = substr($_SERVER['REQUEST_URI'], 1);

		if (isset($config['admin_path'])) $this->request_url = substr($this->request_url, strlen($config['admin_path'])+1);

		$pos = strpos($this->request_url, "?");

		if ($pos !== false) {
			$get_params = substr($this->request_url, $pos+1);
			parse_str($get_params, $_GET);
			$this->url = substr($this->request_url, 0, $pos);
			
			$this->request_url = str_replace("&xajax_call=1", "", $this->request_url);
			$this->request_url = str_replace("?xajax_call=1", "", $this->request_url);
		}
		else {
			$this->url = $this->request_url;
		}

		$this->path = explode("/", $this->url);

		$this->setCurrentPage($this->path);

	}

	function setCurrentPage($path_array) {
		global $config, $coreSQL;

		while (count($path_array) > 0) {

			$path = implode("/", $path_array);

			$this->current_page = $coreSQL->queryRow("SELECT * FROM `".$this->table."` WHERE `admin`=1 AND `path`='".addslashes($path)."'");

			if ($this->current_page) {
				$this->page_found = true;
				$this->include_assign = "assign/pages/".($path == ""?"index":$path).".assign.php";
				$this->include_page = "pages/".($path == ""?"index":$path);
				$this->include_tpl = $this->include_page.$config['templates_extension'];
				break;
			}

			array_pop($path_array);
		}
	}

	function insertAdminPages() {
		global $coreSQL;

		/*$coreSQL->query("INSERT INTO `".$this->table."` (`id`, `parent`, `path`, `title`) VALUES
			('2', '1', 'members', 'Members'),
			('3', '1', 'langs', 'Languages'),
			('4', '1', 'pages', 'Pages'),
			('5', '2', 'info', 'Member Info'),
			('6', '2', 'edit', 'Member Edit'),
			('7', '3', 'translations', 'Translations'),
			('8', '7', 'edit', 'Translation Edit');");*/
	}

	// ============================= Search ============================================

	function searchQuery($filter) {
		$search_sql = " AND `admin`=0";
		
		if (!empty($filter['search_query'])) {
			$search_sql .= " AND (`path` LIKE '%".addslashes($filter['search_query'])."%' OR
				`layout` LIKE '%".addslashes($filter['search_query'])."%' OR
				`page_title` LIKE '%".addslashes($filter['search_query'])."%')";
		}
		
		return parent::searchQuery($filter, array("path" => "ASC"), 20, $search_sql);
	}

}

?>