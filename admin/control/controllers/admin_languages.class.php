<?php

/**
 * Admin languages class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_languages extends controller {

	var $active_langs;

	function admin_languages() {
		parent::controller("languages");

		$this->fields = array (
			"lang" => "string",
			"name" => "string",
			"active" => "bool",
			"pos" => "int",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"pos" => array(
					"type" => "text",
					"title" => "Position",
					"sorting" => 1,
				),
				"lang" => array(
					"type" => "text",
					"title" => "Lang",
					"sorting" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"sorting" => 1,
				),
				"active" => array(
					"type" => "yes_no",
					"title" => "Active",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "languages/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this language?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add language",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"pos" => array(
					"type" => "text",
					"title" => "Position",
				),
				"lang" => array(
					"type" => "text",
					"title" => "Lang",
					"required" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
				),
				"active" => array(
					"type" => "yes_no",
					"title" => "Active",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit language",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"pos" => array(
					"type" => "text",
					"title" => "Position",
				),
				"lang" => array(
					"type" => "text",
					"title" => "Lang",
					"required" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
				),
				"active" => array(
					"type" => "yes_no",
					"title" => "Active",
				),
			),
		);

		$this->search_form = array(
			"title" => "Search languages",
			"layout" => "modal",
			"method" => "post",
			"action" => "languages",
			"submit_title" => "Search",
			"fields" => array(
				"pos" => array(
					"type" => "text",
					"title" => "Position",
				),
				"lang" => array(
					"type" => "text",
					"title" => "Lang",
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
				),
				"active" => array(
					"type" => "yes_no",
					"title" => "Active",
				),
			),
		);

	}

	function getActiveLangs() {
		global $coreSQL;

		$data = $coreSQL->queryDataCached("SELECT `lang` FROM `".$this->table."` WHERE `active`=1 ORDER BY `pos`");

		$result = array();
		foreach ($data as $field) {
			$result[] = $field['lang'];
		}

		return $result;
	}

}

?>