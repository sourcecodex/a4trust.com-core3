<?php

/**
 * Admin SEO links class
 *
 * @package Tefo core3
 * @version 2015.10.28
 * @author Tefo <tefo05@gmail.com>
 */

class admin_seo_links extends controller {

	function admin_seo_links() {
		parent::controller("seo_links");

		$this->fields = array(
			"created" => "created",
			"type" => "string",
			"domain" => "string",
			"url" => "string",
			"submitted" => "bool",
			"confirmed" => "bool",
			
			"page_title" => "string",
			"anchor_text" => "string",
			"first_discovered" => "date",
			
			"page_rank" => "int",
			"alexa_rank" => "int",
			"alexa_backlinks" => "int",
			"domain_authority" => "float",
			"page_authority" => "float",
			
			"facebook_page" => "string",
			"twitter_page" => "string",
			"google_page" => "string",
			"facebook_likes" => "int",
			"twitter_followers" => "int",
			"google_plus" => "int",
			
			"tag" => "string",
			"comment" => "text",
		);
		
		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"row_attributes" => "seo_links",
			"col_attributes" => "align",
			"columns" => array(
				"ids" => array(
					"type" => "id_checkbox",
				),
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"sorting" => 1,
				),
				"domain" => array(
					"type" => "link",
					"title" => "Domain",
					"http" => 1,
					"target" => "blank",
					"sorting" => 1,
				),
				"url" => array(
					"type" => "link",
					"title" => "URL",
					"target" => "blank",
					"sorting" => 1,
				),
				"first_discovered" => array(
					"type" => "text",
					"title" => "Discovered",
					"sorting" => 1,
				),
				"submitted" => array(
					"type" => "yes_no",
					"title" => "Submitted",
					"align" => "center",
					"sorting" => 1,
				),
				"confirmed" => array(
					"type" => "yes_no",
					"title" => "Confirmed",
					"align" => "center",
					"sorting" => 1,
				),
				"page_rank" => array(
					"type" => "text",
					"title" => "PR",
					"align" => "right",
					"sorting" => 1,
				),
				"domain_authority" => array(
					"type" => "string",
					"title" => "DA",
					"format" => "%d",
					"align" => "right",
					"sorting" => 1,
				),
				"page_authority" => array(
					"type" => "string",
					"title" => "PA",
					"format" => "%d",
					"align" => "right",
					"sorting" => 1,
				),
				"alexa_rank" => array(
					"type" => "text",
					"title" => "Alexa",
					"align" => "right",
					"sorting" => 1,
				),
				"alexa_backlinks" => array(
					"type" => "text",
					"title" => "Backlinks",
					"align" => "right",
					"sorting" => 1,
				),
				"tag" => array(
					"type" => "text",
					"title" => "Tag",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "seo_links/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this link?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add link",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"type" => array(
					"type" => "select",
					"options" => "types",
					"title" => "Type",
					"hide_default_option" => 1,
					"required" => 1,
					"default" => "audit profile",
				),
				"url" => array(
					"type" => "text",
					"title" => "URL",
					"required" => 1,
				),
				"submitted" => array(
					"type" => "yes_no",
					"title" => "Submitted",
					"default" => 1,
				),
				"confirmed" => array(
					"type" => "yes_no",
					"title" => "Confirmed",
					"default" => 1,
				),
				"tag" => array(
					"type" => "text",
					"title" => "Tag",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit link",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"type" => array(
					"type" => "select",
					"options" => "types",
					"title" => "Type",
					"hide_default_option" => 1,
					"required" => 1,
					"default" => "audit profile",
				),
				"url" => array(
					"type" => "text",
					"title" => "URL",
					"disabled" => 1,
				),
				"submitted" => array(
					"type" => "yes_no",
					"title" => "Submitted",
				),
				"confirmed" => array(
					"type" => "yes_no",
					"title" => "Confirmed",
				),
				"tag" => array(
					"type" => "text",
					"title" => "Tag",
				),
			),
		);
		
		$this->search_form = array(
			"title" => "Search links",
			"layout" => "modal",
			"method" => "post",
			"action" => "seo_links",
			"submit_title" => "Search",
			"fields" => array(
				"type" => array(
					"type" => "select",
					"options" => "types",
					"title" => "Type",
				),
				"multi_url" => array(
					"type" => "textarea",
					"title" => "URL",
				),
				"submitted" => array(
					"type" => "yes_no",
					"title" => "Submitted",
				),
				"confirmed" => array(
					"type" => "yes_no",
					"title" => "Confirmed",
				),
				"tag" => array(
					"type" => "text",
					"title" => "Tag",
				),
			),
		);
		
		//$this->createTableStructure();
	}
	
	function add($form_data) {
		if ($this->is($form_data['url'])) {
			return false;
		}
		else {
			$url_info = parse_url($form_data['url']);
		
			$form_data['domain'] = $url_info['host'];

			$domain_url = $url_info['scheme'].'://'.$url_info['host'];

			$form_data['page_rank'] = $this->checkPageRank($domain_url);

			$moz = $this->checkMoz($form_data['url']);
			$form_data['page_authority'] = $moz->upa;
			$form_data['domain_authority'] = $moz->pda;

			$alexa = $this->checkAlexa($form_data['domain']);
			$form_data['alexa_rank'] = $alexa['alexa_rank'];
			$form_data['alexa_backlinks'] = $alexa['alexa_backlinks'];
			
			return parent::add($form_data);
		}
	}
	
	function is($url) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `url`='".addslashes($url)."'");
	}
	
	// ============================= Search ============================================

	function searchQuery($filter) {

		$search_sql = " AND `type` != 'bad link'";
		
		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`domain` LIKE '%".addslashes($filter['search_query'])."%' OR
				`url` LIKE '%".addslashes($filter['search_query'])."%')";
		}
		
		if (!empty($filter['multi_url'])) {
			$strings = explode("\r\n", $filter['multi_url']);
			
			if ($strings) {
				$search_sql .= " AND (";
			
				foreach ($strings as $string) {
					$search_sql .= "`url` LIKE '%".addslashes($string)."%' OR ";
				}
				
				$search_sql = substr($search_sql, 0, -4);
				$search_sql .= ")";
			}
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}
	
	// ============================= Metrics ============================================
	
	function checkPageRank($url) {
		global $config;

		$checker_url = 'http://www.directorycritic.com/google-pagerank-checker.html';

		$data = array(
			"urls" => $url,
			"getrank" => "Get Pagerank",
		);

		$fields = '';
		foreach($data as $key => $value) {
			$fields .= $key . '=' . $value . '&';
		}
		rtrim($fields, '&');

		$post = curl_init();

		curl_setopt($post, CURLOPT_URL, $checker_url);
		curl_setopt($post, CURLOPT_POST, count($data));
		curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($post);

		curl_close($post);

		require_once $config['core_dir']."core/classes/html_dom.class.php";
		$doc = str_get_html($result);

		$elements = $doc->find('.get_pr_img');

		$page_rank = substr(trim($elements[0]->src), -6, 1);

		return $page_rank;
	}
	
	function checkMoz($url) {

		// you can obtain you access id and secret key here: https://moz.com/products/api/keys
		$accessID = "member-791795e6c8";
		$secretKey = "c1c489122ed7a6484fd3b7c0fcff87f2";
		// Set your expires for several minutes into the future.
		// Values excessively far in the future will not be honored by the Mozscape API.
		$expires = time() + 300;
		// A new linefeed is necessary between your AccessID and Expires.
		$stringToSign = $accessID."\n".$expires;
		// Get the "raw" or binary output of the hmac hash.
		$binarySignature = hash_hmac('sha1', $stringToSign, $secretKey, true);
		// We need to base64-encode it and then url-encode that.
		$urlSafeSignature = urlencode(base64_encode($binarySignature));
		// Add up all the bit flags you want returned.
		// Learn more here: http://apiwiki.moz.com/url-metrics
		$cols = "103079215104";
		// Put it all together and you get your request URL.
		$requestUrl = "http://lsapi.seomoz.com/linkscape/url-metrics/?Cols=".$cols."&AccessID=".$accessID."&Expires=".$expires."&Signature=".$urlSafeSignature;
		// Put your URLS into an array and json_encode them.
		$batchedDomains = array($url);
		$encodedDomains = json_encode($batchedDomains);
		// We can easily use Curl to send off our request.
		// Note that we send our encoded list of domains through curl's POSTFIELDS.
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => $encodedDomains
		);
		$ch = curl_init($requestUrl);
		curl_setopt_array($ch, $options);
		$content = curl_exec($ch);
		curl_close( $ch );
		$contents = json_decode($content);

		return $contents[0];
	}
	
	function checkAlexa($hostname) {
		global $config;

		require_once $config['core_dir']."core/classes/html_dom.class.php";
		$doc = file_get_html("http://www.alexa.com/siteinfo/".$hostname);

		$elements = $doc->find('.metrics-data');
		$alexa_rank = $elements[0]->plaintext;

		$elements = $doc->find('#linksin-panel-content .box1-r');
		$alexa_backlinks = $elements[0]->plaintext;

		$result = array();
		$result['alexa_rank'] = str_replace(',', '', $alexa_rank);
		$result['alexa_backlinks'] = str_replace(',', '', $alexa_backlinks);
		
		return $result;
	}
	
	function checkIsConfirmed($form_data) {
		global $config;
		
		$link_info = $this->getById($form_data['id']);
		
		if ($link_info) {
			
			require_once $config['core_dir']."core/classes/data_mining.class.php";

			$doc = new dataMining($link_info['url']);

			$confirmed = false;
			$page_title = '';
			$anchor_url = '';
			$anchor_text = '';
			
			if ($doc->html) {

				$doc->findPageHeaders();

				$page_title = trim(strip_tags($doc->headers_array['page_title']));
				
				$url_info = parse_url($link_info['url']);
				$doc->findPageLinks($url_info['scheme']."://".$url_info['host']."/");
				
				if ($doc->outer_links_array)
				foreach ($doc->outer_links_array as $link) {
					$url_info = parse_url($link['url']);
					if ($url_info['host'] == $form_data['domain']) {
						$confirmed = true;
						$anchor_url = trim($link['url']);
						$anchor_text = trim($link['title']);
						break;
					}
				}
			}
			
			$this->edit(array(
				'id' => $link_info['id'],
				'page_title' => $page_title,
				'anchor_text' => $anchor_text,
				'confirmed' => $confirmed,
			));
			
		}
		
	}
	
	function isSubmittedDomain($domain) {
		global $coreSQL;
		return $coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `domain`='".addslashes($domain)."' AND `submitted`=1 AND `tag`='serpyou.com'");
	}
	
	function isConfirmedDomain($domain) {
		global $coreSQL;
		return $coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `domain`='".addslashes($domain)."' AND `confirmed`=1 AND `tag`='serpyou.com'");
	}

}

?>