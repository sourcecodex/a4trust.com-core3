<?php

/**
 * Admin refclicks class
 *
 * @package Tefo core3
 * @version 2019.02.09
 * @author Tefo <tefo05@gmail.com>
 */

class admin_refclicks extends controller {

	function admin_refclicks() {
		parent::controller("refclicks");

		$this->fields = array(
			"created" => "created",
			"type" => "string",
			"user_ip" => "string",
			"http_referer" => "string",
		);

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"row_attributes" => "refclicks",
			"columns" => array(
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"sorting" => 1,
				),
				"user_ip" => array(
					"custom" => 1,
					"type" => "adclicks_user_ip",
					"title" => "User Ip",
					"sorting" => 1,
				),
				"http_referer" => array(
					"custom" => 1,
					"type" => "refclicks_referer",
					"title" => "Referer",
					"sorting" => 1,
					"maxlength" => 50,
				),
			),
			"rows" => array(),
		);

		$this->search_form = array(
			"title" => "Search refclicks",
			"layout" => "modal",
			"method" => "post",
			"action" => "refclicks",
			"submit_title" => "Search",
			"fields" => array(
				"type" => array(
					"type" => "text",
					"title" => "Type",
				),
				"user_ip" => array(
					"type" => "text",
					"title" => "User Ip",
				),
				"http_referer" => array(
					"type" => "text",
					"title" => "Referer",
				),
			),
		);

	}
	
	// ============================= Search ============================================

	function searchQuery($filter, $order = array("id" => "ASC"), $per_page = 50, $search_sql = "") {
		return parent::searchQuery($filter, $order, $per_page, $search_sql);
	}

}

?>