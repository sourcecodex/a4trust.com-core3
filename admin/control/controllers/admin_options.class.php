<?php

/**
 * Admin options class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_options extends controller {

	function admin_options() {
		parent::controller("options");

		$this->fields = array(
			"name" => "string",
			"value" => "text",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"sorting" => 1,
				),
				"value" => array(
					"type" => "text",
					"title" => "Value",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "options/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this option?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add option",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"value" => array(
					"type" => "textarea",
					"title" => "Value",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit option",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"value" => array(
					"type" => "textarea",
					"title" => "Value",
				),
			),
		);

		$this->search_form = array(
			"title" => "Search options",
			"layout" => "modal",
			"method" => "post",
			"action" => "settings",
			"submit_title" => "Search",
			"fields" => array(
				"name" => array(
					"type" => "text",
					"title" => "Name",
				),
				"value" => array(
					"type" => "textarea",
					"title" => "Value",
				),
			),
		);

	}

	// ============================= Search ============================================

	function searchQuery($filter) {

		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`name` LIKE '%".addslashes($filter['search_query'])."%' OR
				`value` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>