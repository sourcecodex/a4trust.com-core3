<?php

/**
 * Admin translations class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_translations extends controller {

	var $translations;

	function admin_translations() {
		parent::controller("translations");

		$this->fields = array (
			"created" => "created",
			"updated" => "datetime",
			"lid" => "string",
			"t" => "text",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"id" => array(
					"type" => "text",
					"title" => "Id",
					"sorting" => 1,
				),
				"t" => array(
					"type" => "text",
					"title" => "Translation",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "translations/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this translation?",
				),
			),
			"rows" => array(),
		);
		
		$this->add_form = array(
			"title" => "Add translation",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"en" => array(
					"type" => "text",
					"title" => "English",
				),
			),
		);
		
		$this->edit_form = array(
			"title" => "Edit translation",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"en" => array(
					"type" => "text",
					"title" => "English",
				),
			),
		);

		$this->search_form = array(
			"title" => "Search translations",
			"layout" => "modal",
			"method" => "post",
			"action" => "translations",
			"submit_title" => "Search",
			"fields" => array(
				"en" => array(
					"type" => "text",
					"title" => "English",
				),
			),
		);
		
	}

	function setTranslations() {
		global $languages;

		$list_columns = array();
		foreach ($languages->active_langs as $lang) {
			$this->fields[$lang] = "text";
			$list_columns[$lang] = array("type" => "text", "title" => strtoupper($lang));
		}

		$this->list_table['columns'] = array_slice($this->list_table['columns'], 0, 2) +
				$list_columns +	array_slice($this->list_table['columns'], 2);

		//pre($this->list_table['columns']);

	}

	function getTranslations() {
		global $coreSQL, $coreSession;

		$lang = "en";
		$data = $coreSQL->queryDataCached("SELECT `lid`, `$lang` AS `value` FROM `".$this->table."`");

		$this->translations = array();
		foreach ($data as $field) {
			$this->translations[$field['lid']] = $field['value'];
		}

		return true;
	}

	function t($value, $template = '') {
		global $coreSQL, $config;

		if (isset($this->translations[md5($value)])) {

			//$this->updateTranslation($value, $template);

			return $this->translations[md5($value)];
		}
		else {
			$tt = $coreSQL->queryRow("SELECT `id` FROM `".$this->table."` WHERE `lid`='".md5($value)."' LIMIT 1");
			if (empty($tt)) {
				$this->add(array(
					"updated" => time(),
					"lid" => md5($value),
					"t" => $value,
					"en" => $value,
				));
			}
			return $value;
		}
	}

}

?>