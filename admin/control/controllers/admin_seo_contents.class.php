<?php

/**
 * Admin SEO contents class
 *
 * @package Tefo core3
 * @version 2015.10.28
 * @author Tefo <tefo05@gmail.com>
 */

class admin_seo_contents extends controller {

	function admin_seo_contents() {
		parent::controller("seo_contents");

		$this->fields = array(
			"created" => "created",
			"type" => "string",
			"format" => "string",
			"title" => "string",
			"content" => "text",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax");
		
		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"ids" => array(
					"type" => "id_checkbox",
				),
				"info" => array(
					"type" => "info_button",
					"title" => "Info",
					"popup" => "seo_contents/info",
				),
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"format" => array(
					"type" => "text",
					"title" => "Format",
					"sorting" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"sorting" => 1,
				),
				"title" => array(
					"type" => "text",
					"title" => "Title",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "seo_contents/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this content?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add content",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"format" => array(
					"type" => "select",
					"title" => "Format",
					"options" => "format",
					"hide_default_option" => 1,
					"required" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
				),
				"title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"content" => array(
					"type" => "textarea",
					"title" => "Content",
					"style" => "height: 300px;",
				),
			),
			"width" => 1000,
		);

		$this->edit_form = array(
			"title" => "Edit content",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"format" => array(
					"type" => "select",
					"title" => "Format",
					"options" => "format",
					"hide_default_option" => 1,
					"required" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
				),
				"title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"content" => array(
					"type" => "textarea",
					"title" => "Content",
					"style" => "height: 300px;",
				),
			),
			"width" => 1000,
		);
		
		$this->search_form = array(
			"title" => "Search contents",
			"layout" => "modal",
			"method" => "post",
			"action" => "seo_contents",
			"submit_title" => "Search",
			"fields" => array(
				"type" => array(
					"type" => "text",
					"title" => "Type",
				),
				"title" => array(
					"type" => "text",
					"title" => "Title",
				),
			),
		);

		$this->info_modal = array(
			"title" => "Content Info",
			"layout" => "default",
			"width" => 1000,
		);
		
		//$this->createTableStructure();
	}
	
	// ============================= Add, edit, delete ============================================

	function add($form_data) {
		parent::add($form_data, false);
	}

	function edit($form_data) {
		parent::edit($form_data, false);
	}
	
	// ============================= Search ============================================

	function searchQuery($filter) {

		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`type` LIKE '%".addslashes($filter['search_query'])."%' OR
				`title` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>