<?php

/**
 * Admin Mailer emails class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_mailer_emails extends controller {

	function admin_mailer_emails() {
		parent::controller("mailer_emails");

		$this->fields = array(
			"created" => "created",
			"type" => "string",
			"user_from" => "string",
			"user_name" => "string",
			"to" => "string",
			"subject" => "string",
			"body" => "text",
			"attachments" => "text",
			"images" => "text",
			"status" => "int",
			"sending_options" => "int",
			"priority" => "int",
			"opened" => "bool",
			"clicked" => "bool",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax", "send_xajax", "sendToUsers_xajax");
		
		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"info" => array(
					"type" => "info_button",
					"title" => "Info",
					"popup" => "mailer_emails/info",
				),
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"sorting" => 1,
				),
				"user_from" => array(
					"type" => "email",
					"title" => "From",
					"sorting" => 1,
				),
				"user_name" => array(
					"type" => "text",
					"title" => "Username",
					"sorting" => 1,
				),
				"to" => array(
					"type" => "email",
					"title" => "To",
					"sorting" => 1,
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"sorting" => 1,
				),
				"status" => array(
					"type" => "text",
					"title" => "Status",
					"sorting" => 1,
				),
				"sending_options" => array(
					"type" => "text",
					"title" => "Options",
					"sorting" => 1,
				),
				"priority" => array(
					"type" => "text",
					"title" => "Priority",
					"sorting" => 1,
				),
				"opened" => array(
					"type" => "text",
					"title" => "Opened",
					"sorting" => 1,
				),
				"clicked" => array(
					"type" => "text",
					"title" => "Clicked",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "mailer_emails/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this email?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add email",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"required" => 1,
				),
				"user_from" => array(
					"type" => "text",
					"title" => "From",
					"required" => 1,
				),
				"user_name" => array(
					"type" => "text",
					"title" => "Username",
				),
				"to" => array(
					"type" => "text",
					"title" => "To",
					"required" => 1,
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"required" => 1,
				),
				"body" => array(
					"type" => "textarea",
					"title" => "Body",
					"required" => 1,
				),
				"status" => array(
					"type" => "text",
					"title" => "Status",
				),
				"sending_options" => array(
					"type" => "select",
					"title" => "Options",
					"options" => "sending_options",
				),
				"priority" => array(
					"type" => "text",
					"title" => "Priority",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit email",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"required" => 1,
				),
				"user_from" => array(
					"type" => "text",
					"title" => "From",
					"required" => 1,
				),
				"user_name" => array(
					"type" => "text",
					"title" => "Username",
				),
				"to" => array(
					"type" => "text",
					"title" => "To",
					"required" => 1,
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"required" => 1,
				),
				"body" => array(
					"type" => "textarea",
					"title" => "Body",
					"required" => 1,
				),
				"status" => array(
					"type" => "text",
					"title" => "Status",
				),
				"sending_options" => array(
					"type" => "select",
					"title" => "Options",
					"options" => "sending_options",
				),
				"priority" => array(
					"type" => "text",
					"title" => "Priority",
				),
			),
		);

		$this->search_form = array(
			"title" => "Search emails",
			"layout" => "modal",
			"method" => "post",
			"action" => "mailer_emails",
			"submit_title" => "Search",
			"fields" => array(
				"type" => array(
					"type" => "text",
					"title" => "Type",
				),
				"user_from" => array(
					"type" => "text",
					"title" => "From",
				),
				"user_name" => array(
					"type" => "text",
					"title" => "Username",
				),
				"to" => array(
					"type" => "text",
					"title" => "To",
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
				),
				"body" => array(
					"type" => "textarea",
					"title" => "Body",
				),
				"status" => array(
					"type" => "text",
					"title" => "Status",
				),
				"sending_options" => array(
					"type" => "select",
					"title" => "Options",
					"options" => "sending_options",
				),
				"priority" => array(
					"type" => "text",
					"title" => "Priority",
				),
				"opened" => array(
					"type" => "text",
					"title" => "Opened",
				),
				"clicked" => array(
					"type" => "text",
					"title" => "Clicked",
				),
			),
		);

		$this->send_form = array(
			"title" => "Send emails",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"user_from" => array(
					"type" => "select",
					"options" => "mailer_users",
					"title" => "From",
					"required" => 1,
				),
				"template" => array(
					"type" => "select",
					"options" => "mailer_templates",
					"title" => "Template",
					"attributes" => 'onchange="xajax_getMailerTemplate(this.value);"',
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"required" => 1,
				),
				"body" => array(
					"type" => "textarea",
					"title" => "Body",
					"required" => 1,
				),
				"div" => array(
					"type" => "div",
					"title" => "Tags: [email], [first_name], [last_name], [domain], [id_hash]",
				),
				"to" => array(
					"type" => "textarea",
					"title" => "Recipients",
					"required" => 1,
				),
				"sending_options" => array(
					"type" => "select",
					"title" => "Options",
					"options" => "sending_options",
				),
				"priority" => array(
					"type" => "text",
					"title" => "Priority",
					"default" => 1,
				),
			),
		);
		
		$this->send_to_users_form = array(
			"title" => "Send emails to users",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"user_from" => array(
					"type" => "select",
					"options" => "mailer_users",
					"title" => "From",
					"required" => 1,
				),
				"template" => array(
					"type" => "select",
					"options" => "mailer_templates",
					"title" => "Template",
					"attributes" => 'onchange="xajax_getMailerTemplate(this.value);"',
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"required" => 1,
				),
				"body" => array(
					"type" => "textarea",
					"title" => "Body",
					"required" => 1,
				),
				"div" => array(
					"type" => "div",
					"title" => "Tags: [email], [first_name], [last_name], [user_id], [lang], [code], [hash], [id_hash], [domain]",
				),
				"to" => array(
					"type" => "textarea",
					"title" => "Recipients",
					"required" => 1,
				),
				"sending_options" => array(
					"type" => "select",
					"title" => "Options",
					"options" => "sending_options",
				),
				"priority" => array(
					"type" => "text",
					"title" => "Priority",
					"default" => 1,
				),
			),
		);

		$this->info_modal = array(
			"title" => "Email Info",
			"layout" => "default",
			"width" => 1000,
		);

	}
	
	// ============================= Add, edit, send ============================================

	function add($form_data) {
		global $coreSQL;
		$item_id = parent::add($form_data, false);
		
		$mail_info = $this->getById($item_id);
		$body = str_replace("[id_hash]", md5($item_id), $mail_info['body']);
		$coreSQL->query("UPDATE `".$this->table."` SET `hash`='".md5($item_id)."', `body`='".addslashes($body)."' WHERE `id`=".(int)$item_id);
		
		return $item_id;
	}

	function edit($form_data) {
		parent::edit($form_data, false);
	}

	function send($form_data) {
		$recipients = explode("\n", $form_data['to']);

		if ($recipients)
		foreach ($recipients as $recipient) {

			$recipient = trim($recipient);

			$recipient_info = array();
			$recipient_info = explode("|", $recipient);

			$email = $recipient_info[0];
			$first_name = $recipient_info[1];
			$last_name = $recipient_info[2];
			$domain = $recipient_info[3];

			// TODO remove duplicates emails

			if (isValidEmail($email)) {
				$data = array();
				$data['type'] = "Admin email";
				$data['user_from'] = $form_data['user_from'];
				$data['user_name'] = $form_data['user_name'];

				$data['to'] = $email;

				$data['subject'] = str_replace("[email]", $email, $form_data['subject']);
				$data['body'] = str_replace("[email]", $email, $form_data['body']);

				if (!empty($first_name)) {
					$data['subject'] = str_replace("[first_name]", $first_name, $data['subject']);
					$data['body'] = str_replace("[first_name]", $first_name, $data['body']);
				}
				else {
					$data['subject'] = str_replace("[first_name]", "", $data['subject']);
					$data['body'] = str_replace("[first_name]", "", $data['body']);
				}

				if (!empty($last_name)) {
					$data['subject'] = str_replace("[last_name]", $last_name, $data['subject']);
					$data['body'] = str_replace("[last_name]", $last_name, $data['body']);
				}
				else {
					$data['subject'] = str_replace("[last_name]", "", $data['subject']);
					$data['body'] = str_replace("[last_name]", "", $data['body']);
				}

				if (!empty($domain)) {
					$data['subject'] = str_replace("[domain]", $domain, $data['subject']);
					$data['body'] = str_replace("[domain]", $domain, $data['body']);
				}
				else {
					$data['subject'] = str_replace("[domain]", "", $data['subject']);
					$data['body'] = str_replace("[domain]", "", $data['body']);
				}

				$data['status'] = 0;
				$data['sending_options'] = (int)$form_data['sending_options'];
				$data['priority'] = (int)$form_data['priority'];

				$this->add($data, false);
			}
		}
	}
	
	function sendToUsers($form_data) {
		global $config, $coreSQL;
		
		$recipients = explode("\n", $form_data['to']);

		if ($recipients)
		foreach ($recipients as $recipient) {

			$user_info = $coreSQL->queryRow("SELECT * FROM `users` WHERE `email`='".addslashes(trim($recipient))."' LIMIT 1");
			
			if ($user_info) {
			
				$user_id = $user_info['id'];
				$lang = $user_info['default_lang'];
				$code = $user_info['confirm_code'];
				$hash = $user_info['password'];
				$email = $user_info['email'];
				$first_name = $user_info['first_name'];
				$last_name = $user_info['last_name'];
				$domain = $user_info['main_domain'];
				
				if (isValidEmail($email)) {
					$data = array();
					$data['type'] = "Admin email";
					$data['user_from'] = $form_data['user_from'];
					$data['user_name'] = $form_data['user_name'];

					$data['to'] = $email;

					$data['subject'] = str_replace("[user_id]", $user_id, $form_data['subject']);
					$data['body'] = str_replace("[user_id]", $user_id, $form_data['body']);

					$data['subject'] = str_replace("[lang]", $lang, $data['subject']);
					$data['body'] = str_replace("[lang]", $lang, $data['body']);

					$data['subject'] = str_replace("[code]", $code, $data['subject']);
					$data['body'] = str_replace("[code]", $code, $data['body']);

					$data['subject'] = str_replace("[hash]", $hash, $data['subject']);
					$data['body'] = str_replace("[hash]", $hash, $data['body']);

					$data['subject'] = str_replace("[email]", $email, $data['subject']);
					$data['body'] = str_replace("[email]", $email, $data['body']);

					if (!empty($first_name)) {
						$data['subject'] = str_replace("[first_name]", $first_name, $data['subject']);
						$data['body'] = str_replace("[first_name]", $first_name, $data['body']);
					}
					else {
						$data['subject'] = str_replace("[first_name]", "", $data['subject']);
						$data['body'] = str_replace("[first_name]", "", $data['body']);
					}

					if (!empty($last_name)) {
						$data['subject'] = str_replace("[last_name]", $last_name, $data['subject']);
						$data['body'] = str_replace("[last_name]", $last_name, $data['body']);
					}
					else {
						$data['subject'] = str_replace("[last_name]", "", $data['subject']);
						$data['body'] = str_replace("[last_name]", "", $data['body']);
					}
					
					if (!empty($domain)) {
						$data['subject'] = str_replace("[domain]", $domain, $data['subject']);
						$data['body'] = str_replace("[domain]", $domain, $data['body']);
					}
					else {
						$data['subject'] = str_replace("[domain]", "", $data['subject']);
						$data['body'] = str_replace("[domain]", "", $data['body']);
					}

					$data['status'] = 0;
					$data['sending_options'] = (int)$form_data['sending_options'];
					$data['priority'] = (int)$form_data['priority'];
					
					if (file_exists($config['main_dir'].'views/images/logo.png')) {
						$data['images'] = serialize(array(
							0 => array(
								'cid' => 'logo',
								'filename' => $config['main_dir'].'views/images/logo.png',
								'name' => 'Logo',
								'encoding' => 'base64',
								'type' => 'image/png',
							),
						));
					}
					
					$this->add($data, false);
				}
			}
		}
	}

	// ============================= Search ============================================

	function searchQuery($filter) {

		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`to` LIKE '%".addslashes($filter['search_query'])."%' OR
				`subject` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>