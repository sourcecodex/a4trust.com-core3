<?php

class admin_mailer_contacts extends controller {

	function admin_mailer_contacts() {
		parent::controller("mailer_contacts");

		$this->fields = array(
			"created" => "created",
			"email" => "string",
			"last_sent" => "datetime",
			"sent_count" => "int",
			"user_id" => "int",
			"undelivered" => "int",
			"out_of_office" => "int",
			"ignore" => "bool",
			"unsubscribe" => "bool",
		);

		//$this->createTableStructure();
	}

	function is($email) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `email`='".addslashes($email)."'");
	}

}

?>