<?php

/**
 * Admin countries class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_countries extends controller {

	function admin_countries() {
		parent::controller("countries");

		$this->fields = array(
			"country" => "string",
			"code" => "string",
			"phone_code" => "string",
			"continent_id" => "int",
		);
	}

	function getCountries() {
		global $coreSQL;

		$countries_data = $coreSQL->queryData("SELECT `id`, `country` AS `value` FROM `".$this->table."` ORDER BY `country`");
		$result = array();
		foreach ($countries_data as $country) {
			$result[$country['id']] = $country['value'];
		}

		return $result;
	}

}

?>