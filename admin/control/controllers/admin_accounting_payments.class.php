<?php

/**
 * Admin accounting payments class
 *
 * @package Tefo core3
 * @version 2014.05.21
 * @author Tefo <tefo05@gmail.com>
 */

class admin_accounting_payments extends controller {

    function admin_accounting_payments() {
		parent::controller("accounting_payments");

		$this->fields = array (
			"user_id" => "int",
			"product_id" => "int",
			"quantity_id" => "int",
			"billing_date" => "datetime",
			"billing_processor" => "string",
			"billing_amount" => "float",
			"refund_amount" => "float",
			"billing_total" => "float",
			"billing_order_number" => "string",
			"billing_key" => "string",
			"billing_status" => "string",
			"extra" => "text",
			"aff_export" => "bool",
			"date_start" => "datetime",
			"date_end" => "datetime",
		);

		$this->logged_acts = array("addManual_xajax");
		
		$this->add_form = array(
			"title" => "Add payment (manual)",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"user_id" => array(
					"type" => "hidden",
				),
				"product_id" => array(
					"type" => "select",
					"options" => "products",
					"title" => "Products",
					"required" => 1,
				),
			),
			"redirect" => "users",
		);

		//$this->createTableStructure();
	}
	
	function is($billing_order_number) {
		global $coreSQL;
		return $coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `billing_order_number` = '".addslashes($billing_order_number)."'");
	}

	function countActiveByUser($user_id) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT COUNT(`id`) FROM `".$this->table."` WHERE `date_end`>NOW() AND `user_id`=".(int)$user_id);
	}
	
	function getByUser($user_id) {
		global $coreSQL;
		$user_id = (int)$user_id;
		return $coreSQL->queryData("SELECT * FROM `".$this->table."` WHERE `user_id`=$user_id");
	}
	
	function getStartFrom($user_id) {
		global $coreSQL;

		$payments_count = $this->countActiveByUser($user_id);

		if ($payments_count) {
			return $coreSQL->queryValue("SELECT MAX(`date_end`) FROM `".$this->table."` WHERE `user_id`=".(int)$user_id);
		}
		else {
			return 0;
		}
	}

	function getByUserWithDescription($user_id) {
		global $coreSQL;
		$user_id = (int)$user_id;
		return $coreSQL->queryData("
			SELECT `".$this->table."`.*, `accounting_products`.`description`
			FROM `".$this->table."`, `accounting_products`
			WHERE `".$this->table."`.`product_id`=`accounting_products`.`id` AND `".$this->table."`.`user_id`=$user_id");
	}
	
	function getProducts() {
		global $coreSQL;
		return $coreSQL->queryData("SELECT * FROM `accounting_products`", "id");
	}
	
	function getProductsWithDescription() {
		global $coreSQL;
		$products = $coreSQL->queryData("SELECT * FROM `accounting_products`");
		
		$result = array();
		foreach ($products as $product) {
			$result[$product['id']] = $product['description'];
		}
		
		return $result;
	}


}

?>