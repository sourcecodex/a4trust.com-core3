<?php

/**
 * Admin popups class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_popups extends controller {

	var $popups;

	function admin_popups() {
		global $config;

		parent::controller("popups");

		$this->fields = array(
			"created" => "created",
			"admin" => "bool",
			"name" => "string",
			"default" => "bool",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"sorting" => 1,
				),
				"default" => array(
					"type" => "yes_no",
					"title" => "Default",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "popups/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this popup?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add popup",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"default" => array(
					"type" => "yes_no",
					"title" => "Default",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit popup",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"default" => array(
					"type" => "yes_no",
					"title" => "Default",
				),
			),
		);

		$this->search_form = array(
			"title" => "Search popups",
			"layout" => "modal",
			"method" => "post",
			"action" => "popups",
			"submit_title" => "Search",
			"fields" => array(
				"name" => array(
					"type" => "text",
					"title" => "Name",
				),
				"default" => array(
					"type" => "yes_no",
					"title" => "Default",
				),
			),
		);

	}

	function setPopups() {
		global $coreSQL, $coreSession;

		$db_popups = $coreSQL->queryDataCached("SELECT * FROM `".$this->table."` WHERE `admin`=1");

		$this->popups = array();
		foreach ($db_popups as $popup) {
			$this->popups[$popup['name']] = $popup;
		}
	}
	
	// ============================= Search ============================================

	function searchQuery($filter) {
		$search_sql = " AND `admin`=0";

		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`name` LIKE '%".addslashes($filter['search_query'])."%')";
		}
		
		return parent::searchQuery($filter, array("name" => "ASC"), 20, $search_sql);
	}


}

?>