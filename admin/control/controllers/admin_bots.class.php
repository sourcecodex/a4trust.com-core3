<?php

/**
 * Admin bots class
 *
 * @package Tefo core3
 * @version 2016.02.15
 * @author Tefo <tefo05@gmail.com>
 */

class admin_bots extends controller {

	function admin_bots() {

		parent::controller("bots");

		$this->fields = array(
			"created" => "created",
			"title" => "string",
			"ip" => "string",
			"botnet" => "bool",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"title" => array(
					"type" => "text",
					"title" => "Title",
					"sorting" => 1,
				),
				"ip" => array(
					"type" => "ip_link",
					"title" => "IP",
					"sorting" => 1,
				),
				"botnet" => array(
					"type" => "yes_no",
					"title" => "Botnet",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "bots/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this bot?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add bot",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"title" => array(
					"type" => "text",
					"title" => "Title",
					"required" => 1,
				),
				"ip" => array(
					"type" => "text",
					"title" => "IP",
					"required" => 1,
				),
				"botnet" => array(
					"type" => "yes_no",
					"title" => "Botnet",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit bot",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"title" => array(
					"type" => "text",
					"title" => "Title",
					"required" => 1,
				),
				"ip" => array(
					"type" => "text",
					"title" => "IP",
					"required" => 1,
				),
				"botnet" => array(
					"type" => "yes_no",
					"title" => "Botnet",
				),
			),
		);

		$this->search_form = array(
			"title" => "Search bots",
			"layout" => "modal",
			"method" => "post",
			"action" => "bots",
			"submit_title" => "Search",
			"fields" => array(
				"title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"ip" => array(
					"type" => "text",
					"title" => "IP",
				),
				"botnet" => array(
					"type" => "yes_no",
					"title" => "Botnet",
				),
			),
		);

	}
	
	// ============================= Search ============================================

	function searchQuery($filter) {
		$search_sql = "";

		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`title` LIKE '%".addslashes($filter['search_query'])."%' OR
								`ip` LIKE '%".addslashes($filter['search_query'])."%')";
		}
		
		return parent::searchQuery($filter, array("id" => "ASC"), 50, $search_sql);
	}


}

?>