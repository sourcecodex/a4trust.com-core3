<?php

/**
 * Admin CRM companies class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_crm_companies extends controller {

	function admin_crm_companies() {
		parent::controller("crm_companies");

		$this->fields = array(
			"created" => "created",
			"name" => "string",
			"code" => "string",
			"vat" => "string",
		);

		//$this->createTableStructure();
	}

	// ============================= Search ============================================

	function searchQuery($filter) {

		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`name` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>