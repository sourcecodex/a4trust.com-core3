<?php

/**
 * Admin Mailer templates class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_mailer_templates extends controller {

	function admin_mailer_templates() {
		parent::controller("mailer_templates");

		$this->fields = array(
			"created" => "created",
			"format" => "string",
			"lang" => "string",
			"name" => "string",
			"subject" => "text",
			"body" => "text",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"info" => array(
					"type" => "info_button",
					"title" => "Info",
					"popup" => "mailer_templates/info",
				),
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"format" => array(
					"type" => "text",
					"title" => "Format",
					"sorting" => 1,
				),
				"lang" => array(
					"type" => "text",
					"title" => "Language",
					"sorting" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"sorting" => 1,
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "mailer_templates/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this template?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add template",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"format" => array(
					"type" => "select",
					"title" => "Format",
					"options" => "format",
					"hide_default_option" => 1,
					"required" => 1,
				),
				"lang" => array(
					"type" => "text",
					"title" => "Language",
					"required" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"required" => 1,
				),
				"body" => array(
					"type" => "textarea",
					"title" => "Body",
					"required" => 1,
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit template",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"format" => array(
					"type" => "select",
					"title" => "Format",
					"options" => "format",
					"hide_default_option" => 1,
					"required" => 1,
				),
				"lang" => array(
					"type" => "text",
					"title" => "Language",
					"required" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"subject" => array(
					"type" => "text",
					"title" => "Subject",
					"required" => 1,
				),
				"body" => array(
					"type" => "textarea",
					"title" => "Body",
					"required" => 1,
				),
			),
		);

		$this->info_modal = array(
			"title" => "Template: ",
			"layout" => "default",
			"width" => 1000,
		);
		
	}

	// ============================= Add, edit, delete ============================================

	function add($form_data) {
		parent::add($form_data, false);
	}

	function edit($form_data) {
		parent::edit($form_data, false);
	}

	// ============================= Get & Search ============================================

	function getAllTemplates() {
		global $coreSQL;
		$templates_data = $coreSQL->queryData("SELECT * FROM `".$this->table."` ORDER BY `name` ASC, `lang` ASC");

		$result = array();
		foreach ($templates_data as $template) {
			$result[$template['id']] = $template['name'].' ('.$template['lang'].')';
		}

		return $result;
	}

	function searchQuery($filter) {

		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`name` LIKE '%".addslashes($filter['search_query'])."%' OR
				`subject` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>