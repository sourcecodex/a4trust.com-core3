<?php

class admin_mailer_users extends controller {

	function admin_mailer_users() {
		parent::controller("mailer_users");

		$this->fields = array(
			"user" => "string",
			"password" => "string",
			"from_email" => "string",
			"from_name" => "string",
		);

		//$this->createTableStructure();
	}

	function getAllUsers() {
		global $coreSQL;
		$users_data = $coreSQL->queryColumn("SELECT `user` FROM `".$this->table."` ORDER BY `user` ASC", "user");

		$result = array();
		foreach ($users_data as $user) {
			$result[$user] = $user;
		}

		return $result;
	}

}

?>