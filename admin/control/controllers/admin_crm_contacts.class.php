<?php

/**
 * Admin CRM contacts class
 *
 * @package Tefo core3
 * @version 2014.02.23
 * @author Tefo <tefo05@gmail.com>
 */

class admin_crm_contacts extends controller {

	function admin_crm_contacts() {
		parent::controller("crm_contacts");

		$this->fields = array(
			"created" => "created",
			"domain_id" => "int",
			"profile_url" => "string",
			
			"first_name" => "string",
			"last_name" => "string",
			"email" => "string",
			"phone" => "string",
			
			"location" => "string",
			"company" => "string",
			"company_id" => "int",
			"job_title" => "string",

			"website" => "string",
			"twitter" => "string",
			"google_plus" => "string",
			"facebook" => "string",
			"linkedin" => "string",
			"skype" => "string",
			"contacts" => "string",
			
			"tag" => "string",
			"tag2" => "int",
			"comment" => "text",
			
			// join mailer_contacts
			
			"sent_count" => "int",
			"last_sent" => "datetime",
			"undelivered" => "bool",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax", "export_xajax");

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"ids" => array(
					"type" => "id_checkbox",
				),
				"info" => array(
					"type" => "info_button",
					"title" => "Info",
					"popup" => "crm_contacts/info",
				),
				"first_name" => array(
					"type" => "text",
					"title" => "Name",
					"sorting" => 1,
				),
				"email" => array(
					"type" => "email",
					"title" => "Email",
					"sorting" => 1,
				),
				"company" => array(
					"type" => "text",
					"title" => "Company",
					"sorting" => 1,
				),
				"location" => array(
					"type" => "text",
					"title" => "Location",
					"sorting" => 1,
				),
				"website" => array(
					"type" => "link",
					"title" => "Website",
					"http" => 1,
					"target" => "blank",
					"sorting" => 1,
				),
				"social" => array(
					"type" => "social_buttons",
					"title" => "Social",
					"sorting" => 1,
				),
				"sent_count" => array(
					"type" => "text",
					"title" => "Sent",
					"sorting" => 1,
				),
				"tag" => array(
					"type" => "tag",
					"title" => "Tag",
					"sorting" => 1,
				),
				"contacts" => array(
					"type" => "link",
					"title" => "Contacts",
					"target" => "blank",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "crm_contacts/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this contact?",
				),
			),
			"rows" => array(),
		);

		$this->add_form = array(
			"title" => "Add contact",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"first_name" => array(
					"type" => "text",
					"title" => "First name",
				),
				/*"last_name" => array(
					"type" => "text",
					"title" => "Last name",
				),*/
				"email" => array(
					"type" => "text",
					"title" => "Email",
				),
				"company" => array(
					"type" => "text",
					"title" => "Company",
				),
				"location" => array(
					"type" => "text",
					"title" => "Location",
				),
				"website" => array(
					"type" => "text",
					"title" => "Website",
				),
				"contacts" => array(
					"type" => "text",
					"title" => "Contacts",
				),
				"tag" => array(
					"type" => "text",
					"title" => "Tag",
				),
			),
		);

		$this->edit_form = array(
			"title" => "Edit contact",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"first_name" => array(
					"type" => "text",
					"title" => "First name",
				),
				/*"last_name" => array(
					"type" => "text",
					"title" => "Last name",
				),*/
				"email" => array(
					"type" => "text",
					"title" => "Email",
				),
				"phone" => array(
					"type" => "text",
					"title" => "Phone",
				),
				"company" => array(
					"type" => "text",
					"title" => "Company",
				),
				"location" => array(
					"type" => "text",
					"title" => "Location",
				),
				"website" => array(
					"type" => "text",
					"title" => "Website",
				),
				"twitter" => array(
					"type" => "text",
					"title" => "Twitter",
				),
				"google_plus" => array(
					"type" => "text",
					"title" => "Google+",
				),
				"facebook" => array(
					"type" => "text",
					"title" => "Facebook",
				),
				"linkedin" => array(
					"type" => "text",
					"title" => "LinkedIn",
				),
				"skype" => array(
					"type" => "text",
					"title" => "Skype",
				),
				"contacts" => array(
					"type" => "text",
					"title" => "Contacts",
				),
				"tag" => array(
					"type" => "text",
					"title" => "Tag",
				),
				"comment" => array(
					"type" => "textarea",
					"title" => "Comment",
				),
			),
		);
		
		$this->search_form = array(
			"title" => "Search contacts",
			"layout" => "modal",
			"method" => "post",
			"action" => "crm_contacts",
			"submit_title" => "Search",
			"fields" => array(
				"first_name" => array(
					"type" => "text",
					"title" => "First name",
				),
				"email" => array(
					"type" => "text",
					"title" => "Email",
				),
				"location" => array(
					"type" => "text",
					"title" => "Location",
				),
				"website" => array(
					"type" => "text",
					"title" => "Website",
				),
				"tag" => array(
					"type" => "text",
					"title" => "Tag",
				),
				"tag2" => array(
					"type" => "text",
					"title" => "Tag2",
				),
				"sent_count" => array(
					"type" => "text",
					"title" => "Sent",
				),
			),
		);

		$this->info_modal = array(
			"title" => "Contact Info",
			"layout" => "default",
			"width" => 700,
		);

		//$this->createTableStructure();
	}

	function is($email) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE `email`='".addslashes($email)."'");
	}

	// ============================= Search ============================================

	function searchQuery($filter) {

		// issiusta 28-40
		$search_sql = " AND `tag2`=27";
		
		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`first_name` LIKE '%".addslashes($filter['search_query'])."%' OR
				`email` LIKE '%".addslashes($filter['search_query'])."%' OR
				`phone` LIKE '%".addslashes($filter['search_query'])."%' OR
				`website` LIKE '%".addslashes($filter['search_query'])."%' OR
				`tag` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}
	
	function searchQuery2($filter) {
		
		$search_sql = "";
		
		if (!empty($filter['search_query'])) {
			$search_sql .= " AND (`".$this->table."`.`first_name` LIKE '%".addslashes($filter['search_query'])."%' OR
				`".$this->table."`.`email` LIKE '%".addslashes($filter['search_query'])."%' OR
				`".$this->table."`.`phone` LIKE '%".addslashes($filter['search_query'])."%' OR
				`".$this->table."`.`website` LIKE '%".addslashes($filter['search_query'])."%' OR
				`".$this->table."`.`tag` LIKE '%".addslashes($filter['search_query'])."%')";
		}
		
		if (!empty($filter['sent_count'])) {
			$search_sql .= " AND `mailer_contacts`.`sent_count`=".(int)$filter['sent_count'];
			unset($filter['sent_count']);
		}

		$result_fields = "`".$this->table."`.*, `sent_count`, `last_sent`, `undelivered`";
		$from_sql = "`".$this->table."` LEFT JOIN `mailer_contacts` ON `".$this->table."`.`email`=`mailer_contacts`.`email`";
		
		return parent::searchQuery2($filter, array("created" => "DESC"), 50, $search_sql, false, $result_fields, $from_sql);
	}

	function export($form_data) {
		global $coreSQL;

		$ids = array();

		foreach ($form_data as $form_field) {
			if (substr($form_field['name'], 0, 3) == "ids") {
				array_push($ids, $form_field['value']);
			}
		}

		if ($ids) {
			$export_sql = $coreSQL->sqlValues("id", $ids, "OR");

			$export_data = $coreSQL->queryData("SELECT * FROM `".$this->table."` WHERE ( $export_sql )");
			
			$result = '';
			foreach ($export_data as $row) {
				$result .= $row['email'].'|'.$row['first_name']/*.'|'.$row['last_name'].'|'.$row['website']*/.'<br/>';
			}
			
			return $result;
		}
		
		// SELECT `email`, `first_name`, `last_name`, `website`  FROM `crm_contacts` WHERE `tag` LIKE ''
	}

	// SELECT `domain`, CONCAT('info@', `domain`, '.', `top_level_domain`), `hostname`, `tag` FROM `domains` WHERE `tag` LIKE 'eshop uk' AND `domain`!='co'
	
}

?>