<?php

/**
 * Admin adclicks class
 *
 * @package Tefo core3
 * @version 2016.01.25
 * @author Tefo <tefo05@gmail.com>
 */

class admin_adclicks extends controller {

	function admin_adclicks() {
		parent::controller("adclicks");

		$this->fields = array(
			"created" => "created",
			"type" => "string",
			"user_ip" => "string",
			"keyword" => "string",
			"location" => "string",
			"matchtype" => "string",
			"creative" => "string",
			"loc_physical_ms" => "string",
			"loc_interest_ms" => "string",
			"http_referer" => "string",
		);

		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"row_attributes" => "adclicks",
			"columns" => array(
				"created" => array(
					"type" => "text",
					"title" => "Created",
					"sorting" => 1,
				),
				"type" => array(
					"type" => "text",
					"title" => "Type",
					"sorting" => 1,
				),
				"user_ip" => array(
					"custom" => 1,
					"type" => "adclicks_user_ip",
					"title" => "User Ip",
					"sorting" => 1,
				),
				"creative" => array(
					"custom" => 1,
					"type" => "adclicks_campaign",
					"title" => "Campaign",
					"sorting" => 1,
				),
				"location" => array(
					"type" => "text",
					"title" => "Location",
					"sorting" => 1,
				),
				"keyword" => array(
					"type" => "text",
					"title" => "Keyword",
					"sorting" => 1,
				),
				"http_referer" => array(
					"custom" => 1,
					"type" => "adclicks_referer",
					"title" => "Referer",
					"sorting" => 1,
					"maxlength" => 50,
				),
			),
			"rows" => array(),
		);

		$this->search_form = array(
			"title" => "Search adclicks",
			"layout" => "modal",
			"method" => "post",
			"action" => "adclicks",
			"submit_title" => "Search",
			"fields" => array(
				"type" => array(
					"type" => "text",
					"title" => "Type",
				),
				"user_ip" => array(
					"type" => "text",
					"title" => "User Ip",
				),
				"keyword" => array(
					"type" => "text",
					"title" => "Keyword",
				),
				"location" => array(
					"type" => "text",
					"title" => "Location",
				),
				"http_referer" => array(
					"type" => "text",
					"title" => "Referer",
				),
			),
		);

	}
	
	// ============================= Search ============================================

	function searchQuery($filter) {

		//$search_sql = " AND `creative`!='' AND ((`location`!='' AND `http_referer`!='') OR `type`='bing ads')";
		$search_sql = " AND `creative`!='' AND (`location`!='' AND `http_referer`!='')";
		
		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>