<div class="row" style="margin-bottom: 20px;">
	<div class="col-lg-6">
		<button type="button" class="btn btn-primary" onclick="xajax_createPopup('mailer_emails/send');">
			<span class="glyphicon glyphicon-plus"></span> Send emails
		</button>
		<button type="button" class="btn btn-default" onclick="xajax_createPopup('mailer_emails/send_to_users');">
			<span class="glyphicon glyphicon-plus"></span> Send emails to users
		</button>
	</div>
	<div class="col-lg-6">
		<div class="pull-right">
			{include file="file:`$core_com`search_form.tpl" controller=$core_controllers.mailer_emails}
		</div>
	</div>
</div>

{include file="file:`$core_com`table.tpl" controller=$core_controllers.mailer_emails table_name="list_table"}

{include file="file:`$core_com`pagination.tpl"}