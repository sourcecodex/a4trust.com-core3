{capture name="modal_body"}
<div style="margin-bottom: 10px; padding-bottom: 10px; border-bottom: 1px solid #c1c1c1;">
	Subject: {$email.subject}
</div>
<div>
	{$email.body}
</div>
{/capture}
{include file="file:`$core_com`modal.tpl" controller=$core_controllers.mailer_emails modal_name="info_modal"}