{capture name="modal_body"}
<div class="panel panel-default" style="width: 650px;">
	<div class="panel-heading">
		<h3 class="panel-title">
			Title
		</h3>
	</div>
	<div class="panel-body">
		<table style="margin-bottom: 20px;">
			<tr>
				<td width="120">Name:</td>
				<td>{$contact.first_name} {$contact.last_name}</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><a href="mailto:{$contact.email}">{$contact.email}</a></td>
			</tr>
			<tr>
				<td>Phone:</td>
				<td>{$contact.phone}</td>
			</tr>
			<tr>
				<td>Company:</td>
				<td>{$contact.company} {if $contact.job_title}<span style="color: #999;">({$contact.job_title})</span>{/if}</td>
			</tr>
			<tr>
				<td>Location:</td>
				<td>{$contact.location}</td>
			</tr>
			<tr>
				<td>Website:</td>
				<td><a href="{$contact.website}">{$contact.website}</a></td>
			</tr>
			<tr>
				<td>Social:</td>
				<td>
					{if $contact.twitter}
					<a target="_blank" href="{$contact.twitter}">
						<img width="22" src="http://serpyou.com/views/images/icons/social/twitter.png">
					</a>
					{/if}
					{if $contact.facebook}
					<a target="_blank" href="{$contact.facebook}">
						<img width="22" src="http://serpyou.com/views/images/icons/social/facebook.png">
					</a>
					{/if}
					{if $contact.google_plus}
					<a target="_blank" href="{$contact.google_plus}">
						<img width="22" src="http://serpyou.com/views/images/icons/social/googleplus.png">
					</a>
					{/if}
					{if $contact.linkedin}
					<a target="_blank" href="{$contact.linkedin}">
						<img width="22" src="http://serpyou.com/views/images/icons/social/linkedin.png">
					</a>
					{/if}
				</td>
			</tr>
			<tr>
				<td>Tag:</td>
				<td>{$contact.tag}</td>
			</tr>
		</table>
	</div>
</div>
{/capture}
{include file="file:`$core_com`modal.tpl" controller=$core_controllers.crm_contacts modal_name="info_modal"}