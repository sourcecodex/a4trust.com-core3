{capture name="modal_body"}
<div style="margin-bottom: 10px; padding-bottom: 10px; border-bottom: 1px solid #c1c1c1;">
	Subject: {$template.subject}
</div>
<div>
	{if $template.format == "text"}
		{$template.body|nl2br}
	{elseif $template.format == "html"}
		{$template.body}
	{/if}
</div>
{/capture}
{include file="file:`$core_com`modal.tpl" controller=$core_controllers.mailer_templates modal_name="info_modal"}