{capture name="modal_body"}
<div class="panel panel-default" style="width: 950px;">
	<div class="panel-heading">
		<h3 class="panel-title">
			{$content.title}
		</h3>
	</div>
	<div class="panel-body">
		{if $content.format == "text"}
			{$content.content|nl2br}
		{elseif $content.format == "html"}
			{$content.content}
		{/if}
	</div>
</div>
{/capture}
{include file="file:`$core_com`modal.tpl" controller=$core_controllers.seo_contents modal_name="info_modal"}