{if $core_user->isLogged}
<div class="navbar navbar-inverse" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{$config.site_url}">{$config.project_name}</a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li {if $core_pages->path[0] == ""}class="active"{/if}>
					<a href="{$config.site_url}">Home</a>
				</li>
				<li {if $core_pages->path[0] == "users"}class="active"{/if}>
					<a href="{$config.site_url}users">Users</a>
				</li>
				<li {if $core_pages->path[0] == "companies"}class="active"{/if}>
					<a href="{$config.site_url}companies">Companies</a>
				</li>
				<li {if $core_pages->path[0] == "requests"}class="active"{/if}>
					<a href="{$config.site_url}requests">Requests</a>
				</li>
				<li {if $core_pages->path[0] == "news"}class="active"{/if}>
					<a href="{$config.site_url}news">News</a>
				</li>
				{*
				<li class="dropdown{if $core_pages->path[0] == "pages" || $core_pages->path[0] == "popups" || $core_pages->path[0] == "translations" || $core_pages->path[0] == "languages"} active{/if}">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">CMS <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="{$config.site_url}pages">Pages</a></li>
						<li><a href="{$config.site_url}popups">Popups</a></li>
					</ul>
				</li>
				<li class="dropdown{if $core_pages->path[0] == "mailer_emails" || $core_pages->path[0] == "mailer_templates" || $core_pages->path[0] == "mailer_undelivered"} active{/if}">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Mailer <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="{$config.site_url}mailer_emails">Emails</a></li>
						<li><a href="#" onclick="postToUrl('mailer_emails', {ldelim}'clicked': 1{rdelim}); return false;">Email clicks</a></li>
						<li><a href="{$config.site_url}mailer_templates">Templates</a></li>
						<li><a href="{$config.site_url}mailer_undelivered">Undelivered</a></li>
					</ul>
				</li>
				<li {if $core_pages->path[0] == "settings"}class="active"{/if}>
					<a href="{$config.site_url}settings">Settings</a>
				</li>
				*}
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						{$core_user->userData.first_name} {$core_user->userData.last_name} <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{$config.site_url}profile">Edit my profile</a></li>
						<li><a href="{$config.site_url}history">Log in history</a></li>
						<li class="divider"></li>
						<li><a href="#" onclick="signOut(); return false;">Log out</a></li>
					</ul>
				</li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>
{/if}