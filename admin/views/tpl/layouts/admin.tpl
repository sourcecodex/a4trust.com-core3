{include file="file:`$config.core_dir`admin/views/tpl/topbar.tpl"}
<div class="container">
	{if $core_pages->current_page.type == "core"}
		{include file="file:`$config.core_tpl_dir``$core_pages->include_tpl`"}
	{else}
		{include file=$core_pages->include_tpl}
	{/if}
</div>