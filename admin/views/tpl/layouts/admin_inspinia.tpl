<div id="wrapper">
	{include file="file:`$config.core_dir`admin/views/tpl/topbar_inspinia.tpl"}
	<div id="page-wrapper" class="gray-bg" style="padding-top: 15px;">
		{if $core_pages->current_page.type == "core"}
			{include file="file:`$config.core_tpl_dir``$core_pages->include_tpl`"}
		{else}
			{include file=$core_pages->include_tpl}
		{/if}
	</div>
</div>

<script type="text/javascript" src="http://admin.seopoz.com/views/js/pace.min.js"></script>
<script type="text/javascript" src="http://admin.seopoz.com/views/js/jquery.metisMenu.js"></script>
<script type="text/javascript" src="http://admin.seopoz.com/views/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="http://admin.seopoz.com/views/js/inspinia.js"></script>