{if $core_user->isLogged} 
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
					<span class="clear">
						<span class="block m-t-xs">
							<strong class="font-bold">{$core_user->userData.first_name} {$core_user->userData.last_name}</strong>
						</span>
						<span class="text-muted text-xs block">Admin <b class="caret"></b></span>
					</span>
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a href="{$config.site_url}profile">Edit my profile</a></li>
						<li><a href="{$config.site_url}history">Log in history</a></li>
						<li><a href="#" onclick="signOut(); return false;">Logout</a></li>
					</ul>
				</div>
				<div class="logo-element">
					IN+
				</div>
			</li>
			<li {if $core_pages->path[0] == ""}class="active"{/if}>
				<a href="{$config.site_url}"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span></a>
			</li>
			<li {if $core_pages->path[0] == "pages"}class="active"{/if}>
				<a href="{$config.site_url}pages"><i class="fa fa-th-large"></i> <span class="nav-label">Pages</span></a>
			</li>
			<li {if $core_pages->path[0] == "popups"}class="active"{/if}>
				<a href="{$config.site_url}popups"><i class="fa fa-th-large"></i> <span class="nav-label">Popups</span></a>
			</li>
			<li {if $core_pages->path[0] == "users"}class="active"{/if}>
				<a href="{$config.site_url}users"><i class="fa fa-th-large"></i> <span class="nav-label">Users</span></a>
			</li>
			<li class="dropdown{if $core_pages->path[0] == "mailer_emails" || $core_pages->path[0] == "mailer_templates" || $core_pages->path[0] == "mailer_undelivered"} active{/if}">
				<a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Mailer</span><span class="fa arrow"></span></a>
				 <ul class="nav nav-second-level collapse">
					<li><a href="{$config.site_url}mailer_emails">Emails</a></li>
					<li><a href="{$config.site_url}mailer_templates">Templates</a></li>
					<li><a href="{$config.site_url}mailer_undelivered">Undelivered</a></li>
				</ul>
			</li>
			{if $config.module_crm}
			<li {if $core_pages->path[0] == "crm_contacts" || $core_pages->path[0] == "crm_companies"}class="active"{/if}>
				<a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">CRM</span><span class="fa arrow"></span></a>
				 <ul class="nav nav-second-level collapse">
					<li><a href="{$config.site_url}crm_contacts">Contacts</a></li>
					<li><a href="{$config.site_url}crm_companies">Companies</a></li>
				</ul>
			</li>
			{/if}
			{if $config.module_seo}
			<li {if $core_pages->path[0] == "seo_contents" || $core_pages->path[0] == "seo_links"}class="active"{/if}>
				<a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">SEO</span><span class="fa arrow"></span></a>
				 <ul class="nav nav-second-level collapse">
					<li><a href="{$config.site_url}seo_contents">Content</a></li>
					<li><a href="{$config.site_url}seo_links">Links</a></li>
				</ul>
			</li>
			{/if}
			<li {if $core_pages->path[0] == "translations" || $core_pages->path[0] == "translations_list" || $core_pages->path[0] == "languages"}class="active"{/if}>
				<a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Languages</span><span class="fa arrow"></span></a>
				 <ul class="nav nav-second-level collapse">
					<li><a href="{$config.site_url}translations">Translations</a></li>
					<li><a href="{$config.site_url}translations_list">Translations list</a></li>
					<li><a href="{$config.site_url}languages">Languages</a></li>
				</ul>
			</li>
			<li {if $core_pages->path[0] == "statistics" || $core_pages->path[0] == "blocked_ips" || $core_pages->path[0] == "bots"}class="active"{/if}>
				<a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Tools</span><span class="fa arrow"></span></a>
				 <ul class="nav nav-second-level collapse">
					<li><a href="{$config.site_url}adclicks">Ad clicks</a></li>
					<li><a href="{$config.site_url}blocked_ips">Blocked Ips</a></li>
					<li><a href="{$config.site_url}bots">Bots</a></li>
				</ul>
			</li>
			<li {if $core_pages->path[0] == "settings"}class="active"{/if}>
				<a href="{$config.site_url}settings"><i class="fa fa-th-large"></i> <span class="nav-label">Settings</span></a>
			</li>
		</ul>
	</div>
</nav>
{/if}