<!DOCTYPE html>
<html lang="en">

<head>
<base href="{$config.site_url}" />
<title>Admin [{$config.project_name}]</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="{$config.project_url}views/vendors/slick/slick.css">
<link rel="stylesheet" type="text/css" href="{$config.project_url}views/vendors/tinymce/js/tinymce/skins/ui/oxide/skin.min.css" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/css/star-rating.min.css" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/themes/krajee-svg/theme.min.css" />

{foreach from=$default_css_styles item="style"}
<link rel="stylesheet" type="text/css" href="{$config.views_url}css/{$style}" />
{/foreach}

<link rel="shortcut icon" href="{$config.project_url}views/images/favicon.ico" />

{$xajax_js}

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{$config.project_url}views/vendors/slick/slick.js"></script>
<script type="text/javascript" src="{$config.project_url}views/js/misc_functions.js"></script>
<script type="text/javascript" src="{$config.project_url}views/js/validation.js"></script>
<script type="text/javascript" src="{$config.project_url}views/js/popup.js"></script>
<script type="text/javascript" src="{$config.project_url}views/js/callbacks.js"></script>
<script type="text/javascript" src="{$config.project_url}views/vendors/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/js/star-rating.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.1.2/themes/krajee-svg/theme.min.js"></script>
</head>

<body>
	<div id="core_checker">
		{include file="file:`$core_com`checker.tpl"}
	</div>

	{if !$core_pages->page_found}
		{include file=$core_pages->error_tpl}
	{else}
		{include file="file:`$config.core_tpl_dir`layouts/`$core_pages->current_page.layout`.tpl"}
	{/if}
	
	{*pre var=$core_popups*}
	{*pre var=$core_pages*}
	
	<div id="core_sql_debugger">
		{include file="file:`$core_com`debuger.tpl"}
	</div>
</body>

</html>