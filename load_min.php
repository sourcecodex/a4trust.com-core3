<?php

/**
 * Load_min.php
 *
 * @package Tefo core3
 * @version 2014.05.09
 * @author Tefo <tefo05@gmail.com>
 */

include_once $config['core_dir']."core/inc/config.inc.php";

include_once $config['core_dir']."core/inc/misc.inc.php";

require_once $config['core_dir']."core/classes/controller.class.php";

if (isset($config['disable_core_session'])) {
	// Do nothing
}
else {
	require_once $config['core_dir']."core/classes/session.class.php";
	$coreSession = new coreSession();
}

require_once $config['core_dir']."core/classes/mysql.class.php";
$coreSQL = new coreSQL($config['mysql_server'], $config['mysql_user'], $config['mysql_pass'], $config['mysql_database']);
$coreSQL->connect();
$coreSQL->query("SET NAMES utf8");

//$coreSQL2 = new coreSQL('localhost', $config['mysql2_user'], $config['mysql2_pass'], $config['mysql2_database']);
//$coreSQL2->connect();
//$coreSQL2->query("SET NAMES utf8");

?>
